<?php

namespace Nitra\ProductBundle\Routing;

use Symfony\Bundle\FrameworkBundle\Routing\Router as BaseRouter;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class Router extends BaseRouter
{
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /** @var \Symfony\Component\Routing\RequestContext */
    protected $context;

    protected $locale = null;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container, $resource, array $options = array(), RequestContext $context = null, $logger = null)
    {
        parent::__construct($container, $resource, $options, $context, $logger);

        $this->container = $container;
        $this->dm        = $container->get('doctrine_mongodb.odm.document_manager');
        $this->context   = $context;
    }

    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if ($this->getConfig('enabled')) {
            return $this->generateSemantic($name, $parameters, $referenceType);
        } elseif (isset($parameters['slug']) && is_object($parameters['slug']) && method_exists($parameters['slug'], 'getAlias')) {
            $parameters['slug'] = $parameters['slug']->getAlias();
        }

        return $this->getGenerator()->generate($name, $parameters, $referenceType);
    }

    /**
     * Generate semantic link
     *
     * @param string            $name
     * @param array             $parameters
     * @param boolean|string    $referenceType
     *
     * @return string|null Generated link
     */
    protected function generateSemantic($name, $parameters, $referenceType)
    {
        $methodName = $this->generateMethodByName($name);
        if (!$methodName) {
            return;
        }

        $method = 'generateSemantic' . $methodName;

        if (method_exists($this, $method) && $link = $this->$method($parameters)) {
            $homeParameters = array();
            if (array_key_exists('_locale', $parameters)) {
                $homeParameters['_locale'] = $parameters['_locale'];
            }
            $home = $this->getGenerator()->generate('nitra_store_home_index', $homeParameters, $referenceType);

            return $home . $link;
        }

        return $this->getGenerator()->generate($name, $parameters, $referenceType);
    }

    /**
     * Generate method for call
     *
     * @param string $name  Route name
     *
     * @return null|string
     */
    protected function generateMethodByName($name)
    {
        $method = null;
        foreach (explode('_', $name) as $nameItem) {
            $method .= ucfirst($nameItem);
        }

        return $method;
    }

    /**
     * {@inheritdoc}
     */
    public function matchRequest(Request $request)
    {
        return $this->match($request->getPathInfo());
    }

    /**
     * {@inheritdoc}
     */
    public function match($pathInfo)
    {
        try {
            return $this->getMatcher()->match($pathInfo);
        } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $exception) {
            $locale = null;
            if ($this->container->hasParameter('locales') && $locales = $this->container->getParameter('locales')) {
                $matches = array();
                if (!preg_match('/^(\/(' . implode('|', $locales) . '))(.*)/', $pathInfo, $matches)) {
                    throw $exception;
                }
                list (, $this->locale, $locale, $pathInfo) = $matches;
            }
            if ($this->getConfig('enabled') && $matched = $this->matchSemantic($pathInfo)) {
                if ($locale) {
                    $matched['_locale'] = $locale;
                }
                return $matched;
            }

            throw $exception;
        }
    }

    /**
     * Match semantic link
     *
     * @param string            $pathInfo
     *
     * @return array An array of parameters
     */
    protected function matchSemantic($pathInfo)
    {
        if ($articleCategory = $this->matchSemanticInformationCategoryPage($pathInfo)) {
            return $articleCategory;
        }
        if ($article = $this->matchSemanticInformationPage($pathInfo)) {
            return $article;
        }
        if ($category = $this->matchSemanticCategoryPage($pathInfo)) {
            return $category;
        }
        if ($model = $this->matchSemanticModelPage($pathInfo)) {
            return $model;
        }
        if ($product = $this->matchSemanticProductPage($pathInfo)) {
            return $product;
        }

        return array();
    }

    /**
     * Get config by key
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getConfig($key)
    {
        return $this->container->getParameter('nitra.router.' . $key);
    }

    /**
     * Generate query string for link
     *
     * @param string $routeName
     * @param array  $parameters
     *
     * @return null|string
     */
    protected function generateQueryString($routeName, $parameters)
    {
        unset($parameters['slug'], $parameters['_locale']);

        if (isset($parameters['page']) && $parameters['page'] == 1) {
            unset($parameters['page']);
        }
        foreach ($this->getRouteCollection()->get($routeName)->getDefaults() as $key => $value) {
            if (array_key_exists($key, $parameters) && $parameters[$key] == $value) {
                unset ($parameters[$key]);
            }
        }

        $queryParameters = http_build_query($parameters);

        return $queryParameters
            ? ('?' . $queryParameters)
            : null;
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticCategoryPage($parameters)
    {
        if (is_object($parameters['slug']) && $parameters['slug'] instanceof \Nitra\ProductBundle\Document\Category) {
            return $parameters['slug']->getFullUrlAliasEn() . $this->generateQueryString('category_page', $parameters);
        }

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();
        if (!$category) {
            return null;
        }

        return $category['fullUrlAliasEn'] . $this->generateQueryString('category_page', $parameters);
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticCategoryPage($pathInfo)
    {
        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('aliasEn')
            ->field('fullUrlAliasEn')->equals(trim($pathInfo, '/'))
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $sort = null;
        if ($this->container->isScopeActive('request') && $sort = $this->container->get('request')->query->get('sort')) {
            $sort = '/' . $sort;
        }
        $parameters = $this->getMatcher()->match($this->locale . '/category/__slug__' . $sort);
        $parameters['slug'] = $category['aliasEn'];

        return $parameters;
    }

    /**
     * Generate semantic link to model page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticModelPage($parameters)
    {
        if (is_object($parameters['slug']) && $parameters['slug'] instanceof \Nitra\ProductBundle\Document\Model) {
            $categoryFullAlias = $parameters['slug']->getCategory()->getFullUrlAliasEn();
            return $categoryFullAlias . '/' . $parameters['slug']->getAlias();
        }

        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('category')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$model) {
            return null;
        }

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('id')->equals($model['category']['$id'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['fullUrlAliasEn'] . '/' . $parameters['slug'];
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticModelPage($pathInfo)
    {
        $matches = array();
        if (!preg_match('/\/(.*)\/(.*)$/', $pathInfo, $matches)) {
            return null;
        }
        list (, $categoryAlias, $modelAlias) = $matches;

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('_id')
            ->field('fullUrlAliasEn')->equals($categoryAlias)
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($modelAlias)
            ->field('category.$id')->equals($category['_id'])
            ->getQuery()->getSingleResult();

        if (!$model) {
            return null;
        }

        return $this->getMatcher()->match($this->locale . '/model/' . $modelAlias);
    }

    /**
     * Generate semantic link to product page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticProductPage($parameters)
    {
        if (is_object($parameters['slug']) && $parameters['slug'] instanceof \Nitra\ProductBundle\Document\Product) {
            return $parameters['slug']->getFullUrlAliasEn();
        }

        $product = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$product) {
            return null;
        }

        return $product['fullUrlAliasEn'];
    }

    /**
     * Match semantic product page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticProductPage($pathInfo)
    {
        $product = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('aliasEn')
            ->field('fullUrlAliasEn')->equals(trim($pathInfo, '/'))
            ->getQuery()->getSingleResult();

        if (!$product) {
            return null;
        }

        return $this->getMatcher()->match($this->locale . '/product/' . $product['aliasEn']);
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticInformationCategoryPage($parameters)
    {
        if (is_object($parameters['slug']) && $parameters['slug'] instanceof \Nitra\InformationBundle\Document\InformationCategory) {
            return $parameters['slug']->getAlias() . $this->generateQueryString('information_category_page', $parameters);
        }
        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['aliasEn'] . $this->generateQueryString('information_category_page', $parameters);
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticInformationCategoryPage($pathInfo)
    {
        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals(trim($pathInfo, '/'))
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $this->getMatcher()->match($this->locale . '/information-category-page/' . trim($pathInfo, '/'));
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticInformationPage($parameters)
    {
        if (is_object($parameters['slug']) && $parameters['slug'] instanceof \Nitra\InformationBundle\Document\Information) {
            $categoryAlias = $parameters['slug']->getInformationCategory()->getAlias();
            return $categoryAlias . '/' . $parameters['slug']->getAlias();
        }

        $article = $this->dm->createQueryBuilder('NitraInformationBundle:Information')
            ->hydrate(false)->select('informationCategory')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$article) {
            return null;
        }

        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('_id')->equals($article['informationCategory']['$id'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['aliasEn'] . '/' . $parameters['slug'];
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticInformationPage($pathInfo)
    {
        if (!preg_match('/\/.*\/.*\/?/', $pathInfo)) {
            return null;
        }
        list (, $categoryAlias, $articleAlias) = explode('/', $pathInfo);

        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('_id')
            ->field('aliasEn')->equals($categoryAlias)
            ->getQuery()->getSingleResult();
        if (!$category) {
            return null;
        }

        $article = $this->dm->createQueryBuilder('NitraInformationBundle:Information')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($articleAlias)
            ->field('informationCategory.$id')->equals($category['_id'])
            ->getQuery()->getSingleResult();

        if (!$article) {
            return null;
        }

        return $this->getMatcher()->match($this->locale . '/information-page/' . $articleAlias);
    }
}