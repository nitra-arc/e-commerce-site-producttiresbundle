<?php

namespace Nitra\ProductBundle\Form\Type\Selector;

use Symfony\Component\Form\FormBuilderInterface;

class AutoRadioType extends AbstractAutoType
{
    /**
     * {@ineritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('category', 'choice', array(
            'label'    => 'selector.auto.form.category.label',
            'help'     => 'selector.auto.form.category.help',
            'choices'  => $options['categories'],
            'expanded' => true,
            'multiple' => false,
        ));

        $this->addSubmitButton($builder);
    }
}