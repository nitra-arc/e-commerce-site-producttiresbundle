<?php

namespace Nitra\ProductBundle\Form\Type\Selector;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class AbstractAutoType extends AbstractType
{
    protected $name;
    protected $useSelect2;

    /**
     * Constructor
     *
     * @param string  $name
     * @param boolean $useSelect2
     */
    public function __construct($name, $useSelect2 = true)
    {
        $this->name       = $name;
        $this->useSelect2 = $useSelect2;
    }

    /**
     * {@ineritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('vendor',       $this->getFormType('vendor'),       $this->getFormOptions('vendor',       $options));
        $builder->add('vehicle',      $this->getFormType('vehicle'),      $this->getFormOptions('vehicle',      $options));
        $builder->add('year',         $this->getFormType('year'),         $this->getFormOptions('year',         $options));
        $builder->add('modification', $this->getFormType('modification'), $this->getFormOptions('modification', $options));
    }

    /**
     * {@ineritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'vendors'            => array(),
            'categories'         => array(),
            'translation_domain' => 'NitraProductBundle',
            'required'           => false,
            'index'              => null,
        ));
    }

    /**
     * Get form type
     *
     * @param string $field
     *
     * @return string
     */
    protected function getFormType($field)
    {
        return $this->useSelect2
            ? 'genemu_jqueryselect2_choice'
            : 'choice';
    }

    /**
     * Get options for form
     *
     * @param string $field
     * @param array  $options
     *
     * @return array
     */
    protected function getFormOptions($field, $options)
    {
        $formOptions = array(
            'label'         => "selector.auto.form.$field.label",
            'help'          => "selector.auto.form.$field.help",
            'empty_value'   => '',
            'disabled'      => true,
        );

        if ($this->useSelect2) {
            $formOptions['configs'] = array(
                'placeholder'   => null,
                'width'         => 200,
                'allowClear'    => true,
            );
        }

        if ($field == 'vendor') {
            $formOptions['disabled']    = false;
            $formOptions['choices']     = $options['vendors'];
        }

        return $formOptions;
    }

    /**
     * Add button for submit form
     *
     * @param FormBuilderInterface $builder
     */
    protected function addSubmitButton($builder)
    {
        $builder->add('submit', 'submit', array(
            'label'    => 'selector.auto.form.submit',
            'disabled' => true,
        ));
    }

    /**
     * {@ineritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
}