<?php

namespace Nitra\ProductBundle\Form\Type\Selector;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class AutoTabsType extends AbstractAutoType
{
    /**
     * {@ineritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('category', 'hidden', array(
            'data' => $options['index'] ?: array_keys($options['categories'])[0],
        ));

        $this->addSubmitButton($builder);
    }

    /**
     * {@ineritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['categories'] = $options['categories'];
    }
}