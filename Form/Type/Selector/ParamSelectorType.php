<?php

namespace Nitra\ProductBundle\Form\Type\Selector;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParamSelectorType extends AbstractType
{
    /** @var string */
    protected $name;
    /** @var boolean */
    protected $useSelect2;

    /**
     * Constructor
     *
     * @param string  $name
     * @param boolean $useSelect2
     */
    public function __construct($name, $useSelect2 = false)
    {
        $this->name       = $name;
        $this->useSelect2 = $useSelect2;
    }

    /**
     * {@ineritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categories', 'hidden', array(
            'data' => $options['key'],
        ));

        $brandsData = isset($options['optSel']['brands'])
            ? $options['optSel']['brands']
            : null;

        if ($options['brands'] != null) {
            $builder->add('brands', $this->getFormType(), $this->getFormOptions('selector.parameters.field.brand.label', $options['brands'], $brandsData));
        }

        foreach ($options['params'] as $key => $value) {
            if ($value['value']) {
                $parameterData = isset($options['optSel'][$key])
                    ? $options['optSel'][$key]
                    : null;
                $builder->add($key, $this->getFormType(), $this->getFormOptions($value['name'], $value['value'], $parameterData));
            }
        }

        // Добавление фильтра по цене
        if ($options['usePriceFilter'] && !empty($options['priceFilter'])) {
            $builder->add('price', 'text', array(
                'label' => 'selector.parameters.price.title',
                'data'  => (count($options['priceFilter']) > 0)
                    ? (array_values($options['priceFilter'])[0] . ';' . end($options['priceFilter']))
                    : '0;0',
            ));
        }

        $builder->add('submit', 'submit', array(
            'label' => 'selector.parameters.form.submit',
        ));
    }

    /**
     * {@ineritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['key'] = $options['key'];
    }

    /**
     * {@ineritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'required'           => false,
            'translation_domain' => 'NitraProductBundle',
            'optSel'             => array(),
            'params'             => array(),
            'brands'             => null,
            'key'                => null,
            'categoryName'       => null,
            // Использовать ценовой фильтр
            'usePriceFilter'     => true,
            // Массив цен фильтра
            'priceFilter'        => array(),
        ));
    }

    /**
     * {@ineritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    protected function getFormType()
    {
        return $this->useSelect2
            ? 'genemu_jqueryselect2_choice'
            : 'choice';
    }

    /**
     * @param string $field
     * @param array  $options
     *
     * @return array
     */
    protected function getFormOptions($label, $choices, $data)
    {
        $formOptions = array(
            'label'         => $label,
            'choices'       => $choices,
            'empty_value'   => '',
            'data'          => $data,
        );

        if ($this->useSelect2) {
            $formOptions['configs'] = array(
                'placeholder'   => null,
                'width'         => 200,
                'allowClear'    => true,
            );
        }

        return $formOptions;
    }
}