<?php

namespace Nitra\ProductBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\StoreBundle\Helper\nlActions;

class Filter
{
    /** @var ContainerInterface Container instance */
    protected $container;

    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /** @var \Doctrine\Common\Cache\ApcCache */
    protected $cache;

    /** @var integer */
    protected $total;
    /** @var array */
    protected $amounts;
    /** @var array */
    protected $query;
    /** @var array */
    protected $filtered;
    /** @var array */
    protected $data;

    /** @var \Nitra\ProductBundle\Document\Category */
    protected $category;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dm        = $container->get('doctrine_mongodb.odm.document_manager');
        $this->cache     = $container->get('cache_apc');
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param \Doctrine\ODM\MongoDB\Query\Builder    $qb
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function filter($category, $qb)
    {
        $this->category = $category;

        $this->data     = $this->getFilterData($qb);
        $this->query    = $this->transformRequestQuery($this->container->get('request')->query->all());
        $this->filtered = $this->filterProducts($this->data, $this->query);
        $this->amounts  = $this->calcAmounts($this->filtered);

        foreach (array_keys($this->query) as $id) {
            if ($id == 'price-from' || $id == 'price-to') {
                continue;
            }
            $subQuery = $this->query;
            unset($subQuery[$id]);
            $subFiltered = $this->filterProducts($this->data, $subQuery);
            $this->amounts = $this->calcAmounts($subFiltered, $id) + $this->amounts;
        }

        $mode = $this->container->getParameter('nitra.filter.mode');

        switch ($mode) {
            case 'product':
                $resultQb = $this->dm->getRepository('NitraProductBundle:Product')
                    ->createQueryBuilder()
                    ->field('id')->in(array_keys($this->filtered));
                break;
            case 'model':
                $ids = $this->dm->getRepository('NitraProductBundle:Product')
                    ->createQueryBuilder()
                    ->distinct('model.$id')
                    ->field('id')->in(array_keys($this->filtered))
                    ->getQuery()
                    ->execute()
                    ->toArray();
                $resultQb = $this->dm->getRepository('NitraProductBundle:Model')
                    ->createQueryBuilder()
                    ->field('id')->in($ids);
                break;
            default:
                throw new \Exception(sprintf('Mode "%s" is not defined', $mode));
                break;
        }

        $this->total    = $resultQb->count()->getQuery()->execute();

        return $resultQb->find();
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param \Closure|null $pathGenerator Callback for generate path
     * @param string        $route
     *
     * @return array
     */
    public function getTemplateContext($route = 'category_page')
    {
        $request = $this->container->get('request');

        $parameters = $this->dm
            ->getRepository('NitraProductBundle:Category')
            ->getFilterParameters($this->category, false);

        $routeParameters = array_merge(
            $request->query->all(),
            $request->attributes->get('_route_params')
        );
        $path = $this->container->get('router')->generate($route, $request->attributes->get('_route_params'));

        $params = $routeParameters;

        foreach ($this->container->get('router')->getRouteCollection()->get($route)->getDefaults() as $key => $value) {
            if (array_key_exists($key, $params)) {
                unset($params[$key]);
            }
        }
        unset($params['_locale'], $params['page']);

        foreach ($params as &$param) {
            $param = explode(',', $param);
        }
        $brandFilter = array(
            'name'         => 'filters.brand',
            'alias'        => 'brand',
            'suffix'       => null,
            'active'       => array_key_exists('brand', $params),
            'isFilterShow' => true,
        );
        $brands = $this->dm->createQueryBuilder('NitraProductBundle:Brand')
            ->hydrate(false)
            ->select(array(
                'aliasEn',
                'name',
            ))
            ->field('id')->in(array_keys($this->amounts))
            ->getQuery()
            ->execute();
        $hasAmountValues       = false;
        $brandFilter['values'] = array();
        foreach ($brands as $id => $brand) {
            $brandFilter['values'][$id] = array(
                'active'    => isset($params['brand']) && in_array($brand['aliasEn'], $params['brand']),
                'alias'     => $brand['aliasEn'],
                'name'      => $brand['name'],
                'order'     => 0,
                'intAmount' => $this->amounts[$id],
                'amount'    => ' (' . ($brandFilter['active'] ? '+' : '') . $this->amounts[$id] . ')',
                'link'      => $this->getValueLink($path, $params, 'brand', $brand['aliasEn']),
            );
            $hasAmountValues = $this->amounts[$id] ? true : $hasAmountValues;
        }
        $brandFilter['hasAmountValues'] = $hasAmountValues;

        $this->sortFilterValues($brandFilter['values']);

        $filters   = array();
        $filters[] = $brandFilter;
        foreach ($parameters as $parameter) {
            $filter = array(
                'name'         => $parameter['name'],
                'alias'        => $parameter['aliasEn'],
                'suffix'       => isset($parameter['suffix']) ? $parameter['suffix'] : null,
                'active'       => isset($params[$parameter['aliasEn']]),
                'isFilterShow' => isset($parameter['isFilterShow']) ? $parameter['isFilterShow'] : false,
            );
            $hasAmountValues  = false;
            $filter['values'] = array();
            $values = $this->dm
                ->createQueryBuilder('NitraProductBundle:ParameterValue')
                ->hydrate(false)
                ->field('parameter.id')->equals($parameter['_id'])
                ->getQuery()
                ->execute()
                ->toArray();
            foreach ($values as $id => $value) {
                if (!isset($this->amounts[$id])) {
                    continue;
                }

                $filter['values'][$id] = array(
                    'active'    => isset($params[$parameter['aliasEn']]) && in_array($value['aliasEn'], $params[$parameter['aliasEn']]),
                    'alias'     => $value['aliasEn'],
                    'name'      => $value['name'],
                    'order'     => isset($value['sortOrder']) ? $value['sortOrder'] : 0,
                    'intAmount' => $this->amounts[$id],
                    'amount'    => ' (' . ($filter['active'] ? '+' : '') . $this->amounts[$id] . ')',
                    'link'      => $this->getValueLink(
                        $path,
                        $params,
                        $parameter['aliasEn'],
                        $value['aliasEn'],
                        isset($parameter['isSiteMultiple']) ? $parameter['isSiteMultiple'] : false
                    ),
                );
                $hasAmountValues = $this->amounts[$id] ? true : $hasAmountValues;
            }

            $filter['hasAmountValues'] = $hasAmountValues;
            $this->sortFilterValues($filter['values']);
            $filters[] = $filter;
        }

        $prices = array_map(function ($product) {
            return $product['price'];
        }, $this->data);

        return array(
            'prices'      => array(
                'min'  => $prices ? min($prices) : 0,
                'max'  => $prices ? max($prices) : 0,
                'from' => isset($this->query['price-from']) ? $this->query['price-from'] : null,
                'to'   => isset($this->query['price-to'])   ? $this->query['price-to']   : null,
            ),
            'filters'     => $filters,
            'total'       => $this->total,
            'route'       => $route,
            'selectedBox' => $this->container->getParameter('nitra.filter.selected_box'),
        );
    }

    /**
     *
     * @param type $path
     * @param type $params
     * @param type $pAlias
     * @param type $vAlias
     * @param boolean $multiple
     *
     * @return string
     */
    protected function getValueLink($path, $params, $pAlias, $vAlias, $multiple = true)
    {
        $newParams = array();
        foreach ($params as $id => $param) {
            $newParams[$id] = $param;
        }

        if ($multiple && isset($newParams[$pAlias])) {
            if (($key = array_search($vAlias, $newParams[$pAlias])) !== false) {
                unset ($newParams[$pAlias][$key]);
                if (!$newParams[$pAlias]) {
                    unset($newParams[$pAlias]);
                }
            } else {
                $newParams[$pAlias][] = $vAlias;
            }
        } elseif (isset($newParams[$pAlias]) && $vAlias == $newParams[$pAlias]) {
            unset($newParams[$pAlias]);
        } else {
            $newParams[$pAlias] = array($vAlias);
        }

        foreach ($newParams as &$newParam) {
            $newParam = implode(',', $newParam);
        }

        return urldecode($path . ($newParams ? ('?' . http_build_query($newParams)) : null));
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Query\Builder    $qb
     *
     * @return array
     */
    protected function getFilterData($qb)
    {
        $data = $qb
            ->hydrate(false)
            ->select(array(
                'parameters.parameter',
                'parameters.values',
                'model',
                'storePrice',
            ))
            ->getQuery()
            ->execute()
            ->toArray();

        $allowedIds = $this->dm
            ->getRepository('NitraProductBundle:Category')
            ->getAllowedParametersIds($this->category);

        $products = array();
        $models   = array();

        foreach ($data as $id => $product) {
            $modelId = (string) $product['model']['$id'];
            if (!array_key_exists($modelId, $models)) {
                $models[$modelId] = 0;
            }
            $models[$modelId] ++;

            if (!isset($product['storePrice'][Globals::getStore()['id']]['discount'])) {
                $product['storePrice'][Globals::getStore()['id']]['discount'] = 0;
            }

            $productPrice    = $product['storePrice'][Globals::getStore()['id']]['price'];
            $productDiscount = $product['storePrice'][Globals::getStore()['id']]['discount'];
            $actionDiscount  = nlActions::getDiscountByProductId($id, $productPrice);
            $resultDiscount  = $productDiscount > $actionDiscount ? $productDiscount : $actionDiscount;
            $products[$id]   = array(
                'modelId'    => $modelId,
                'parameters' => array(),
                'price'      => $productPrice * (100 - $resultDiscount) / 100,
            );
            if (!array_key_exists('parameters', $product)) {
                continue;
            }
            foreach ($product['parameters'] as $productParameter) {
                $parameterId = (string) $productParameter['parameter'];
                if (!in_array($parameterId, $allowedIds)) {
                    continue;
                }
                $products[$id]['parameters'][$parameterId] = array();
                foreach ($productParameter['values'] as $value) {
                    $products[$id]['parameters'][$parameterId][] = (string) $value;
                }
            }
        }

        $modelsToBrands = $this->dm->getRepository('NitraProductBundle:Model')
            ->createQueryBuilder()
            ->hydrate(false)
            ->select(array(
                'brand',
            ))
            ->field('id')->in(array_keys($models))
            ->getQuery()
            ->execute()
            ->toArray();

        foreach ($products as &$product) {
            $product['parameters']['brand'] = array(
                (string) $modelsToBrands[$product['modelId']]['brand']['$id'],
            );
        }

        return $products;
    }

    /**
     * @param array $query
     *
     * @return array
     */
    protected function transformRequestQuery(array $query)
    {
        $cRepo = $this->dm
            ->getRepository('NitraProductBundle:Category');
        $pRepo = $this->dm
            ->getRepository('NitraProductBundle:Parameter');
        $vRepo = $this->dm
            ->getRepository('NitraProductBundle:ParameterValue');

        $ids = $cRepo->getAllowedParametersIds($this->category);

        $result = array();

        foreach ($query as $pAlias => $vAliases) {
            if ($pAlias === 'brand') {
                $brands = $this->dm
                    ->createQueryBuilder('NitraProductBundle:Brand')
                    ->hydrate(false)
                    ->select(array(
                        '_id',
                    ))
                    ->field('aliasEn')->in(explode(',', $vAliases))
                    ->getQuery()
                    ->execute()
                    ->toArray();

                if ($brands) {
                    $result['brand'] = array_keys($brands);
                }
                continue;
            } elseif ($pAlias == 'price-from' || $pAlias == 'price-to') {
                $result[$pAlias] = (int) $vAliases;
                continue;
            }

            $parameter = $pRepo->createQueryBuilder()
                ->field('aliasEn')->equals($pAlias)
                ->field('id')->in($ids)
                ->getQuery()
                ->getSingleResult();
            if ($parameter) {
                $values = $vRepo->createQueryBuilder()
                    ->field('aliasEn')->in(explode(',', $vAliases))
                    ->field('parameter.id')->equals($parameter->getId())
                    ->distinct('_id')
                    ->getQuery()
                    ->execute()
                    ->toArray();
                if ($values) {
                    $result[$parameter->getId()] = array_map(function($valueId) {
                        return (string) $valueId;
                    }, $values);
                }
            }
        }

        return $result;
    }

    /**
     * @param array       $data
     * @param null|string $parameterId
     *
     * @return array
     */
    protected function calcAmounts($data, $parameterId = null)
    {
        $result = array();
        foreach ($data as $parameters) {
            foreach ($parameters['parameters'] as $id => $values) {
                if (!is_null($parameterId) && ($id != $parameterId)) {
                    continue;
                }
                foreach ($values as $value) {
                    if (!array_key_exists($value, $result)) {
                        $result[$value] = 0;
                    }
                    $result[$value] ++;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @param array $query
     *
     * @return array
     */
    protected function filterProducts($data, $query)
    {
        return array_filter($data, function($product) use ($query) {
            foreach ($query as $parameter => $values) {
                if (($parameter == 'price-to') && ($product['price'] > (int) $values)) {
                    return false;
                } elseif (($parameter == 'price-from') && ($product['price'] < (int) $values)) {
                    return false;
                }
                if (($parameter != 'price-to') && ($parameter != 'price-from')) {
                    if (!array_key_exists($parameter, $product['parameters'])) {
                        return false;
                    } else {
                        $hasOne = false;
                        foreach ($values as $value) {
                            if (in_array($value, $product['parameters'][$parameter])) {
                                $hasOne = true;
                            }
                        }
                        if (!$hasOne) {
                            return false;
                        }
                    }
                }
            }

            return true;
        });
    }

    /**
     * Sort filter values by order and name
     *
     * @param array $values
     */
    protected function sortFilterValues(&$values)
    {
        $valuesMultiSort = array(
            'order' => array(),
            'name'  => array(),
        );
        $nonNumeric = 0;
        foreach ($values as $key => $value) {
            $valuesMultiSort['order'][$key] = array_key_exists('order', $value) && is_numeric($value['order'])
                ? $value['order']
                : 0;
            // if first symbol from value name is non numeric
            if (!is_numeric($value['name'][0])) {
                $nonNumeric ++;
                $valuesMultiSort['name'][$key] = $value['name'];
            } else {
                $valuesMultiSort['name'][$key] = (float) $value['name'];
            }
        }

        // sort as numeric, if amount of non numeric values less half of all values
        $namesIsNumeric = $nonNumeric < count($values) / 2;
        array_multisort(
            $valuesMultiSort['order'], SORT_NUMERIC,
            $valuesMultiSort['name'],  $namesIsNumeric ? SORT_NUMERIC : SORT_ASC,
            $values
        );
    }
}