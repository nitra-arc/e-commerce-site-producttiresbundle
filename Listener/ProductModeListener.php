<?php

namespace Nitra\ProductBundle\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\StoreBundle\Helper\nlActions;

/**
 * Post load doctrine event listener for products and models
 * For set full and short names by current store
 */
class ProductModeListener
{
    /** @var ContainerInterface */
    protected $container;

    /** @var string Url to home page */
    protected $homePath;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function getHomePath()
    {
        if ($this->homePath) {
            return $this->homePath;
        }

        $this->homePath = $this->container
            ->get('router')
            ->generate('nitra_store_home_index', array());

        return $this->homePath;
    }

    /**
     * Post load doctrine event handler
     *
     * @param \Doctrine\ODM\MongoDB\Event\LifecycleEventArgs $event
     *
     * @return null
     */
    public function postLoad(LifecycleEventArgs $event)
    {
        $document  = $event->getDocument();

        $isProduct = $document instanceof \Nitra\ProductBundle\Document\Product;
        $isModel   = $document instanceof \Nitra\ProductBundle\Document\Model;

        if (!$isModel && !$isProduct) {
            return;
        }

        $store   = Globals::getStore();
        $storeId = $store['id'];

        $this->setFullName($document, $storeId);
        $this->setShortName($document, $storeId);

        if ($isProduct) {
            $this->setDiscount($document);
            $this->setPrice($document, $storeId);
            $this->setBadge($document);

            $document->setUrl($this->getHomePath() . $document->getFullUrlAliasEn());
        }
    }

    /**
     * Set full name to product or model from names array
     *
     * @param \Nitra\ProductBundle\Document\Product|\Nitra\ProductBundle\Document\Model $item
     * @param string                                                                    $storeId
     */
    protected function setFullName($item, $storeId)
    {
        if (array_key_exists($storeId, $item->getFullNames())) {
            $item->setFullName($item->getFullNames()[$storeId]);
        }
    }

    /**
     * Set short name to product or model from names array
     *
     * @param \Nitra\ProductBundle\Document\Product|\Nitra\ProductBundle\Document\Model $item
     * @param string                                                                    $storeId
     */
    protected function setShortName($item, $storeId)
    {
        if (array_key_exists($storeId, $item->getShortNames())) {
            $item->setShortName($item->getShortNames()[$storeId]);
        }
    }

    /**
     * Set price to product from storePrice array
     *
     * @param \Nitra\ProductBundle\Document\Product $item
     * @param string                                $storeId
     */
    protected function setPrice($item, $storeId)
    {
        $storePrice = $item->getStorePrice();
        $price = array_key_exists($storeId, $storePrice) && array_key_exists('price', $storePrice[$storeId])
            ? $storePrice[$storeId]['price']
            : 0;

        $round      = Globals::getPriceRound();
        $converted  = $this->convertPrice($price);
        $discounted = $converted * (100 - $item->getDiscount()) / 100;

        $item->setOriginalPrice(round($converted, $round));
        $item->setPrice(round($discounted, $round));
    }

    /**
     * Convert price by currency exchange
     *
     * @param float $price
     *
     * @return float
     */
    protected function convertPrice($price)
    {
        $exchange       = Globals::getCurrency('exchange');
        $baseExchange   = Globals::getBaseCurrency('exchange');

        return $price * $baseExchange / $exchange;
    }

    /**
     * Set discount to product from storePrice array
     *
     * @param \Nitra\ProductBundle\Document\Product $item
     */
    protected function setDiscount($item)
    {
        $item->setDiscount((int) nlActions::getProductDiscount($item));
    }

    /**
     * Set badge to product
     * if badge not specified obviously
     * set if product has action and action has badge
     * set if product has discount get default badge from store settings
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function setBadge($product)
    {
        $badge = $product->getBadge();

        // if product has action and action has badge
        if (!$badge &&
            ($productAction = nlActions::getProductAction($product)) &&
            $productAction->getBadge()
        ) {
            $badge = $productAction->getBadge();
        }

        // if product has discount
        if (!$badge && $product->getDiscount()) {
            // get default badge from store settings
            $badge = Globals::getDiscountBage();
        }

        // set badge
        $product->setBadge($badge);
    }
}