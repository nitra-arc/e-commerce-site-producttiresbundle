<?php

namespace Nitra\ProductBundle\Controller\Selector;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SelectionController extends NitraController
{
    protected $autoSelectorForm  = '\Nitra\ProductBundle\Form\Type\Selector\Auto%sType';
    protected $paramSelectorForm = '\Nitra\ProductBundle\Form\Type\Selector\ParamSelectorType';

    protected $tiresWidth;
    protected $tiresProfile;
    protected $tiresDiameter;
    protected $wheelsWidth;
    protected $wheelsDiameter;
    protected $wheelsET;
    protected $optSel = array();

    /**
     * @param string $alias
     *
     * @return \Nitra\SelectionBundle\Document\Vendor
     */
    protected function getVendor($alias)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Vendor')
            ->field('alias')->equals($alias)
            ->getQuery()
            ->execute()
            ->getSingleResult();
    }

    /**
     * @param string $alias
     *
     * @return \Nitra\SelectionBundle\Document\Category
     */
    protected function getCategory($alias)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->field('aliasEn')->equals($alias)
            ->getQuery()
            ->execute()
            ->getSingleResult();
    }

    /**
     * @Template("NitraProductBundle:Selector:fullSelector.html.twig")
     *
     * @param Request $request
     * @param string  $redirect
     * @param boolean $useSelect2
     * @param boolean $priceFilter
     * @param boolean $brandFilter
     * @param integer $limit
     * @param string  $autoType
     *
     * @return array
     */
    public function selectorFullAction(Request $request, $redirect, $useSelect2 = false, $priceFilter = true, $brandFilter = true, $limit = -1, $autoType = 'simple')
    {
        $filters = $this->selectorByParamAction($request, null, $useSelect2, $priceFilter, $brandFilter, $limit);
        $autos   = array();

        foreach (array_keys($filters['forms']) as $key) {
            $autos[$key] = $this->getSelectorByAutoForm($autoType, $key, $useSelect2)->createView();
        }

        return array(
            'filter'     => $filters,
            'auto'       => $autos,
            'redirect'   => $redirect,
            'useSelect2' => $useSelect2,
        );
    }

    /**
     * @Route("/selector/{type}/{key}/{change}", name="selector", defaults={"type"="radio", "key"=null, "change"=null}, requirements={"key"="[^/]*"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string                                    $type         Тип формы (радио баттоны, табы..)
     * @param string                                    $key          Доп имя формы
     * @param string                                    $category     Id категории
     * @param bool                                      $useSelect2   Использовать select2
     * @param string                                    $selectedTwig Шаблон Используемый в случае если автомобиль уже выбран
     * @param mixed                                     $change       Для рендера подборщика без учета выбран или нет
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function selectorByAutoFormAction(Request $request, $type = 'radio', $key = null, $redirect = null, $category = null, $useSelect2 = true, $selectedTwig = null, $change = null)
    {
        $cacheKey = $this->mapCacheKey('selector_by_auto_options');
        if ($change && $this->getCache()->contains($cacheKey)) {
            $cached       = $this->getCache()->fetch($cacheKey);
            $redirect     = $cached['redirect'];
            $category     = $cached['category'];
            $useSelect2   = $cached['useSelect2'];
            $selectedTwig = $cached['selectedTwig'];
        }
        $this->getCache()->save($cacheKey, array(
            'type'         => $type,
            'key'          => $key,
            'redirect'     => $redirect,
            'category'     => $category,
            'useSelect2'   => $useSelect2,
            'selectedTwig' => $selectedTwig,
        ));
        $form = $this->getSelectorByAutoForm($type, $key, $useSelect2, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $redirectResponse = $this->handleSelectorForm($form);
            if (!$request->isXmlHttpRequest()) {
                return $redirectResponse;
            } elseif (!$category) {
                $category = $form->get('category')->getData();
            }
        }
        if ($request->getSession()->has('selector_selected_vendor') && ($change === null)) {
            $parameters = $this->getSelectorTemplateParameters($redirect, $category);

            return $this->render($selectedTwig ? : 'NitraProductBundle:Selector\Auto:selected.html.twig', $parameters);
        }

        return $this->render($this->getSelectorByAutoTemplate($type), array_merge(
            array(
                'type'       => $type,
                'key'        => $key,
                'useSelect2' => $useSelect2,
            ),
            isset($parameters)
                ? $parameters
                : array(
                    'form' => $form->createView(),
                )
        ));
    }

    /**
     * @Template("NitraProductBundle:Selector\Auto:changeAuto.html.twig")
     *
     * @return array
     */
    public function changeAutoAction()
    {
        $cache    = $this->getCache();
        $cacheKey = $this->mapCacheKey('selector_by_auto_options');
        $options  = $cache->contains($cacheKey)
            ? $cache->fetch($cacheKey)
            : array('type' => 'radio');

        return array(
            'options' => $options + array('change' => 1),
        );
    }

    /**
     * @Route("/selector-clear", name="selector-clear")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function clearSelectorByAutoAction(Request $request)
    {
        if ($request->getSession()->has('selector_selected_vendor')) {
            $request->getSession()->remove('selector_selected_vendor');
        }

        $redirect = $request->query->get('redirect', $this->get('router')->getGenerator()->getContext()->getBaseUrl());

        return new RedirectResponse(preg_match('/^\//', $redirect)
            ? $redirect
            : $this->generateUrl('category_product_page', array(
                'slug' => $redirect,
            )
        ));
    }

    /**
     * @Template("NitraProductBundle:Selector\Auto:categoryComplectation.html.twig")
     *
     * @param Request $request
     * @param string  $categoryId
     *
     * @return array
     *
     * @throws NotFoundHttpException
     */
    public function categoryComplectationAction(Request $request, $categoryId)
    {
        if (!$request->getSession()->has('selector_selected_vendor')) {
            throw new NotFoundHttpException("You must change automobile");
        }
        $categories = $this->getCategories()[$categoryId];

        list ($alias, $categoriesType, $combination) = array_values($request->getSession()->get('selector_selected_vendor'));

        $vendor       = $this->getVendor($alias);
        $combinations = $this->getVendorCombinations($vendor, $categoriesType, $categories);

        return array(
            'complectation' => $combinations,
            'active'        => $combination,
            'vendor'        => $vendor,
            'category'      => $categoryId,
        );
    }

    /**
     * @param string $type
     *
     * @return string
     */
    protected function getSelectorByAutoTemplate($type)
    {
        return sprintf('NitraProductBundle:Selector\Auto:%s.html.twig', $type);
    }

    /**
     * @param string    $type
     * @param int|null  $index
     * @param bool      $useSelect2
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function getSelectorByAutoForm($type, $index = null, $useSelect2 = true)
    {
        $form = sprintf($this->autoSelectorForm, ucfirst($type));

        return $this->createForm(new $form('auto_selector_' . $index, $useSelect2), null, array(
            'categories' => $this->getCategories(),
            'index'      => $index,
            'vendors'    => $this->getVendors(),
            'action'     => $this->generateUrl('selector', array(
                'type' => $type,
                'key'  => $index,
            )),
            'attr'       => array(
                'id' => 'auto_selector_' . $index,
            ),
        ));
    }

    /**
     * @param string $redirect
     * @param string $categoryId
     *
     * @return array
     */
    protected function getSelectorTemplateParameters($redirect, $categoryId)
    {
        list($alias, $categoriesType) = array_values($this->getRequest()->getSession()->get('selector_selected_vendor'));

        $vendor       = $this->getVendor($alias);
        $categories   = $this->getCategories();
        $cats         = $categoryId ? $categories[$categoryId] : $categories[$categoriesType];
        $combinations = $this->getVendorCombinations($vendor, $categoryId ?: $categoriesType, $cats);

        return array(
            'combinations' => $combinations,
            'redirect'     => $redirect,
            'categories'   => $cats,
            'selected'     => $vendor,
            'category'     => $categoryId ?: $categoriesType,
        );
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return RedirectResponse
     *
     * @throws NotFoundHttpException
     */
    protected function handleSelectorForm($form)
    {
        $data         = $this->getRequest()->request->get($form->getName());
        $vendorObject = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Vendor')
            ->field('name')->equals($data['vendor'])
            ->field('vehicle')->equals($data['vehicle'])
            ->field('year')->equals($data['year'])
            ->field('modification')->equals($data['modification'])
            ->getQuery()
            ->execute()
            ->getSingleResult();

        if (!$vendorObject) {
            throw new NotFoundHttpException(sprintf(
                'Vendor by name "%s", vehicle "%s", year "%s" and modification "%s" not found',
                $data['vendor'],
                $data['vehicle'],
                $data['year'],
                $data['modification']
            ));
        }

        $vendor = array(
            'alias'       => $vendorObject->getAlias(),
            'categories'  => $data['category'],
            'combination' => 0,
        );
        $this->getRequest()->getSession()->set('selector_selected_vendor', $vendor);

        $id       = array_values($this->getStore()["{$data['category']}TabCategories"])[0];
        $category = $this->getDocumentManager()->find('NitraProductBundle:Category', $id);

        return new RedirectResponse($this->generateUrl('selection', array(
            'type'        => $data['category'],
            'alias'       => $vendorObject->getAlias(),
            'category'    => $category->getAlias(),
            'combination' => 0,
        )));
    }

    /**
     * @param \Nitra\ProductBundle\Document\Vendor     $vendor
     * @param string                                   $tORw
     * @param \Nitra\ProductBundle\Document\Category[] $categories
     *
     * @return array
     */
    protected function getVendorCombinations($vendor, $tORw, $categories)
    {
        $cache = $this->getCache();

        $key   = $this->mapCacheKey('selector_vendor_combinations');
        $key  .= sprintf("_%s_%s", implode('_', array_map(function($cat) { return $cat->getId(); }, $categories)), $vendor->getAlias());

        if ($cache->contains($key)) {
            $combinations = $cache->fetch($key);
        } else {
            $tws = $vendor->{'get' . ucfirst($tORw)}();
            if ($tORw == 'tires') {
                $this->tiresWidth       = $this->getParameter('Ширина',  $categories);
                $this->tiresProfile     = $this->getParameter('Профиль', $categories);
                $this->tiresDiameter    = $this->getParameter('Диаметр', $categories);
            } else {
                $this->wheelsWidth      = $this->getParameter('Ширина',  $categories);
                $this->wheelsDiameter   = $this->getParameter('Диаметр', $categories);
                $this->wheelsET         = $this->getParameter('ET',      $categories);
            }

            $combinations = array();
            foreach ($tws as $type => $tw) {
                $fbs = (key_exists('all', $tw) && $tw['all']) ? array('all') : array('front', 'back');
                if (!key_exists($type, $combinations)) {
                    $combinations[$type] = array();
                }

                foreach ($fbs as $fb) {
                    if (key_exists($fb, $tw)) {
                        if (!key_exists($fb, $combinations[$type])) {
                            $combinations[$type][$fb] = array();
                        }
                        foreach ($tw[$fb] as $ind => $combin) {
                            $names = array();
                            $parameters = array();
                            foreach ($combin as $parameterId => $valueId) {
                                $parameter = $this->getDocumentManager()->find('NitraProductBundle:Parameter', $parameterId);
                                $value = null;
                                if ($parameter) {
                                    foreach ($parameter->getParameterValues() as $val) {
                                        if ($val->getId() == $valueId) {
                                            $value = $val;
                                            break;
                                        }
                                    }
                                }
                                if ($parameter && $value) {
                                    $names[$parameterId] = $value->getName();
                                    $parameters[$parameter->getAlias()] = $value->getAlias();
                                }
                            }
                            $translator = $this->get('translator');
                            $key = ($tORw == 'tires')
                                ? $translator->trans(
                                    'selector.auto.tires_format',
                                    array(
                                        '%width%'    => $names[$this->tiresWidth],
                                        '%profile%'  => $names[$this->tiresProfile],
                                        '%diameter%' => $names[$this->tiresDiameter],
                                    ),
                                    'NitraProductBundle'
                                )
                                : $translator->trans(
                                    'selector.auto.wheels_format',
                                    array(
                                        '%width%'    => $names[$this->wheelsWidth],
                                        '%diameter%' => $names[$this->wheelsDiameter],
                                        '%et%'       => $names[$this->wheelsET],
                                    ),
                                    'NitraProductBundle'
                                );
                            $combinations[$type][$fb][$ind][$key] = $parameters;
                        }
                    }
                }
            }

            foreach ($combinations as $key => $val) {
                if (!$val) {
                    unset($combinations[$key]);
                }
            }
        }

        return $combinations;
    }

    /**
     * @param string                                   $name
     * @param \Nitra\ProductBundle\Document\Category[] $categories
     *
     * @return \Nitra\ProductBundle\Document\Parameter
     */
    protected function getParameter($name, $categories)
    {
        $parameter = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Parameter')
            ->field('name')->equals(new \MongoRegex("/$name/i"))
            ->field('categories.id')->in(array_map(function ($cat) {
                return $cat->getId();
            }, $categories))
            ->getQuery()
            ->execute()
            ->getSingleResult();

        return $parameter ? $parameter->getId() : null;
    }

    /**
     * Получение значений автомобилей (получить все модели, года или модификации)
     * отсеяных без товаров
     *
     * @param string    $property               Что получать
     * @param bool      $checkExistsProducts    Проверять ли наличие товаров
     * @param array     $parameters             Фильтра автомобилей
     *
     * @return array
     */
    protected function getSelectorValues($property, $checkExistsProducts, array $parameters)
    {
        $tORw       = $this->getRequest()->request->get('category_id');
        $categories = $this->getCategories()[$tORw];

        $result = array();
        if (count($categories)) {
            foreach ($categories as $category) {
                $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Vendor');
                foreach ($parameters as $field => $value) {
                    $qb->field($field)->equals($value);
                }
                $vendors = $qb->getQuery()->execute();

                foreach ($vendors as $vendor) {
                    $tiresOrWheels  = $vendor->{'get' . ucfirst($tORw)}();
                    $amount         = $checkExistsProducts
                        ? $this->existsVendorProducts($category, $tiresOrWheels)
                        : 1;
                    if ($tiresOrWheels && ($amount > 0)) {
                        $value = $vendor->{'get' . ucfirst($property)}();
                        $result[$value] = $value;
                    }
                }
            }
            asort($result, SORT_ASC);
        }

        return $result;
    }

    /**
     * Проверка - есть ли товары по переданным параметрам
     *
     * @param array $Categories     Категории, в которых искать товары
     * @param array $tiresOrWheels  Параметры автомобиля для категории
     *
     * @return integer Количество товаров
     */
    protected function existsVendorProducts($Categories, $tiresOrWheels)
    {
        $qb = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('category.$id')->in(array_map(function($cat) {
                return $cat->getId();
            }, $Categories));
        $hasCombinations = false;

        foreach ($tiresOrWheels as $tireOrWheel) {
            foreach ($tireOrWheel as $combinations) {
                foreach ($combinations as $combination) {
                    $expr = $qb->expr();
                    foreach ($combination as $parameterId => $valueId) {
                        $expr->addAnd(
                            $qb ->expr()
                                ->field('parameters')->elemMatch(
                                    $qb ->expr()
                                        ->field('_id')->equals(new \MongoId($parameterId))
                                        ->field('isUsed')->notEqual(false)
                                        ->field('parameterValues._id')->equals(new \MongoId($valueId))
                            )
                        );
                        $hasCombinations = true;
                    }
                    $qb->addOr($expr);
                }
            }
        }

        return $hasCombinations
            ? $qb->getQuery()->execute()->count()
            : 0;
    }

    /**
     * Метод для получения конфига для проверки ко-ва товаров
     *
     * @param string $param Поле из конфига
     *
     * @return boolean
     */
    protected function getVendorsConfigForCheckExistsProducts($param)
    {
        return $this->container->getParameter('nitra_selectors')['check_exists_products'][$param];
    }

    /**
     * @Route("/selector-vehicles", name="selector-vehicles")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function vehiclesAction(Request $request)
    {
        $vehicles = $this->getSelectorValues('vehicle', $this->getVendorsConfigForCheckExistsProducts('vehicle'), array(
            'name' => $request->get('vendor'),
        ));

        return new JsonResponse(array_values($vehicles));
    }


    /**
     * @Route("/selector-years", name="selector-years")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function yearsAction(Request $request)
    {
        $years = $this->getSelectorValues('year', $this->getVendorsConfigForCheckExistsProducts('year'), array(
            'name'      => $request->get('vendor'),
            'vehicle'   => $request->get('vehicle'),
        ));

        return new JsonResponse(array_values($years));
    }

    /**
     * @Route("/selector-modification", name="selector-modification")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function modificationAction(Request $request)
    {
        $modifications = $this->getSelectorValues('modification', $this->getVendorsConfigForCheckExistsProducts('modification'), array(
            'name'      => $request->get('vendor'),
            'vehicle'   => $request->get('vehicle'),
            'year'      => $request->get('year'),
        ));

        return new JsonResponse(array_values($modifications));
    }

    /**
     * @return array
     */
    protected function getCategories()
    {
        $store  = $this->getStore();
        $result = array(
            'tires'  => $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Category')
                ->field('id')->in($store['tiresTabCategories'])
                ->sort('sortOrder', 1)
                ->getQuery()
                ->execute()
                ->toArray(),
            'wheels' => $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Category')
                ->field('id')->in($store['wheelsTabCategories'])
                ->sort('sortOrder', 1)
                ->getQuery()
                ->execute()
                ->toArray(),
        );

        return $result;
    }

    /**
     * @return array
     */
    protected function getVendors()
    {
        $cache  = $this->getCache();
        $key    = $this->mapCacheKey('selector_vendors');
        if ($cache->contains($key)) {
            $result = $cache->fetch($key);
        } else {
            $vendors = $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Vendor')
                ->distinct('name')
                ->getQuery()
                ->execute()
                ->toArray();

            asort($vendors, SORT_ASC);

            $result = array();
            foreach ($vendors as $vendor) {
                $result[$vendor] = $vendor;
            }

            $cache->save($key, $result, 60 * 60);
        }

        return $result;
    }






    /**
     * @Route("/param_selector/{select2}/{priceFilter}/{brandFilter}/{limit}", name="param_selector", defaults={"select2"=0, "priceFilter"=1, "brandFilter"=1, "limit"=-1})
     * @Template("NitraProductBundle:Selector:paramSelector.html.twig")
     *
     * @param Request $request
     * @param string  $type        табы, не табы....
     * @param bool    $select2     использоавть select2
     * @param bool    $priceFilter выводить ценовой фильтр
     * @param bool    $brandFilter выводить фильтр по брендам
     * @param integer $limit       кол-во параметров (без учета цены и бренда)
     *
     * @return array
     */
    public function selectorByParamAction(Request $request, $type = null, $select2 = 0, $priceFilter = 1, $brandFilter = 1, $limit = -1)
    {
        $tabs = array();
        $categories = $this->getCategories();
        foreach ($categories as $key => $cats) {
            $tabs[$key] = array(
                "{$key}TabCategories",
                "{$key}Selector",
                "categories" => $cats,
            );
        }
        if ($request && $request->isXmlHttpRequest()) {
            $requestData = $request->request;
            $formId      = $requestData->get('formId');
            $optSel      = $requestData->get('options');
            if ($optSel) {
                $this->optSel = $optSel;
            }
            if ($clearSelector = $requestData->get('clearSelector')) {
                $formId = $clearSelector;
            }
            $paramValArr[$formId] = $this->formatParamValue($tabs[$formId][0], $limit);
            $form = $this->getParamSelectorForm($formId, $tabs[$formId], $paramValArr, null, $select2, $priceFilter, $brandFilter, $limit);

            return $this->render('NitraProductBundle:Selector:paramForm.html.twig', array(
                'paramForm'       => $form->createView(),
                'key'             => $formId,
                'usePriceFilter'  => $priceFilter,
                'pricesRange'     => $paramValArr[$formId][3],
                'totalPriceRange' => $paramValArr[$formId][4],
            ));
        }

        $translator       = $this->get('translator');
        $priceRanges      = array();
        $totalPriceRanges = array();
        foreach ($tabs as $key => $value) {
            $paramValArr[$key]      = $this->formatParamValue($value[0], $limit);
            $priceRanges[$key]      = $paramValArr[$key][3];
            $totalPriceRanges[$key] = $paramValArr[$key][4];
            $paramForms[$key]       = $this->getParamSelectorForm($key, $value, $paramValArr, $translator->trans("selector.other.$key", array(), 'NitraProductBundle'), $select2, $priceFilter, $brandFilter, $limit);
            if ($request) {
                $paramForms[$key]->handleRequest($request);
                if ($paramForms[$key]->isSubmitted() && $paramForms[$key]->isValid()) {
                    return $this->parameterSelectorSubmited($paramForms[$key]->getData());
                }
            }
        }

        $forms = array();
        foreach ($paramForms as $key => $paramForm) {
            $forms[$key] = $paramForm->createView();
        }

        return array(
            'forms'            => $forms,
            'type'             => $type,
            'usePriceFilter'   => $priceFilter,
            'pricesRanges'     => $priceRanges,
            'totalPriceRanges' => $totalPriceRanges,
        );
    }

    /**
     * @param array   $tabCategories
     * @param integer $limit
     *
     * @return array
     */
    protected function formatParamValue($tabCategories, $limit)
    {
        $store     = $this->getStore();
        $tabCatIds = array();
        foreach ($store[$tabCategories] as $tabCategory) {
            $tabCatIds[] = new \MongoId($tabCategory);
        }
        $paramValArr = array();
        $params = $this->getTabParams($tabCatIds, $limit);
        foreach ($params as $value) {
            $paramValArr[$value->getId()] = array(
                'name'  => $value->getName(),
                'value' => array(),
            );
        }

        $modelIds = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->in($tabCatIds)
            ->getQuery()
            ->execute()
            ->toArray();

        $qb = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->hydrate(false)
            ->select('parameters', 'storePrice.' . $store['id'] . '.price')
            ->field('model.id')->in($modelIds);

        $withutPricesQb = clone $qb;
        $withutParamsQb = clone $qb;
        $this->qbHelper($qb);
        $this->qbHelper($withutPricesQb, false);

        // фильтр цен без учета параметров
        $totalPriceRange = $withutParamsQb
            ->distinct("storePrice.{$store['id']}.price")
            ->getQuery()->execute()->toArray();

        // получение цен без учета фильтра по цене
        $prices = $withutPricesQb
            ->distinct("storePrice.{$store['id']}.price")
            ->getQuery()->execute()->toArray();

        $collapsedPrices = $qb
            ->distinct("storePrice.{$store['id']}.price")
            ->getQuery()->execute()->toArray();

        $parameters = $qb
            ->distinct('parameters')
            ->getQuery()->execute()->toArray();
        foreach ($parameters as $parameter) {
            $key = $parameter['parameter']->__toString();
            if (in_array($key, array_keys($paramValArr)) && $parameter['values']) {
                $value = $this->getDocumentManager()->find('NitraProductBundle:ParameterValue', $parameter['values'][0]->__toString());
                if ($value) {
                    $paramValArr[$key]['value'][$value->getId()] = $value->getName();
                }
            }
        }
        foreach ($paramValArr as &$pv) {
            if (isset($pv['value'])) {
                natcasesort($pv['value']);
            }
        }

        sort($collapsedPrices, SORT_NUMERIC);
        sort($prices, SORT_NUMERIC);

        return array(
            $tabCatIds,
            $paramValArr,
            $collapsedPrices,
            $prices,
            // min/max фильтр цен без учета параметров
            array(
                'min' => $totalPriceRange ? min($totalPriceRange) : 0,
                'max' => $totalPriceRange ? max($totalPriceRange) : 0,
            ),
        );
    }

    /**
     * @param array $tabCatIds
     * @param int $limit
     * @return \Nitra\ProductBundle\Document\Parameter[]
     */
    protected function getTabParams($tabCatIds, $limit)
    {
        $dm = $this->getDocumentManager();
        $qb = $dm->createQueryBuilder('NitraProductBundle:Parameter')
            ->field('categories.id')->in($tabCatIds)
            ->field('isParameterSelector')->equals(true)
            ->field('isFilter')->equals(true)
            ->sort('sortOrderInParameterSelector', 1);

        if ($limit > 0) {
            $qb->limit($limit);
        }

        return $qb->getQuery()->execute();
    }

    protected function getBrands($tabCatIds)
    {
        $dm       = $this->getDocumentManager();

        $modelIdsFull = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->in($tabCatIds)
            ->getQuery()
            ->execute()
            ->toArray();

        $qb       = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in($modelIdsFull);
        $this->qbHelper($qb);
        $modelIds = $qb->distinct('model.$id')
            ->getQuery()
            ->execute()
            ->toArray();

        $brandIds = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('brand.$id')
            ->field('_id')->in($modelIds)
            ->getQuery()
            ->execute()
            ->toArray();
        $brands = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Brand')
            ->field('_id')->in($brandIds)
            ->sort('name', 1)
            ->getQuery()
            ->execute();

        $brandsName = array();
        foreach ($brands as $brand) {
            $brandsName[$brand->getId()] = $brand->getName();
        }

        return $brandsName;
    }

    /**
     * @param \Doctrine\DBAL\Query\QueryBuilder $qb
     * @param bool                              $withPrices
     */
    protected function qbHelper(&$qb, $withPrices = true)
    {
        if ($withPrices && $this->getRequest()->request->has('prices')) {
            $store = $this->getStore();
            $price = explode(';', $this->getRequest()->request->get('prices'));
            $qb ->field('storePrice.' . $store['id'] . '.price')
                ->gte((float) $price[0])
                ->lte((float) $price[1]);
        }
        if (isset($this->optSel['brands'])) {
            $modelIds = $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id')
                ->field('_id')->in($qb->getQueryArray()['model.$id']['$in'])
                ->field('brand.id')->equals($this->optSel['brands'])
                ->getQuery()
                ->execute()
                ->toArray();

            $qb->field('model.id')->in($modelIds);
        }
        $paramArr = $this->optSel;
        unset($paramArr['brands']);
        foreach ($paramArr as $key => $value) {
            if (trim($value)) {
                $qb->addAnd(
                    $qb->expr()
                        ->field('parameters')->elemMatch(
                            $qb->expr()
                                ->field('parameter')->equals(new \MongoId($key))
                                ->field('values')->equals(new \MongoId($value))
                        )
                );
            }
        }
    }

    /**
     * Создание формы
     *
     * @param string    $key                Ключ (id категории к примеру)
     * @param array     $data               Второй ключ
     * @param array     $paramValArr        Данные
     * @param string    $categoryName       Имя категории (таба)
     * @param bool      $select2            Импользовать select2
     * @param bool      $usePriceFilter     Использоавть ценовой фильтр
     * @param bool      $brandFilter        Использоавть фильтр по брендам
     * @param bool      $limit              Лимит параметров
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function getParamSelectorForm($key, $data, $paramValArr, $categoryName, $select2, $usePriceFilter, $brandFilter, $limit)
    {
        $prices = array();
        if ($this->getRequest()->request->has('prices')) {
            $priceTotal    = explode(';', $this->getRequest()->request->get('prices'));
            $priceSelected = explode(';', $this->getRequest()->request->get('priceSelected'));

            $prices = array(
                $priceSelected[0] > $priceTotal[0] ? $priceSelected[0] : $priceTotal[0],
                isset($priceSelected[1]) && $priceSelected[1] < $priceTotal[1] ? $priceSelected[1] : $priceTotal[1],
            );
        }

        if (!$prices) {
            $prices = $paramValArr[$key][2];
        }

        $form = $this->createForm(new $this->paramSelectorForm($data[1], $select2), null, array(
            'brands'         => $brandFilter ? $this->getBrands($paramValArr[$key][0]) : null,
            'params'         => $paramValArr[$key][1],
            'action'         => $this->generateUrl('param_selector', array(
                'select2'     => $select2 ? 1 : 0,
                'priceFilter' => $usePriceFilter ? 1 : 0,
                'brandFilter' => $brandFilter ? 1 : 0,
                'limit'       => $limit,
            )),
            'key'            => $key,
            'optSel'         => $this->optSel,
            'categoryName'   => $categoryName,
            'usePriceFilter' => $usePriceFilter,
            'priceFilter'    => $prices,
        ));

        return $form;
    }

    /**
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function parameterSelectorSubmited($parameters)
    {
        $resultPageParameters = array();
        foreach ($parameters as $key => $val) {
            switch ($key) {
                case 'categories':
                    $resultPageParameters['category'] = $val;
                    break;
                case 'brands':
                    $this->appendBrandFilter($val, $resultPageParameters);
                    break;
                case 'price':
                    $this->appendPriceFilter($val, $resultPageParameters);
                    break;
                default:
                    $this->appendParameterFilter($key, $val, $resultPageParameters);
                    break;
            }
        }

        return new RedirectResponse($this->generateUrl('parameter_selection', $resultPageParameters));
    }

    /**
     * @param string $pricesFromTo from;to
     * @param array  $filters
     */
    protected function appendPriceFilter($pricesFromTo, &$filters)
    {
        $prices = explode(';', $pricesFromTo);

        if (count($prices) == 2) {
            $urlLanguages   = $this->container->getParameter('url_languages')['en'];
            $filterAlias    = key_exists('filter_alias', $urlLanguages) ? $urlLanguages['filter_alias'] : array();

            $from           = key_exists('price_from', $filterAlias)
                ? $filterAlias['price_from']
                : 'price-from';
            $to             = key_exists('price_to', $filterAlias)
                ? $filterAlias['price_to']
                : 'price-to';

            $filters[$from] = $prices[0];
            $filters[$to]   = $prices[1];
        }
    }

    /**
     * @param string    $parameterId
     * @param string    $valueId
     * @param array     $filters
     */
    protected function appendParameterFilter($parameterId, $valueId, &$filters)
    {
        if ($valueId && $parameterId) {
            $parameter = $this->getDocumentManager()->find('NitraProductBundle:Parameter', $parameterId);
            if (!$parameter) {
                return;
            }
            $value = $this->getDocumentManager()->find('NitraProductBundle:ParameterValue', $valueId);
            if (!$value) {
                return;
            }
            $filters[$parameter->getAliasEn()] = $value->getAliasEn();
        }
    }

    /**
     * @param string    $val
     * @param array     $filters
     */
    protected function appendBrandFilter($val, &$filters)
    {
        if ($val) {
            $brand = $this->getDocumentManager()->find('NitraProductBundle:Brand', $val);
            if ($brand) {
                $filters['brand'] = $brand->getAliasEn();
            }
        }
    }
}