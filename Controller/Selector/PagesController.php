<?php

namespace Nitra\ProductBundle\Controller\Selector;

use Nitra\StoreBundle\Controller\NitraController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PagesController extends NitraController
{
    /**
     * @return \Nitra\ProductBundle\Lib\Filter Filter instance
     */
    protected function getFilter()
    {
        return $this->get('nitra.filter');
    }

    /**
     * @Route("city/{id}", name="selector_city")
     * @Template("NitraProductBundle:Selector\Pages:city.html.twig")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return array
     */
    public function cityPageAction(Request $request, $id)
    {
        $host = $request->getHost();
        $seo  = $this->getDocumentManager()
            ->createQueryBuilder('NitraSeoBundle:Seo')
            ->field('key')->equals(new \MongoRegex("/$host\/city\/$id/"))
            ->getQuery()->execute()->getSingleResult();

        if (!$seo) {
            throw $this->createNotFoundException(sprintf("Seo info for city with id \"%s\" not found", $id));
        }

        return array(
            'id'  => $id,
            'seo' => $seo,
        );
    }

    /**
     * @Route("cities", name="selector_cites")
     * @Template("NitraProductBundle:Selector\Pages:cities.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function citiesPageAction(Request $request)
    {
        $titles  = array();
        $seoList = $this->getDocumentManager()
            ->createQueryBuilder('NitraSeoBundle:Seo')
            ->hydrate(false)
            ->field('key')->equals(new \MongoRegex("/{$request->getHost()}\/city\/[\d]+$/i"))
            ->getQuery()
                ->execute();

        foreach ($seoList as $seo) {
            $titles[end(explode('/', $seo['key']))] = $seo['metaTitle'];
        }
        $cities = $this->getTetradka()->getCities();

        return array(
            'cities' => $this->checkInArray($cities, $titles),
            'titles' => $titles,
        );
    }

    /**
     * для проверки существует ли город
     *
     * @param array $cities
     * @param array $titles
     */
    protected function checkInArray($cities, $titles)
    {
        $result = array();
        foreach ($cities as $city) {
            if (key_exists($city['id'], $titles)) {
                $result[$city['id']] = $city['city_name'];
            }
        }

        return $result;
    }

    /**
     * Страница автомобилей
     *
     * @Route("cars", name="selector_automobiles")
     * @Template("NitraProductBundle:Selector\Pages:automobiles.html.twig")
     *
     * @return array
     */
    public function automobilesPageAction()
    {
        $key = $this->mapCacheKey('selector_automobiles_page');
        if ($this->getCache()->contains($key)) {
            $vendors = $this->getCache()->fetch($key);
        } else {
            $vendors = $this->formatVendors($this->getVendors());

            $this->getCache()->save($key, $vendors, 60 * 60 * 24);
        }

        return array(
            'vendors' => $vendors,
        );
    }

    /**
     * @return \Nitra\ProductBundle\Document\Category
     */
    protected function getTiresCategory()
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->field('id')->in($this->getStore()['tiresTabCategories'])
            ->getQuery()
            ->execute()
            ->getSingleResult();
    }

    /**
     * @return \Nitra\ProductBundle\Document\Vendor[]
     */
    protected function getVendors()
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Vendor')
            ->getQuery()
            ->execute();
    }

    /**
     * @param \Nitra\ProductBundle\Document\Vendor[] $Vendors
     * @return array
     */
    protected function formatVendors($Vendors)
    {
        $vendors       = array();
        $linkPrototype = $this->generateUrl('selection', array(
            'alias'     => '__vendor_alias__',
            'category'  => 'tires',
        ));
        foreach ($Vendors as $vendor) {
            if (!key_exists($vendor->getName(), $vendors)) {
                $vendors[$vendor->getName()] = array();
            }
            if (!key_exists($vendor->getVehicle(), $vendors[$vendor->getName()])) {
                $vendors[$vendor->getName()][$vendor->getVehicle()] = array();
            }
            if (!key_exists($vendor->getYear(), $vendors[$vendor->getName()][$vendor->getVehicle()])) {
                $vendors[$vendor->getName()][$vendor->getVehicle()][$vendor->getYear()] = array();
            }
            $vendors[$vendor->getName()][$vendor->getVehicle()][$vendor->getYear()][$vendor->getModification()] = str_replace('__vendor_alias__', $vendor->getAlias(), $linkPrototype);
        }

        ksort($vendors);
        foreach ($vendors as &$vendor) {
            ksort($vendor);
            foreach ($vendor as &$vehicle) {
                ksort($vehicle);
                foreach ($vehicle as &$year) {
                    ksort($year);
                }
            }
        }

        return $vendors;
    }

    /**
     * Страница результатов подборщика по авто
     *
     * @Route("/selection/{category}/{alias}/{combination}/{sort}", name="selection", defaults={"category"="", "alias"="", "combination"=0, "sort"="name"})
     * @Template("NitraProductBundle:Selector\Pages:selectorAutoResults.html.twig")
     *
     * @param string $category    Алиас категории
     * @param string $alias       Алиас вендора (автомобиля)
     * @param int    $combination Номер сборки (для каждой категории свой)
     * @param string $sort        Поле(я) сортировки товаров
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function selectorByAutoPageAction(Request $request, $category, $alias, $combination = 0, $sort = "name")
    {
        if (!$category || !$alias) {
            throw $this->createNotFoundException();
        }

        $store      = $this->getStore();
        $categories = !key_exists("{$category}TabCategories", $store)
            ? array()
            : $this->getDocumentManager()
                ->getRepository('NitraProductBundle:Category')
                ->findBy(array(
                    '_id' => array(
                        '$in' => array_map(function($categoryId) {
                            return new \MongoId($categoryId);
                        }, $store["{$category}TabCategories"]),
                    ),
                ));
        $vendorObject = $this->getVendor($alias);
        $vendorQuery  = $this->getVendorProductsQuery($vendorObject, $category, $combination);
        $categoryIds  = $this->getAllChildCategoryIds($categories);
        $vendor = array(
            'alias'       => $vendorObject->getAlias(),
            'categories'  => $category,
            'combination' => $combination,
        );
        $this->getRequest()->getSession()->set('selector_selected_vendor', $vendor);
        $qb = $this->getFilteredData(
            $vendorQuery->addAnd($this->getAdditionalFilterQuery($categoryIds)),
            $sort
        );

        $products = $this->paginate($qb);

        return array(
            'total'           => $this->getFilter()->getTotal(),
            'vendor'          => $vendorObject,
            'category'        => $category,
            'products'        => $products,
            'productModels'   => array(),
            'routeParameters' => array_merge($request->request->all(), $request->query->all(), $request->attributes->get('_route_params')),
            'mode'            => $this->container->getParameter('nitra.filter.mode'),
            'characteristics' => $this->getProductCharacteristics($products->getItems()),
            'filter'          => $this->getFilter()->getTemplateContext('selection'),
        );
    }

    /**
     * Получение данных фильтра ajax-ом
     * @Route("/auto_selection_ajax_parameters/{category}/{alias}/{combination}", name="auto_selection_ajax_parameters", defaults={"combination"=0})
     */
    public function ajaxAutoAction($category, $alias, $combination = 0)
    {
        $store          = $this->getStore();
        $Vendor         = $this->getVendor($alias);
        $Category       = $this->getCategory($category);
        $vendorQuery    = $this->getVendorProductsQuery($Vendor, $Category, $combination)->getQueryArray();
        $categoryIds    = $this->getAllChildCategoryIds(array($Category));

        $data           = $this->getFilteredData($categoryIds, array_merge(array(
            $vendorQuery,
        ), $this->getAdditionalFilterQuery($store, $categoryIds)), 'auto_selection_ajax_parameters', true);

        return new JsonResponse($data);
    }

    /**
     * @param string $alias
     * @return \Nitra\SelectionBundle\Document\Vendor
     */
    protected function getVendor($alias)
    {
        return $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Vendor')
            ->field('alias')->equals($alias)
            ->getQuery()->execute()->getSingleResult();
    }

    /**
     * @param string $alias
     * @return \Nitra\SelectionBundle\Document\Category
     */
    protected function getCategory($alias)
    {
        return $this->getDocumentManager()->createQueryBuilder("NitraProductBundle:Category")
            ->field('alias_en')->equals($alias)
            ->getQuery()->execute()->getSingleResult();
    }

    /**
     * @param \Nitra\ProductBundle\Document\Vendor      $vendor
     * @param string                                    $categoryType
     * @param int                                       $combination
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    protected function getVendorProductsQuery($vendor, $categoryType, $combination)
    {
        $qb      = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product');
        $request = $this->getRequest();
        $index   = 0;
        foreach ($vendor->{'get' . ucfirst($categoryType)}() as $tw) {
            foreach ($tw as $combins) {
                foreach ($combins as $combin) {
                    if ($index == $combination) {
                        foreach ($combin as $parameterId => $valueId) {
                            $parameter = $this->getDocumentManager()->find('NitraProductBundle:Parameter', $parameterId);
                            foreach ($parameter->getParameterValues() as $value) {
                                if ($value->getId() == $valueId) {
                                    $request->query->set($parameter->getAlias(), $value->getAlias());
                                }
                            }
                            $qb->addAnd($qb->expr()
                                ->field('parameters')->elemMatch($qb->expr()
                                    ->field('parameter')->equals(new \MongoId($parameterId))
                                    ->field('values')->equals(new \MongoId($valueId))
                                )
                            );
                        }
                        return $qb;
                    }
                    $index ++;
                }
            }
        }
    }

    /**
     * Страница результатов подборщика по параметрам
     * @Route("/parameters/{category}/{sort}", name="parameter_selection", defaults={"category"="", "sort"="name"})
     * @Template("NitraProductBundle:Selector\Pages:selectorParametersResults.html.twig")
     *
     * @param Request $request
     * @param string                                            $category       Тип подборщика (tires|wheels)
     * @param string                                            $sort           Поле(я) сортировки товаров
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function selectorByParametersPageAction(Request $request, $category, $sort = "name")
    {
        if (!$category) {
            throw $this->createNotFoundException();
        }
        $store = $this->getStore();
        if (!key_exists("{$category}TabCategories", $store)) {
            throw $this->createNotFoundException('Категории не найдены');
        }
        $categories = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->field('id')->in($store["{$category}TabCategories"])
            ->getQuery()->execute()->toArray();

        $categoryIds = $this->getAllChildCategoryIds($categories);
        $qb          = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Product')
            ->setQueryArray($this->getAdditionalFilterQuery($categoryIds));

        $query    = $this->getFilteredData($qb, $sort);
        $products = $this->paginate($query);

        $routeRequestParameters = array_merge($request->request->all(), $request->query->all(), $request->attributes->get('_route_params'));

        return array(
            'total'           => $this->getFilter()->getTotal(),
            'productModels'   => array(),
            'products'        => $products,
            'routeParameters' => $routeRequestParameters,
            'mode'            => $this->container->getParameter('nitra.filter.mode'),
            'characteristics' => $this->getProductCharacteristics($products->getItems()),
            'filter'          => $this->getFilter()->getTemplateContext('parameter_selection'),
        );
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product[] $items
     *
     * @return array
     */
    protected function getProductCharacteristics($items)
    {
        $repo = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product');

        $result = array();
        foreach ($items as $item) {
            $result[$item->getId()] = $repo->getProductOrModelCharacteristics($item, true, 'all');
        }

        return $result;
    }

    /**
     * Получение данных фильтра ajax-ом
     * @Route("/parameter_selection_ajax_parameters/{category}", name="parameter_selection_ajax_parameters")
     */
    public function ajaxParametersAction($category)
    {
        $store = $this->getStore();
        if (!key_exists("{$category}TabCategories", $store)) {
            throw $this->createNotFoundException('Категории не найдены');
        }
        $categories = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->field('isActive')->equals(true)
            ->field('stores.id')->equals($store['id'])
            ->field('id')->in($store["{$category}TabCategories"])
            ->getQuery()->execute()->toArray();

        $categoryIds    = $this->getAllChildCategoryIds($categories);

        return new JsonResponse($this->getFilteredData($categories, $this->getAdditionalFilterQuery($store, $categoryIds), 'parameter_selection_ajax_parameters', true));
    }

    /**
     * Подготовка и фильтрация товаров
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     * @param string                              $sort
     *
     * @return array
     */
    protected function getFilteredData($qb, $sort)
    {
        $filter   = $this->getFilter();
        $resultQb = $filter->filter(null, $qb);

        $resultQb
            ->sort($this->getProductsSort($sort))
            ->getQuery()
            ->execute()
            ->toArray();

        return $resultQb;
    }

    /**
     * Дополнительные параметры запроса на выборку товаров
     * @param array $categoryIds
     * @return array
     */
    protected function getAdditionalFilterQuery($categoryIds)
    {
        $modelsIds = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->in($categoryIds)
            ->getQuery()
            ->execute()
            ->toArray();

        return array(
            'model.$id' => array(
                '$in' => $modelsIds,
            ),
        );
    }

    /**
     * Получение id текущей категории и всех её дочерних
     * @param \Nitra\ProductBundle\Document\Category[] $ids
     * @return \MongoId[]
     */
    protected function getAllChildCategoryIds($ids)
    {
        $store = $this->getStore();

        $categories = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('id')
            ->field('stores.id')->equals($store['id'])
            ->field('path')->equals(new \MongoRegex("/" . implode('|', array_map(function ($cat) { return $cat->getId(); }, $ids)) . "/"))
            ->getQuery()->execute()->toArray();

        return array_values(array_map(function($cat) { return $cat['_id']; }, $categories));
    }

    /**
     * Получение товаров модели
     * @param \Nitra\ProductBundle\Document\Product[]   $items
     * @param array                                     $query
     * @return array
     */
    protected function getModelProducts($items, $query)
    {
        $store = $this->getStore();

        $modelsProducts = array();
        foreach ($items as $product) {
            $repository     = $this->getDocumentManager()->getRepository('NitraProductBundle:Product');
            $qb             = $repository->createQueryBuilder()->setQueryArray($query);
            $modelProducts  = $repository->getModelProducts($product, -1, $qb, true, array(
                'storePrice.' . $store['id'] . '.price' => 1,
            ));
            if ($modelProducts->count() > 1) {
                $modelsProducts[$product->getModel()] = $modelProducts;
            }
        }

        return $modelsProducts;
    }

    /**
     * @param string $sort
     * @return array
     */
    protected function getProductsSort($sort)
    {
        $urlLanguages = $this->container->hasParameter('url_languages')
            ? $this->container->getParameter('url_languages')['en']
            : array();
        $sortAlias  = key_exists('sort_alias', $urlLanguages) ? $urlLanguages['sort_alias'] : array('name' => 'name');
        $badgeSorts = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Badge')
            ->distinct('identifier')
            ->sort('sortOrder', 1)
            ->getQuery()->execute()->toArray();

        $store = $this->getStore();
        $sorts = array();
        switch ($sort) {
            case $sortAlias['name']:
                $sorts['model'] = 1;
                $sorts['name']  = 1;
                break;
            case $sortAlias['price']:
                $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                break;
            default:
                if (in_array($sort, $badgeSorts)) {
                    $sorts['badgeSorts.' . $sort] = -1;
                    $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                }
                break;
        }

        return $sorts;
    }
}