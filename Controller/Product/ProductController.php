<?php

namespace Nitra\ProductBundle\Controller\Product;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Nitra\StoreBundle\Lib\Globals;

class ProductController extends NitraController
{
    /**
     * Страница товара
     *
     * @Route("/product/{slug}", name="product_page")
     * @Template("NitraProductBundle:Product:productPage.html.twig")
     * @Cache(smaxage="300", public="true")
     *
     * @param string $slug Unique product alias
     *
     * @return array Template parameters
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function productPageAction($slug)
    {
        $alias   = mb_strtolower($slug, 'UTF-8');
        $product = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('aliasEn')->equals($alias)
            ->getQuery()->execute()
            ->getSingleResult();

        if (!$product) {
            throw $this->createNotFoundException(sprintf('Product by alias "%s" not found', $slug));
        }

        return array(
            'product' => $product,
        );
    }

    /**
     * Обновление количества просмотров товара
     *
     * @param Request   $request    Request instance
     * @param string    $productId  Id of product
     *
     * @return Response     Empty response
     */
    public function updateProductViewsAction(Request $request, $productId)
    {
        $viewed = $request->cookies->get('viewed_products', array());

        // проверем первый ли раз в данной сесии просматривается этот товар
        if (!in_array($productId, $viewed)) {
            $Product = $this->getDocumentManager()->find('NitraProductBundle:Product', $productId);
            if ($Product) {
                $Product->setViewed($Product->getViewed() + 1);
                $this->getDocumentManager()->flush();
            }
            // добавляем товар к просмотренным
            $viewed[] = $productId;
            $request->cookies->set('viewed_products', $viewed);
        }

        return new Response();
    }

    /**
     * Форматирование продуктов
     *
     * @param \Symfony\Component\HttpFoundation\Request $request    Request instance
     * @param \Nitra\ProductBundle\Document\Product[]   $products   List of products
     * @param string                                    $template   Twig template
     *
     * @return array Template parameters
     */
    public function formatProductListAction(Request $request, $products, $template = 'NitraProductBundle:Product:productList.html.twig')
    {
        $session  = $request->getSession();

        $view = $session->has('view')
            ? $session->get('view')
            : $this->container->getParameter('view');

        return $this->render($template, array(
            'items' => $products,
            'view'  => $view,
        ));
    }

    /**
     * Панель содержащая переключение вида списка и сортировку товаров
     *
     * @Template("NitraProductBundle:Modules:productPanel.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request        Request instance
     * @param array                                     $params         Route parameters
     * @param string                                    $pathInfo       Route path info
     * @param int                                       $limitCount     Count of limits
     * @param array                                     $sortWithBadges Array with badges indentifiers for allow sort by him
     * @param string                                    $class          Css class (<b>js</b> not be displayed if set)
     *
     * @return array Template parameters
     */
    public function productPanelAction(Request $request, $params, $pathInfo, $limitCount = 3, $sortWithBadges = array(), $class = null)
    {
        $session = $request->getSession();

        $view    = $session->has('view')
            ? $session->get('view')
            : $this->container->getParameter('view');

        $limit   = $session->has('limit')
            ? $session->get('limit')
            : Globals::getStoreLimit('page', 10);

        $badges = $sortWithBadges
            ? $this->getBadgesForProductPanelSort($sortWithBadges)
            : array();

        return array(
            'currentRoute' => array_merge($this->get("router")->match($pathInfo), $params),
            'limits'       => $this->getProductPanelLimits(Globals::getStoreLimit('page', 10), $limitCount),
            'limit'        => $limit,
            'view'         => $view,
            'params'       => $params,
            'badges'       => $badges,
            'class'        => $class,
        );
    }

    /**
     * Get limits for product panel
     *
     * @param int   $perPage        Count items per page
     * @param int   $limitCount     Limits count
     *
     * @return array Limits
     */
    protected function getProductPanelLimits($perPage, $limitCount)
    {
        $limits = array();

        for ($i = 1; $i <= $limitCount; $i++) {
            $limits[] = $perPage * $i;
        }

        return $limits;
    }

    /**
     * Get badges for sorting
     *
     * @param array $sortWithBadges
     *
     * @return \Nitra\ProductBundle\Document\Badge[]
     */
    protected function getBadgesForProductPanelSort($sortWithBadges)
    {
        $repository = $this->getDocumentManager()->getRepository('NitraProductBundle:Badge');
        if (is_array($sortWithBadges)) {
            return $repository->findByIdentifier(array(
                '$in' => $sortWithBadges,
            ));
        } else {
            return $repository->findAll();
        }
    }

    /**
     * Сохранение вида списка
     *
     * @Route("/save-view", name="save_view")
     *
     * @param Request $request  Request instance
     *
     * @return Response Empty response
     */
    public function saveViewAction(Request $request)
    {
        $view = $request->request->get('view');
        if ($view) {
            $request->getSession()->set('view', $view);
        }

        return new Response();
    }

    /**
     * Сохранение количества товаров для вывода
     *
     * @Route("/save-limit", name="save_limit")
     *
     * @param Request $request Request instance
     *
     * @return Response Empty response
     */
    public function saveLimitAction(Request $request)
    {
        $limit = $request->request->get('limit');
        if ($limit) {
            $request->getSession()->set('limit', $limit);
        }

        return new Response();
    }
}