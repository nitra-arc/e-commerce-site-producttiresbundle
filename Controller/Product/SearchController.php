<?php

namespace Nitra\ProductBundle\Controller\Product;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class SearchController extends NitraController
{
    protected $searchType           = '\Nitra\ProductBundle\Form\Type\SearchType';
    protected $suggestImagineFilter = 'small_search';

    /** @return \Nitra\StoreBundle\Twig\Extension\NitraExtension */
    protected function getNitraStoreBundleTwigExtension() { return $this->get('nitra_extension'); }

    /**
     * Header search form
     *
     * @Template("NitraProductBundle:Search:headerSearch.html.twig")
     *
     * @param null|string $pathInfo
     *
     * @return array Template context
     */
    public function headerSearchAction($pathInfo = null)
    {
        $term = null;
        if ($pathInfo) {
            $route = $this->get('router')->match($pathInfo);
            if ($route['_route'] == 'search_page') {
                $term = $route['term'];
            }
        }
        $form = $this->createForm(new $this->searchType, array(
            'term'  => $term,
        ));

        return array(
            'form'  => $form->createView(),
        );
    }

    /**
     * Page of search results
     *
     * @Route("/search/{term}/{sort}/{group}", name="search_page", defaults={"term"=null,"group"=null,"sort"="name"})
     * @Cache(smaxage="300", public="true")
     *
     * @param null|string $term
     * @param null|string $group
     * @param string $sort
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($term = null, $group = null, $sort = "name")
    {
        $finder = $this->getFinder();

        return $finder->search($term, false, $group, $sort);
    }

    /**
     * Подсказка при поиске
     *
     * @Route("/search-suggest", name="search_suggest")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function suggestAction(Request $request)
    {
        // get finder service
        $finder = $this->getFinder();

        // get paginated results from finder service
        $paginatedResults = $finder->search($request->get('term'), true);
        // format search results as array for select2
        if (count($paginatedResults) > 1) {
            $allResults = array(array(
                'id'    => $this->generateUrl('search_page', array(
                    'term'  => $request->get('term'),
                )),
                'label' => $this->getTranslator()->trans('search.suggestAll', array(), 'NitraProductBundle'),
                'group' => true,
                'all'   => true,
            ));
            $results    = array_merge($this->formatSuggestGroups($paginatedResults), $allResults);
        } elseif (count($paginatedResults)) {
            $group      = array_keys($paginatedResults)[0];
            $allResults = array(array(
                'id'    => $this->generateUrl('search_page', array(
                    'term'  => $request->get('term'),
                    'group' => $group,
                )),
                'label' => $this->getTranslator()->trans('search.suggestAll', array(), 'NitraProductBundle'),
                'group' => true,
                'all'   => false,
            ));
            $results    = array_merge($this->formatSuggestGroup($group, $paginatedResults[$group]), $allResults);
        } else {
            $results    = array();
        }

        // return response as json
        return new JsonResponse($results);
    }

    /**
     * Formatting results for suggest for each group
     *
     * @param \Knp\Component\Pager\Pagination\PaginationInterface[] $paginatedResults
     *
     * @return array
     */
    protected function formatSuggestGroups($paginatedResults)
    {
        // define results array
        $results = array();
        // foreach by paginated results
        foreach ($paginatedResults as $group => $paginator) {
            // getting formatted results for group
            $children = $this->formatSuggestGroup($group, $paginator);
            // push group to results if has children
            if ($children) {
                $results[] = array(
                    // trans group title
                    'id'        => $this->generateUrl('search_page', array(
                        'term'      => $this->getRequest()->get('term'),
                        'group'     => $group,
                    )),
                    'label'     => $this->getTranslator()->trans("search.groups.$group.suggest.title", array('%q%' => $paginator->getTotalItemCount()), 'NitraProductBundle'),
                    'group'     => true,
                );
                $results = array_merge($results, $children);
            }
        }

        // return results
        return $results;
    }

    /**
     * Formatting results for group for suggest
     *
     * @param string $group
     * @param \Knp\Component\Pager\Pagination\PaginationInterface $paginator
     *
     * @return array
     */
    protected function formatSuggestGroup($group, $paginator)
    {
        $groupConfiguration = $this->getFinder()->getConfiguration($group);
        // define group result
        $groupResults = array();
        // define variable for check items image
        $hasImage = null;
        // foreach by paginated items
        foreach ($paginator->getItems() as $item) {
            $object = $groupConfiguration['embedded']
                ? $item['embedded']
                : $item;
            // if first iteration (variable equals null)
            if ($hasImage === null) {
                // method getImage exists in item
                $hasImage = method_exists($object, 'getImage');
            }
            // define item array
            $result = $this->appendToSuggestItem($group, $item) + array(
                'id'    => $this->generateLinkForGroupItem($object, $groupConfiguration),
                'label' => (string) ($groupConfiguration['toString'] ? $object->{'get' . ucfirst($groupConfiguration['toString'])}() : $object),
                'group' => false,
            );
            // if method getImage exists in item
            if ($hasImage) {
                // push loaded to cache image to item array
                $result['image'] = $this->loadSuggestImage($object->getImage());
            }
            // add formatted item to results array
            $groupResults[] = $result;
        }

        // return results
        return $groupResults;
    }

    /**
     * Метод для добавления уникальных для элементов данных
     * в результаты авто подсказки поиска
     *
     * @param string $group
     * @param object $item
     *
     * @return array
     */
    protected function appendToSuggestItem($group, $item)
    {
        $method = 'appendToSuggest' . ucfirst($group);
        if (method_exists($this, $method)) {
            return (array) $this->$method($item);
        }

        return array();
    }

    /**
     * Метод для добавления уникальных для товаров данных
     * в результаты авто подсказки поиска
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return array
     */
    protected function appendToSuggestProducts($product)
    {
        return array(
            'price' => $this->getNitraStoreBundleTwigExtension()->price($product->getPrice()),
        );
    }

    /**
     * Loading image from admin to site cache for suggest
     *
     * @param string $image
     *
     * @return string path to image
     */
    protected function loadSuggestImage($image)
    {
        // if not image
        if (!$image) {
            // set to no_image
            $image = 'images/no_image.jpg';
        }

        // load and resize image to cache
        try {
            $this->getLiipImagineController()->filterAction($this->getRequest(), $image, $this->suggestImagineFilter);
        } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $ex) {
            // if source image not found in admin
        }

        // return path to loaded image
        return $this->getLiipImagineCacheManager()->getBrowserPath($image, $this->suggestImagineFilter);
    }

    /**
     * Generate link for group item
     *
     * @param object $item
     * @param array  $configuration
     *
     * @return string
     */
    protected function generateLinkForGroupItem($item, $configuration)
    {
        // define route parameters array
        $parameters = array();
        // foreach by configured route parameters
        foreach ($configuration['routeParameters'] as $parameter => $getter) {
            // add route parameter
            $parameters[$parameter] = $this->getLinkParameter($item, $getter);
        }

        // return generated link to configured path
        return $this->generateUrl($configuration['route'], array_merge($configuration['staticRouteParameters'], $parameters));
    }

    /**
     * Getting link parameter (sample: category.alias for $item->getCategory()->getAlias())
     *
     * @param object $item
     * @param string $parameter
     *
     * @return string
     */
    protected function getLinkParameter($item, $parameter)
    {
        $object = $item;
        foreach (explode('.', $parameter) as $getter) {
            $object = $object->{'get' . ucfirst($getter)}();
        }

        return $object;
    }
}