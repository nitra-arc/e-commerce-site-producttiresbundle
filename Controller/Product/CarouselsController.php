<?php

namespace Nitra\ProductBundle\Controller\Product;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Nitra\StoreBundle\Lib\Globals;

/**
 * Набор каруселей
 */
class CarouselsController extends NitraController
{
    /**
     * Get products query builder
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected function getProductsQb()
    {
        return $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getDefaultQb();
    }

    /**
     * Карусель товаров конкретной категории
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product     $product            Product instance
     * @param string                                    $orientation        Carousel orientation
     * @param integer                                   $limit              Products limit
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param integer                                   $priceDeviation     Price deviation for select products
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function categoryProductsAction($product, $orientation = 'horizontal', $limit = 20, $class = 'category_products', $caption = 'carousel.category', $visible = 7, $type = 'simple', $imageSize = 'big_carousel_thumb', $priceDeviation = 0, $autoWidth = false, $responsive = false)
    {
        $category = $product->getModel()->getCategory();

        $products = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getCarouselCategoryProducts(
                $category,
                $product,
                $limit,
                $priceDeviation
            );

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель товаров конкретной модели
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product     $product            Product instance
     * @param string                                    $orientation        Carousel orientation
     * @param integer                                   $limit              Products limit
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function modelProductsAction($product, $orientation, $limit = 20, $class = 'model_products', $caption = '', $visible = 7, $type = 'simple', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $products = $this->getProductsQb()
            ->field('model.id')->equals($product->getModel()->getId())
            ->field('id')->notEqual($product->getId())
            ->limit($limit)
            ->getQuery()->execute();

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель просмотренных товаров
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request            Request instance
     * @param \Nitra\ProductBundle\Document\Product     $product            Product instance
     * @param string                                    $orientation        Carousel orientation
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function viewedProductsAction(Request $request, $product = null, $orientation = 'horizontal', $class = '', $caption = '', $visible = 7, $type = '', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $cookies   = $request->cookies;

        $limit     = Globals::getStoreLimit('viewed', 10);
        $allIds    = $cookies->get('viewed_products', array());

        $viewedIds = array_slice($allIds, 0, $limit);

        $qb        = $this->getProductsQb()
            ->field('id')->in($viewedIds);

        if ($product) {
            $qb->field('id')->notEqual($product->getId());
        }

        $products  = $qb->getQuery()->execute();

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель просмотренных моделей
     *
     * @Template("NitraProductBundle:Carousels:models.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request            Request instance
     * @param \Nitra\ProductBundle\Document\Model       $model              Model instance
     * @param string                                    $orientation        Carousel orientation
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function viewedModelsAction(Request $request, $model = null, $orientation = 'horizontal', $class = null, $caption = null, $visible = 7, $type = '', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $session   = $request->getSession();

        $limit     = Globals::getStoreLimit('viewed', 10);
        $allIds    = $session->get('viewed_models', array());

        $viewedIds = array_slice($allIds, 0, $limit);

        $qb        = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->field('id')->in($viewedIds);

        if ($model) {
            $qb->field('id')->notEqual($model->getId());
        }

        $models    = $qb->getQuery()->execute();

        return array(
            'items'       => $models,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель рекомендуемых (похожих) товаров
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product     $product            Product instance
     * @param string                                    $orientation        Carousel orientation
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function recommendedProductsAction($product, $orientation = 'horizontal', $class = '', $caption = '', $visible = 7, $type = '', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $recommendedProducts = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getRecommendedProducts(
                $product,
                Globals::getStoreLimit('recommended', 10)
            );

        return array(
            'items'       => $recommendedProducts,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель популярных товаров
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param string                                    $orientation        Carousel orientation
     * @param integer                                   $limit              Products limit
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function popularProductsAction($orientation = 'horizontal', $limit = 20, $class = 'popular_products', $caption = '', $visible = 7, $type = 'simple', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $mode = $this->container->hasParameter('popular_products_mode')
            ? $this->container->getParameter('popular_products_mode')
            : 'by_viewed';

        $products = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getPopularProducts($mode, $limit);

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Карусель популярных моделей
     *
     * @Template("NitraProductBundle:Carousels:models.html.twig")
     *
     * @param string                                    $orientation        Carousel orientation
     * @param integer                                   $limit              Products limit
     * @param string                                    $class              Carousel class
     * @param string                                    $caption            Carousel caption
     * @param integer                                   $visible            Visible carousel items
     * @param string                                    $type               Carousel type
     * @param string                                    $imageSize          Imagine filter name
     * @param boolean                                   $autoWidth          Auto width
     * @param boolean                                   $responsive         Responsive
     *
     * @return array Template parameters
     */
    public function popularModelsAction($orientation = 'horizontal', $limit = 20, $class = 'popular_products', $caption = '', $visible = 7, $type = 'simple', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $mode = $this->container->hasParameter('popular_models_mode')
            ? $this->container->getParameter('popular_models_mode')
            : 'by_viewed';
        $qb = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->limit($limit);

        $this->addConditionsToPopularModelsQueryByMode($qb, $mode);

        $models = $qb->getQuery()->execute();

        return array(
            'items'       => $models,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Add  conditions to models query for select popular
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb     Builder instance
     * @param string                                $mode   Mode for select models
     */
    protected function addConditionsToPopularModelsQueryByMode($qb, $mode)
    {
        switch ($mode) {
            case 'by_is_popular':
                $qb ->field('isPopular')->equals(true);
                break;
            default:
                $qb ->field('viewed')->gt(0)
                    ->sort('viewed', -1);
                break;
        }
    }
}