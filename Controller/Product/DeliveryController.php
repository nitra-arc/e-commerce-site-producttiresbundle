<?php

namespace Nitra\ProductBundle\Controller\Product;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\ProductBundle\Document\Product;
use Nitra\ProductBundle\Form\Type\Product\ProductSearchDeliveryType;
use Nitra\ProductBundle\Form\Type\Product\ProductEstimateDeliveryType;

/**
 * Расчет стоимости доставки продукта
 */
class DeliveryController extends NitraController
{
    /**
     * Send error as json
     *
     * @param string    $id
     * @param boolean   $trans
     * @param array     $parameters
     *
     * @return JsonResponse
     */
    protected function sendError($id, $trans = true, array $parameters = array())
    {
        if ($trans) {
            $message = $this->getTranslator()->trans(
                'product.delivery.error.' . $id,
                $parameters,
                'NitraProductBundle'
            );
        } else {
            $message = $id;
        }

        return new JsonResponse(array(
            'type'    => 'error',
            'message' => $message,
        ));
    }

    /**
     * Получить покупателя
     *
     * @return \Nitra\BuyerBundle\Document\Buyer|null
     */
    protected function getBuyer()
    {
        // получить сессию
        $session = $this->container->get('session');

        // проверить покупателя
        if ($session->has('buyer')) {

            // получить покупателя
            $buyer = $session->get('buyer');

            // вернуть покупателя
            return $this->getDocumentManager()->find('NitraBuyerBundle:Buyer', $buyer['id']);
        }
    }

    /**
     * Получить форму расчета стоимости доставки продукта
     *
     * @param \Nitra\ProductBundle\Document\Product|null $product доставляемый продукт для которого сторится форма
     *
     * @return Symfony\Component\Form\Form
     */
    protected function getProductEstimateDeliveryForm(Product $product)
    {
        // получить покупателя
        $buyer = $this->getBuyer();

        // массив данных формы
        $formData = array(
            'city'      => $buyer ? $buyer->getCityId() : null,
            // ID продукта
            'productId' => $product->getId(),
        );

        // масив параметров формы
        $formOptions = array(
            // получаем масив городов из тетрадки для построения виджита выбора города
            'citiesTetradka' => $this->getTetradka()->getDeliveryCities(),
        );

        // создать форму доставки продукта
        $form = $this->createForm(new ProductEstimateDeliveryType(), $formData, $formOptions);

        // вернуть форму стоимости доставки продукта
        return $form;
    }

    /**
     * Обрабочик отображение формы расчета стоимости доставки товара
     *
     * @param Product $product - доставляемый продукт для которого сторится форма
     *
     * @Route("/delivery_context", name="delivery_context")
     * @Template("NitraProductBundle:Delivery:productEstimateDeliveryForm.html.twig")
     */
    public function getProductEstimateDeliveryFormAction(Product $product = null)
    {
        $productId = $this->getRequest()->get('productId');
        if ($productId) {
            $product = $this->getDocumentManager()->find('NitraProductBundle:Product', $productId);
        }

        // получить форму
        $form = $this->getProductEstimateDeliveryForm($product);

        // вернуть массив данных передаваемых в шаблон
        return array(
            'product' => $product,
            'form'    => $form->createView(),
        );
    }

    /**
     * расчет стоимости дотсавки
     *
     * @Route("/product-delivery", name="product_delivery")
     * @Template("NitraProductBundle:Delivery:deliveryPage.html.twig")
     */
    public function deliveryAction(Request $request)
    {
        // создать форму доставки продукта
        $form = $this->createForm(new ProductSearchDeliveryType());

        // обработка формы
        if ($request->getMethod() == 'POST') {
            // заполнить форму
            $form->handleRequest($request);
            // если форма не валидная
            if (!$form->isValid()) {
                // Текст ошибки валидации
                $errorsText = '';

                // ошибка валидации по форме
                foreach ($form->getErrors() as $error) {
                    $errorsText .= $error->getMessage() . ' ';
                }

                // ошибка валидации по каждому виджету
                foreach ($form->all() as $child) {
                    $errors = $child->getErrors();
                    if ($errors) {
                        $errorsText .= $child->getName() . ': ' . $errors[0]->getMessage() . " ";
                    }
                }

                // вернуть ошибки валидации формы
                return $this->sendError($errorsText, false);
            }

            // получить продукт
            $product = $this->get('doctrine.odm.mongodb.document_manager')
                ->getRepository($this->productRepository)
                ->findOneBy(array(
                    'article' => $form->get('article')->getData(),
                ));

            // проверить продукт
            if (!$product) {
                // вернуть ошибки валидации формы
                return $this->sendError('product_not_found');
            }

            // получить форму расчет стоимости доставки продукта
            $formEstimateDelivery = $this->getProductEstimateDeliveryForm($product);

            // вернуть результат поиска
            return new JsonResponse(array(
                'type' => 'success',
                // html отображение формы расчета
                'html' => $this->renderView('NitraProductBundle:Delivery:productEstimateDeliveryForm.html.twig', array(
                    // продукт для которого производятся расчеты
                    'product' => $product,
                    // форма расчета
                    'form'    => $formEstimateDelivery->createView(),
                )),
            ));
        }

        // вернуть форму поиска продукта
        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * расчет стоимости дотсавки
     * 
     * @Route("/product-estimate-delivery", name="product_estimate_delivery")
     */
    public function estimateDeliveryAction(Request $request)
    {
        // масив параметров формы
        $formOptions = array(
            // получаем масив городов из тетрадки для построения виджита выбора города
            'citiesTetradka' => $this->getTetradka()->getDeliveryCities(),
        );

        // создать форму доставки продукта
        $form = $this->createForm(new ProductEstimateDeliveryType(), null, $formOptions);

        // обработка формы
        if ($request->getMethod() == 'POST') {
            // заполнить форму
            $form->submit($request);

            // если форма не валидная
            if (!$form->isValid()) {
                // Текст ошибки валидации
                $errorsText = '';
                // ошибка валидации по форме
                foreach ($form->getErrors() as $error) {
                    $errorsText .= $error->getMessage() . ' ';
                }

                // ошибка валидации по каждому виджету
                foreach ($form->all() as $child) {
                    $errors = $child->getErrors();
                    if ($errors) {
                        $errorsText .= $child->getName() . ': ' . $errors[0]->getMessage() . " ";
                    }
                }

                // вернуть ошибки валидации формы
                return $this->sendError($errorsText, false);
            }

            // получить данные формы
            $formData = $form->getData();

            // получить склад из тетрадки на котором есть продукт
            $warehouse = $this->getTetradka()->getNearestWarehouseForProduct($formData['productId']);
            if (!$warehouse) {
                // вернуть ошибку продукт не найден не на одном из складов
                return $this->sendError('product_not_found_at_any_warehouse');
            }

            // получить продукт
            $product = $this->get('doctrine.odm.mongodb.document_manager')
                ->getRepository($this->productRepository)
                ->find($formData['productId']);

            // проверить продукт
            if (!$product) {
                // вернуть ошибки валидации формы
                return $this->sendError('product_not_found');
            }


            // валидация пройдена успешно
            // массив параметров расчета
            $estimateOptions = array(
                // ID города получателя
                'toCityId'      => $formData['city'],
                // склад ТК не выбран, расчет до первого склада в городе
                'toWarehouseId' => false,
                // маасив доставляемых продуктов
                'products'      => array(
                    array(
                        'productId'  => $product->getId(),
                        'fromCityId' => $warehouse['cityBusinessKey'],
                        'quantity'   => 1,
                        'width'      => $product->getWidth(),
                        'height'     => $product->getHeight(),
                        'length'     => $product->getLength(),
                        'weight'     => $product->getWeight(),
                        'priceOut'   => $product->getPrice(),
                    ),
                ),
            );

            // расчет стоимости доставки
            $estimateDeliveryCost = $this->apiSendRequest('estimateDeliveryCost', $estimateOptions);

            // проверить ответ расчет стоимости доставки
            if (!$estimateDeliveryCost instanceof \stdClass) {
                // вернуть ошибку Ответ не был получен от сервера
                return $this->sendError('server_is_not_available');
            }

            // результирущий массив ТК
            $tkDeliveries = array(); // ТК обычные
            $imDeliveries = array(); // ТК Тариф Интернет магазина
            foreach ($estimateDeliveryCost->deliveries as $deliveryKey => $delivery) {

                // если ключ ТК орицательный
                // то $delivery = расчет по тарифу Интернет Магазина
                if ($deliveryKey <= 0) {
                    // тк интернет магазина
                    $imDeliveries[$delivery->id] = $delivery;
                } else {
                    // тк обычная
                    $tkDeliveries[$delivery->id] = $delivery;
                }
            }


            // обойти массив тариф Интернет магазина
            // если доставка по тарифу Интернет магазина меньше обычной доставки
            // то запоминаем более дешевый тариф
            foreach ($imDeliveries as $deliveryId => $imDelivery) {

                // проверить обычный тариф для тариф Интернет магазина
                if (!isset($tkDeliveries[$deliveryId])) {
                    // тариф не найден
                    continue;
                }

                // получить стоимость обычного тарифа
                $tkDelivery = $tkDeliveries[$deliveryId];

                // ставнить обычный тариф и тариф интернет магазина
                // если тариф интернет магазина меньше обычного тарифа
                if ($imDelivery->estimateCost->costTk < $tkDelivery->estimateCost->costTk) {
                    // обновляем обычный тариф
                    // запоминаем более дешевый
                    $tkDeliveries[$deliveryId]->estimateCost = $imDelivery->estimateCost;
                }
            }

            // вернуть рузультат расчета
            return new JsonResponse(array(
                'type' => 'success',
                // html отображение результатов расчетов
                'html' => $this->renderView('NitraProductBundle:Delivery:productEstimateDeliveryFormResult.html.twig', array(
                    // результат расчетов преобразовать из stdСlass в array
                    'deliveries' => json_decode(json_encode($tkDeliveries), true),
                )),
            ));
        }

        // вернуть ошибку Ответ не был получен от сервера
        return $this->sendError('server_is_not_available');
    }

    /**
     * Отправить запрос на сервер
     *
     * @param string $command       название команды синхронизации
     * @param array  $options       массив параметров передаваемых на сервер DeliverySync
     *
     * @return stdClass             ответ сервера
     */
    protected function apiSendRequest($command, array $options = null)
    {
        // команда синхронизации отправляемая на сервер
        $sendParams = array(
            'command' => $command,
            'options' => $options,
        );

        // отправить запрос на сервер
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->container->getParameter('delivery_sync_api_url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sendParams));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $apiResponse = curl_exec($ch);
        curl_close($ch);

        // вернуть ответ сервера
        return json_decode($apiResponse);
    }
}