<?php

namespace Nitra\ProductBundle\Controller\Category;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nitra\ProductBundle\Document\Category;
use Nitra\StoreBundle\Lib\Globals;

class CategoryController extends NitraController
{
    /**
     * @return \Nitra\ProductBundle\Lib\Filter Filter instance
     */
    protected function getFilter()
    {
        return $this->get('nitra.filter');
    }

    /**
     * Получение категорий для топ-меню
     *
     * @Template("NitraProductBundle:Category:categoryMenu.html.twig")
     *
     * @param string|null   $currentPath    Path of current category
     *
     * @return array Template parameters
     */
    public function categoryMenuAction($currentPath = null)
    {
        return array(
            'categories'  => $this->getCategoriesTree(true),
            'currentPath' => $currentPath,
        );
    }

    /**
     * Получение категорий для меню на главной
     *
     * @Template("NitraProductBundle:Category:categoryMenuHome.html.twig")
     * @Cache(smaxage="3600", public="true", expires="3600")
     *
     * @return array Template parameters
     */
    public function categoryMenuHomeAction()
    {
        return array(
            'categories' => $this->getCategoriesTree(),
        );
    }

    /**
     * Получение категорий для футера
     *
     * @Template("NitraProductBundle:Category:categoryMenuFooter.html.twig")
     * @Cache(smaxage="3600", public="true", expires="3600")
     *
     * @return array Template parameters
     */
    public function categoryMenuFooterAction()
    {
        return array(
            'categories' => $this->getCategoriesTree(),
        );
    }

    /**
     * Get categories tree from cache
     *
     * @param boolean $columned
     *
     * @return array
     */
    protected function getCategoriesTree($columned = false)
    {
        $cache = $this->getCache();
        $key   = $this->mapCacheKey('category_tree_menu' . ($columned ? '_columned' : null));
        if ($cache->contains($key)) {
            $categories = $cache->fetch($key);
        } else {
            $categories = $this->getDocumentManager()
                ->getRepository('NitraProductBundle:Category')
                ->getCategoryTree($this->getRouter());

            if ($columned) {
                $this->getDocumentManager()
                    ->getRepository('NitraProductBundle:Category')
                    ->columnCategoriesTree($categories);
            }

            $cache->save($key, $categories, 60 * 60);
        }

        return $categories;
    }

    /**
     * Dynamic generate route (ajax)
     *
     * @Route("/get-dynamic-link", name="get_dynamic_link")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function dynamicRoute(Request $request)
    {
        $routeName       = $request->request->get('name');
        $routeParameters = $request->request->get('parameters');

        return new Response(urldecode($this->generateUrl($routeName, $routeParameters)));
    }

    /**
     * Page of category
     *
     * @Route("/category/{slug}/{sort}", name="category_page", defaults={"sort"="name"})
     * @Template("NitraProductBundle:Category:categoryPage.html.twig")
     * @Cache(smaxage="300", public=true)
     *
     * @param Request $request  Request instance
     * @param string $slug      Category alias
     * @param string $sort      Sort field
     *
     * @return array Template context
     */
    public function categoryPageAction(Request $request, $slug, $sort)
    {
        $mode     = $this->container->getParameter('nitra.filter.mode');

        $category = $this->findCategory($slug);

        if (!$category) {
            throw $this->createNotFoundException(sprintf('Category by alias "%s" not found', $slug));
        }

        $items = array();
        if ($this->checkNeedFiltering($category, $mode)) {
            $filter = $this->getFilter();
            $qb     = $filter->filter($category, $this->getQbForFilter($category));

            $total = $filter->getTotal();

            $fakeItems = range(1, $total);
            $items     = $request->query->has('all')
                ? $this->paginate($fakeItems, null, (int) $request->query->get('all'))
                : $this->paginate($fakeItems);

            $paginationData = $items->getPaginationData();
            $items->setItems($qb
                ->limit($paginationData['numItemsPerPage'])
                ->skip(($paginationData['current'] - 1) * $paginationData['numItemsPerPage'])
                ->sort($this->getFilterSorts($sort))
                ->getQuery()
                ->execute()
                ->toArray()
            );
        }

        $routeParameters = array_merge(
            $request->query->all(),
            $request->attributes->get('_route_params')
        );
        // перенаправляем на первую страницу
        if (count($items) == 0 && key_exists('page', $routeParameters) && $routeParameters['page'] > 1) {
            unset($routeParameters['page']);
            return $this->redirect($this->generateUrl('category_page', $routeParameters));
        }

        return array(
            'mode'            => $mode,
            'category'        => $category,
            'children'        => $category->getChildren(),
            'characteristics' => $items ? $this->getProductCharacteristics($items->getItems()) : array(),
            'items'           => $items,
            'routeParameters' => $routeParameters,
            'filter'          => isset($filter) ? $filter->getTemplateContext() : array(),
        );
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product[] $items
     *
     * @return array
     */
    protected function getProductCharacteristics($items)
    {
        $repo = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product');

        $result = array();
        foreach ($items as $item) {
            $result[$item->getId()] = $repo->getProductOrModelCharacteristics($item, true, 'all');
        }

        return $result;
    }

    /**
     * Loading flter via ajax
     *
     * @Route("/filter/{slug}", name="filter")
     * @Cache(smaxage=3600, public=true, expires=3600)
     *
     * @param string      $slug
     * @param null|string $template
     *
     * @return Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function filterAction($slug, $template = null)
    {
        $category = $this->findCategory($slug);

        if (!$category) {
            throw $this->createNotFoundException(sprintf('Category by alias "%s" not found', $slug));
        }

        $filter = $this->getFilter();

        $paramsIds = $this
            ->getDocumentManager()
            ->getRepository('NitraProductBundle:Category')
            ->getAllowedParametersIds($category);
        $qb        = $filter
            ->processQuery($this->getQbForFilter($category), $paramsIds);
        $filter->filter($qb);

        return new Response($filter->render($template));
    }

    /**
     * Check need call filtering
     *
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param string $mode Enum value (model or product)
     *
     * @return boolean
     */
    protected function checkNeedFiltering($category, $mode)
    {
        $need = false;

        switch ($mode) {
            case 'model':
                $need = (bool) $this->getDocumentManager()
                    ->createQueryBuilder('NitraProductBundle:Model')
                    ->field('category.id')->equals($category->getId())
                    ->getQuery()->count();
                break;
            case 'product':
                $models = $this->getDocumentManager()
                    ->createQueryBuilder('NitraProductBundle:Model')
                    ->distinct('_id')
                    ->field('category.id')->equals($category->getId())
                    ->getQuery()->toArray();
                $need = (bool) $this->getDocumentManager()
                    ->createQueryBuilder('NitraProductBundle:Product')
                    ->field('model.$id')->in($models)
                    ->getQuery()->count();
                break;
        }

        return $need;
    }

    /**
     * @Route("/ajax-filter/{alias}", name="ajax_filter")
     * @Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request     $request    Request instance
     * @param string                                        $alias      Alias of category
     *
     * @return JsonResponse
     */
    public function loadAjaxFilter(Request $request, $alias)
    {
        foreach ($request->request->get('parameters', array()) as $parameter => $values) {
            $request->query->set($parameter, implode(',', $values));
        }

        $category = $this->findCategory($alias);

        if (!$category) {
            throw $this->createNotFoundException(sprintf('Category by alias "%s" not found', $slug));
        }

        $filter                 = $this->getFilter();
        $allowedParametersIds   = $this->getDocumentManager()->getRepository('NitraProductBundle:Category')->getAllowedParametersIds($category);
        $qb                     = $this->getQbForFilter($category);

        $result = $filter->ajax($qb, $allowedParametersIds);

        return new JsonResponse($result);
    }

    /**
     * Get query builder for filter
     *
     * @param Category $category
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected function getQbForFilter($category)
    {
        return $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getProductsByCategoryQb($category);
    }

    /**
     * Find category by alias
     *
     * @param string $alias
     *
     * @return \Nitra\ProductBundle\Document\Category|null
     */
    protected function findCategory($alias)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->field('aliasEn')->equals(mb_strtolower($alias, 'UTF-8'))
            ->getQuery()->execute()
            ->getSingleResult();
    }

    /**
     * Get sort array
     *
     * @param string $sort
     *
     * @return array
     */
    protected function getFilterSorts($sort)
    {
        $sorts = array();
        $store = $this->getStore();
        $mode  = $this->container->getParameter('nitra.filter.mode');

        switch ($mode) {
            case 'product':
                $badgeSorts = $this->getBadgeSorts();

                switch ($sort) {
                    case 'name':
                        $sorts['fullNames.' . $store['id']]  = 1;
                        break;
                    case 'price':
                        $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                        break;
                    default:
                        if (in_array($sort, $badgeSorts)) {
                            $sorts['badgeSorts.' . $sort] = -1;
                            $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                        }
                        break;
                }
                break;
            case 'model':
                switch ($sort) {
                    case 'name':
                        $sorts['fullNames.' . $store['id']]  = 1;
                        break;
                    case 'price':
                        $sorts['prices.' . $store['id'] . '.from'] = 1;
                        $sorts['prices.' . $store['id'] . '.to'] = 1;
                        break;
                }
                break;
        }

        return $sorts;
    }

    /**
     * Get badges for sorting
     *
     * @return array
     */
    protected function getBadgeSorts()
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Badge')
            ->sort('sortOrder', 1)
            ->distinct('identifier')
            ->getQuery()->execute()->toArray();
    }

    /**
     * Добавление новой страницы категории при скроллинге
     *
     * @Route("/scroller_add_page/{slug}/{sort}", name="scroller_add_page", defaults={"sort"=""})
     * @Template("NitraProductBundle:Aditional:scrollerAddPage.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request     $request    Request instance
     * @param string                                        $slug       Category alias
     * @param string                                        $sort       Sort mode
     *
     * @return array Template parameters
     */
    public function scrollerAddPageAction(Request $request, $slug, $sort)
    {
        $category = $this->findCategory($slug);

        if (!$category) {
            throw $this->createNotFoundException(sprintf('Category by alias "%s" not found', $slug));
        }

        $filter           = $this->getFilter();
        $allowedParamsIds = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Category')
            ->getAllowedParametersIds($category);

        $qb = $filter->filter($this->getQbForFilter($category), $allowedParamsIds);
        $qb->sort($this->getFilterSorts($sort));

        // paginate products
        $products = $this->paginate($qb);

        // set route name for paginator
        $products->setUsedRoute('category_page');

        return array(
            'limit'             => $request->getSession()->get(
                'limit',
                $request->get(
                    'limit',
                    Globals::getStoreLimit('page', 10)
                )
            ),
            'products'          => $products,
            'currentPage'       => $request->query->get('page', 1),
        );
    }

    /**
     * Фильтр по категориям
     *
     * @Template("NitraProductBundle:Category:categoryFilter.html.twig")
     *
     * @param string        $routeName          Route name
     * @param array         $routeParameters    Route parameters
     * @param string        $active             Alias(es) of active category or categories (separator - ",")
     * @param array         $ids                Allowed categories ids
     * @param integer       $minLevel           Minimal level for select categories
     * @param boolean       $deactivate         Allow deselect categories
     * @param boolean       $onlyOne            On select two categories - deactivate first
     *
     * @return array Template parameters
     */
    public function categoryFilterAction($routeName, $routeParameters, $active = null, $ids = array(), $minLevel = 0, $deactivate = true, $onlyOne = false)
    {
        $result = array();
        $categories = $this->getCategoriesForFilter($minLevel, $ids);

        foreach ($categories as $category) {
            list ($isActive, $link) = $this->generateLinkToCategoryFilter($category, $routeName, $routeParameters, $active, $deactivate, $onlyOne);

            $result[$category->getAlias()] = array(
                'name'   => $category->getName(),
                'path'   => $link,
                'active' => $isActive,
            );
        }

        return array(
            'categories' => $result,
        );
    }

    /**
     * Get categories for filter
     *
     * @param integer   $minLevel   Minimal level for select
     * @param array     $ids        Allowed categories ids
     *
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getCategoriesForFilter($minLevel, $ids)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->field('level')->gte($minLevel);
        if ($ids) {
            $qb->field('_id')->in($ids);
        }

        return $qb
            ->sort('path')
            ->getQuery()->execute();
    }

    /**
     * Generate link to page with filter by category
     *
     * @param string    $routeName              Route name
     * @param array     $parameters             Route parameters
     * @param string    $selectedCategories     Alias(es) of active category or categories (separator - ",")
     * @param boolean   $deactivate             Allow deselect categories
     * @param boolean   $onlyOne                On select two categories - deactivate first
     *
     * @return array Is active flag and generated link to page with category filter
     */
    protected function generateLinkToCategoryFilter($category, $routeName, $parameters, $selectedCategories, $deactivate, $onlyOne)
    {
        $currentActive = false;
        $aliases       = $selectedCategories ? explode(',', $selectedCategories) : array();

        if (in_array($category->getAlias(), $aliases)) {
            $currentActive = true;
            unset($aliases[array_search($category->getAlias(), $aliases)]);
        } elseif ($onlyOne) {
            $aliases = array($category->getAlias());
        } else {
            $aliases[] = $category->getAlias();
        }

        if ($aliases) {
            $parameters['category'] = implode(',', $aliases);
        } elseif ($deactivate) {
            unset ($parameters['category']);
        }

        return array(
            $currentActive,
            $this->generateUrl($routeName, $parameters)
        );
    }
}