<?php

namespace Nitra\ProductBundle\Controller\Model;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModelController extends NitraController
{
    /**
     * Model page action
     *
     * @Route("/model/{slug}", name="model_page")
     * @Template("NitraProductBundle:Model:modelPage.html.twig")
     * @Cache(smaxage="300", public="true")
     *
     * @param string $slug  Transliterated model alias
     *
     * @return array        Template parameters
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function modelPageAction($slug)
    {
        $model = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->field('aliasEn')->equals($slug)
            ->getQuery()->getSingleResult();

        if (!$model) {
            throw $this->createNotFoundException(sprintf('Model by alias "%s" not found', $slug));
        }

        return array(
            'model'  => $model,
            'prices' => $this->getPriceRange($model),
        );
    }

    /**
     * Обновление количества просмотров модели
     *
     * @param \Symfony\Component\HttpFoundation\Request $request    Request instance
     * @param string                                    $modelId    Id of model
     *
     * @return \Nitra\ProductBundle\Controller\Model\Response       Empty response
     */
    public function updateModelViewsAction(Request $request, $modelId)
    {
        $viewed = $request->getSession()->get('viewed_models', array());

        // проверем первый ли раз в данной сесии просматривается эта модель
        if (!in_array($modelId, $viewed)) {
            $model   = $this->getDocumentManager()->find('NitraProductBundle:Model', $modelId);
            if ($model) {
                $model->setViewed($model->getViewed() + 1);
                $this->getDocumentManager()->flush();

                // добавляем модель к просмотренным
                $viewed[] = $modelId;
                $request->getSession()->set('viewed_models', $viewed);
            }
        }

        return new Response();
    }

    /**
     * Get price range of model products
     *
     * @param \Nitra\ProductBundle\Document\Model $model
     *
     * @return array
     */
    protected function getPriceRange($model)
    {
        $store  = $this->getStore();

        $prices = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Product')
            ->distinct('storePrice.' . $store['id'] . '.price')
            ->field('model.id')->equals($model->getId())
            ->getQuery()->execute()->toArray();

        array_multisort($prices, SORT_NUMERIC);

        return $prices;
    }
}