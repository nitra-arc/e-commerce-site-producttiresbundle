<?php

namespace Nitra\ProductBundle\Controller\Parameter;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ParameterController extends NitraController
{
    /**
     * Get parameters in description
     *
     * @Template("NitraProductBundle:Parameter:characteristics.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $item   Product or model instance
     * @param boolean                                                                   $short  Get short or full characteristics
     * @param string                                                                    $type   Parameters type. Only <b>product, model</b> and <b>all</b> is allowed
     *
     * @return array Template parameters
     */
    public function characteristicsAction($item, $short = false, $type = null)
    {
        return $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Product')
            ->getProductOrModelCharacteristics($item, $short, $type);
    }
}