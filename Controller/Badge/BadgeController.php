<?php

namespace Nitra\ProductBundle\Controller\Badge;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class BadgeController extends NitraController
{
    /**
     * Блок бейджей
     *
     * @Template("NitraProductBundle:Badge:badgeProductsBlock.html.twig")
     *
     * @param int       $visible        Amount of visible items
     * @param string    $imageSize      Imagine filter
     * @param string    $type           Carousels type
     * @param string    $orientation    Carousels orientation
     *
     * @return array Template parameters
     */
    public function badgeProductsBlockAction($visible = 4, $imageSize = 'big_carousel_thumb', $type = 'extended', $orientation = 'horizontal')
    {
        $badges = $this->getBadgesInMainPage();

        $items = array();
        foreach ($badges as $badge) {
            $repository = $this
                ->getDocumentManager()
                ->getRepository('NitraProductBundle:Product');
            $qb = $repository
                ->getDefaultQb()
                ->field('badge.id')->equals($badge->getId());
            $products = $repository
                ->getProductsAsArray($qb, 'badged_products_' . $badge->getId(), 60 * 60);

            $repository->addReviewsToProductArray($products);

            if (count($products)) {
                $items[$badge->getId()] = array(
                    'products' => $products,
                    'badge'    => $badge,
                );
            }
        }

        return array(
            'visible'       => $visible,
            'imageSize'     => $imageSize,
            'type'          => $type,
            'orientation'   => $orientation,
            'items'         => $items,
        );
    }

    /**
     * Carousel of badged products
     *
     * @Template("NitraProductBundle:Carousels:products.html.twig")
     *
     * @param string                                    $badgeIdentifier
     * @param integer                                   $limit
     * @param string                                    $class
     * @param string                                    $caption
     * @param integer                                   $visible
     * @param string                                    $type
     * @param string                                    $orientation
     * @param string                                    $imageSize
     * @param boolean                                   $autoWidth
     * @param boolean                                   $responsive
     *
     * @return array Template parameters
     */
    public function productsCarouselAction($badgeIdentifier, $limit = 10, $class = 'badge_products', $caption = null, $visible = 7, $type = 'expanded', $orientation = 'horizontal', $imageSize = 'big_carousel_thumb', $autoWidth = false, $responsive = false)
    {
        $badge  = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Badge')
            ->findOneByIdentifier($badgeIdentifier);

        if (!$badge) {
            throw $this->createNotFoundException(sprintf('Badge by identifier "%s" not found', $badgeIdentifier));
        }

        $repository = $this
            ->getDocumentManager()
            ->getRepository('NitraProductBundle:Product');
        $qb = $repository
            ->getDefaultQb()
            ->field('badge.id')->equals($badge->getId())
            ->limit($limit);
        $products = $repository
            ->getProductsAsArray($qb, 'badged_products_' . $badge->getId(), 60 * 60);

        $repository->addReviewsToProductArray($products);

        return array(
            'items'       => $products,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
            'class'       => $class,
            'caption'     => $caption ?: (string) $badge,
            'type'        => $type,
            'autoWidth'   => $autoWidth,
            'responsive'  => $responsive,
        );
    }

    /**
     * Get badges in which isInMainPage flag is true
     *
     * @return \Nitra\ProductBundle\Document\Badge[]
     */
    protected function getBadgesInMainPage()
    {
        return $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Badge')
            ->field('isInMainPage')->equals(true)
            ->sort('sortOrderOnMainPage')
            ->getQuery()->execute();
    }

    /**
     * Get products for badge
     *
     * @param string        $badgeId    Badge id
     * @param null|integer  $limit      Products limit
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    protected function getBadgeProducts($badgeId, $limit = null)
    {
        return $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('badge.id')->equals($badgeId)
            ->limit($limit)
            ->getQuery()->execute();
    }

    /**
     * Cтраница бейджевых товаров
     *
     * @Route("/badge-products/{identifier}/{sort}", name="badge_products_page", defaults={"sort"=""})
     * @Template("NitraProductBundle:Badge:badgeProductsPage.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request     $request        Request instance
     * @param string                                        $identifier     Badge identifier
     * @param string|null                                   $sort           Sort field
     *
     * @return array Template parameters
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function badgeProductPageAction(Request $request, $identifier, $sort = null)
    {
        $badge  = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Badge')
            ->findOneByIdentifier($identifier);

        if (!$badge) {
            throw $this->createNotFoundException(sprintf('Badge by identifier "%s" not found', $identifier));
        }

        $qb = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('badge.id')->equals($badge->getId())
            ->sort($this->getBadgeProdcutsPageSortParameters($sort));

        $selectCategories = $request->query->get('category');
        $categoriesIds    = $this->addCategoryFilterToQuery($qb, $selectCategories);

        $products         = $this->paginate($qb->getQuery());

        return array(
            'activeCats'    => $selectCategories,
            'products'      => $products,
            'categoriesIds' => $categoriesIds,
            'badge'         => $badge,
        );
    }

    /**
     * Get sort field for badge products page
     *
     * @param string $sort
     *
     * @return string|array
     */
    protected function getBadgeProdcutsPageSortParameters($sort)
    {
        $sortField = null;
        $store     = $this->getStore();

        switch ($sort) {
            case 'name':
                $sortField = 'fullNames.' . $store['id'];
                break;
            default:
                $sortField = 'storePrice.' . $store['id'] . '.price';
                break;
        }

        return $sortField;
    }

    /**
     * Add filter by category to products query
     * And get categories from products
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb
     * @param string                                $selectCategories
     *
     * @return array Categories
     */
    protected function addCategoryFilterToQuery($qb, $selectCategories)
    {
        // товары отфильтрованные по категории
        if ($selectCategories) {
            $categoriesIds = $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Category')
                ->distinct('_id')
                ->field('aliasEn')->in(explode(',', $selectCategories))
                ->getQuery()->execute()->toArray();

            $modelsIds = $this->getDocumentManager()
                ->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id')
                ->field('category.id')->in($categoriesIds)
                ->getQuery()->execute()->toArray();

            $qb->field('model.id')->in($modelsIds);
        }

        return $this->getCategoriesFromProducts($qb->getQueryArray());
    }

    /**
     * Get categoriesIds from products
     *
     * @param array $query
     *
     * @return \MongoId[]
     */
    protected function getCategoriesFromProducts($query)
    {
        $products = $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('model')
            ->setQueryArray($query)
            ->getQuery()->execute()->toArray();

        $modelsIds = array();
        foreach ($products as $product) {
            $modelsIds[] = $product['model']['$id'];
        }

        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('category.$id')
            ->field('_id')->in($modelsIds)
            ->getQuery()->execute()->toArray();
    }
}