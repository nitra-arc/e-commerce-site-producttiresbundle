/* 
 * обьект Scroller
 * @param integer currentPage - текущая страница, default = 1
 * @param string actionUrl - роутинг для получения страницы
 * @param integer maxPagesToDisplay - максимальное кол-во отображаемых страниц при скроллинге, default = false
 * @param string buttonAddId - id кнопки "отобразить еще товаров", есле не передать, то добавление товаров будет автоматическое
 */
var Scroller = function(options) {
    this.options = $.extend({
        // текущая страница
        currentPage:       1,
        // url для получения следующей страницы продуктов
        actionUrl:         '',
        // максимальное количество отображаемых страниц
        maxPagesToDisplay: false,
        buttonAddId:       false,
        click:             function(scroller) {
            scroller.options.addPage(scroller);
        },
        success:           function(data, scroller) {
            // ответ от сервера получен успешно
            if (data && (data.trim() != '')) {
                $('div.bottom_navigation').remove();
                $('.product_list:last').after(data);
                // заменяем верхний пагинатор
                $('div.pagination').replaceWith($('div.bottom_navigation').find('.pagination').clone());
                // установлление позиции всей страницы на последнюю добавленную страницу категории
                //$(window).scrollTop($('.page_count:last').offset().top);
            } else {
                // все страницы были отображены
                scroller.noPagesToDisplay = true;
            }
        },
        addPage:           function(scroller) {
            if (scroller.noPagesToDisplay || scroller.inProcess) {
                return;
            }
            scroller.options.currentPage ++;
            $('#loader').addClass('loader');
            scroller.inProcess = $.ajax({
                url:        scroller.options.actionUrl.replace(/&amp;/g, '&'),
                type:       'GET',
                data:       {
                    page: scroller.options.currentPage
                },
                dataType:   'html',
                success:    function(data) {
                    scroller.options.success(data, scroller);
                    scroller.inProcess = null;
                }
            });
        },
        scroll:            function(scroller) {
            if (!scroller.inProcess && !scroller.noPagesToDisplay && ((scroller.options.currentPage + 1) % scroller.options.maxPagesToDisplay != 0) && ($(window).scrollTop() + $(window).height()) >= ($('footer.footer').offset().top + $('footer.footer').height() * 2 - 736)) {
                $('#scroll_loader').show();
                scroller.options.addPage(scroller);
                $('#scroll_loader').hide();
            }
        }
    }, options);

    // статус выполнения операции
    this.inProcess = false;
    
    // флаг что все страницы уже отображены
    this.noPagesToDisplay = false;

    var self = this;
    /**
     * подгрузка следующей страницы
     * @returns html страница продуктов | ''
     */
    if (!this.options.buttonAddId) {
        $(document).scroll(function() {
            self.options.scroll(self);
        });
    } else {
        $('#' + this.options.buttonAddId).live('click', function() {
            self.options.click(self);
        });
    }
};