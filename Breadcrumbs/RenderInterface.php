<?php

namespace Nitra\ProductBundle\Breadcrumbs;

interface RenderInterface
{
    /**
     * Get support class or classes
     * 
     * @return array|string List or one support class for render
     */
    public function getSupportClass();

    /**
     * Get template for render
     *
     * @return string|null Template for render breadcrumbs
     */
    public function getTemplate();

    /**
     * Get bradscrumbs as array for render
     *
     * @param object $object Document instance of one support class
     *
     * @return BreadcrumbItem[] List of bradcrumb items
     */
    public function getBreadcrumbs($object);
}