<?php

namespace Nitra\ProductBundle\Breadcrumbs\Exceptions;

class InvalidSupportClassException extends \Exception
{
    /**
     * Constructor.
     *
     * @param mixed $supportClasses Result of getSupportClass
     */
    public function __construct($supportClasses)
    {
        $message = 'The method getSupportClass of RenderInterface instance '
            . 'must return string or array<string>. Given '
            . $this->getSupportClassesType($supportClasses);

        parent::__construct($message, 500, null);
    }

    /**
     * Get type of support classes type
     *
     * @param mixed $supportClasses
     * 
     * @return string Type of $supportClasses variable or if array - imploded each types
     */
    protected function getSupportClassesType($supportClasses)
    {
        if (!is_array($supportClasses)) {
            return gettype($supportClasses);
        }

        $items = array();
        foreach ($supportClasses as $supportClass) {
            $items[] = gettype($supportClass);
        }

        $uniqueItems = array_unique($items);

        return 'array of <' . implode(', ', $uniqueItems) . '>';
    }
}