<?php

namespace Nitra\ProductBundle\Breadcrumbs\Renders;

use Nitra\ProductBundle\Breadcrumbs\AbstractRender;
use Nitra\ProductBundle\Breadcrumbs\BreadcrumbItem;

class CategoryModelProductRender extends AbstractRender
{
    protected $mode;

    /**
     * Constructor
     *
     * @param string $mode
     */
    public function __construct($mode)
    {
        $this->mode = $mode;
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbs($object)
    {
        $append = array();

        if ($object instanceof \Nitra\ProductBundle\Document\Category) {
            $category = $object;
        } elseif ($object instanceof \Nitra\ProductBundle\Document\Model) {
            $category = $object->getCategory();
            $append   = $this->getAppendForModel($object);
        } elseif ($object instanceof \Nitra\ProductBundle\Document\Product) {
            $category = $object->getModel()->getCategory();
            $append   = $this->getAppendForProduct($object);
        }

        try {
            $items = array_reverse($this->getCategoriesBreadcrumbs($category));

            return array_merge($items, $append);
        // for prevent 500 error on breadcrumbs
        // if one or more from categories tree is not active
        } catch (\Doctrine\ODM\MongoDB\DocumentNotFoundException $e) {
            return $append;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportClass()
    {
        return array(
            '\Nitra\ProductBundle\Document\Category',
            '\Nitra\ProductBundle\Document\Model',
            '\Nitra\ProductBundle\Document\Product',
        );
    }

    /**
     * Recursive get breadcrumbs for categories
     *
     * @param \Nitra\ProductBundle\Document\Category    $category
     * @param BreadcrumbItem[]                          $result
     *
     * @return BreadcrumbItem[]
     *
     * @throws \Doctrine\ODM\MongoDB\DocumentNotFoundException
     */
    protected function getCategoriesBreadcrumbs($category, array $result = array())
    {
        if ($category instanceof \Doctrine\ODM\MongoDB\Proxy\Proxy && !$category->getIsActive()) {
            throw new \Doctrine\ODM\MongoDB\DocumentNotFoundException();
        }

        $result[] = new BreadcrumbItem($category, 'category_page', array(
            'slug' => $category,
        ));

        return $category->getParent()
            ? $this->getCategoriesBreadcrumbs($category->getParent(), $result)
            : $result;
    }

    /**
     * Get append items for model
     *
     * @param \Nitra\ProductBundle\Document\Model $model
     *
     * @return BreadcrumbItem[]
     */
    protected function getAppendForModel($model)
    {
        return array(
            new BreadcrumbItem($model, 'model_page', array(
                'slug' => $model,
            )),
        );
    }

    /**
     * Get append items for product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return BreadcrumbItem[]
     */
    protected function getAppendForProduct($product)
    {
        $items = array();
        if ($this->mode == 'model') {
            $items[] = new BreadcrumbItem($product->getModel(), 'model_page', array(
                'slug' => $product->getModel(),
            ));
        }
        $items[] = new BreadcrumbItem($product, 'product_page', array(
            'slug' => $product,
        ));

        return $items;
    }
}