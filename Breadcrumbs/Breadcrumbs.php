<?php

namespace Nitra\ProductBundle\Breadcrumbs;

/**
 * Render breadcrumbs
 */
class Breadcrumbs
{
    /**
     * @var string Template for render breadcrumbs
     */
    protected $template;

    /**
     * @var \Twig_Environment Twig environment instance
     */
    protected $twig;

    /**
     * @var RenderInterface[] List of renders
     */
    protected $renders = array();

    /**
     * Constructor
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;

        $this->setDefaultTemplate('NitraProductBundle::breadcrumbs.html.twig');
    }

    /**
     * Set default template
     *
     * @param string $template
     *
     * @return self
     */
    public function setDefaultTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Get default template
     *
     * @return string
     */
    public function getDefaultTemplate()
    {
        return $this->template;
    }

    /**
     * Add breancrumbs render
     *
     * @param \Nitra\ProductBundle\Breadcrumbs\RenderInterface $render
     */
    public function addRender(RenderInterface $render)
    {
        $classes = $this->getClassesFromRender($render);

        foreach ($classes as $class) {
            $this->renders[$class] = $render;
        }
    }

    /**
     * Get support classes from render
     *
     * @param \Nitra\ProductBundle\Breadcrumbs\RenderInterface $render
     *
     * @return array Support classes as string
     *
     * @throws Exceptions\InvalidSupportClassException
     */
    protected function getClassesFromRender(RenderInterface $render)
    {
        $classes = is_array($render->getSupportClass())
            ? $render->getSupportClass()
            : array($render->getSupportClass());

        foreach ($classes as $class) {
            if (!is_string($class)) {
                throw new Exceptions\InvalidSupportClassException($classes);
            }
        }

        return $classes;
    }

    /**
     * Get renders
     *
     * @return RenderInterface[] List of renders by class
     */
    public function getRenders()
    {
        return $this->renders;
    }

    /**
     * Render breadcrumbs
     *
     * @param object $object
     *
     * @return string|null <b>html</b> Rendered breadcrumbs or null
     *
     * @throws Exceptions\InvalidArgumentException
     */
    public function render($object)
    {
        if (!is_object($object)) {
            throw new Exceptions\InvalidArgumentException($object);
        }

        $render = $this->getRenderForObject($object);

        if (!$render) {
            return;
        }

        $template    = $render->getTemplate() ? : $this->getDefaultTemplate();

        $breadcrumbs = $render->getBreadcrumbs($object);

        return $this->twig->render($template, array(
            'breadcrumbs' => $breadcrumbs,
        ));
    }

    /**
     * Get render instance by object if exists
     *
     * @param object $object
     *
     * @return RenderInterface|null Render instance or <b>null</b> if support render not exists
     */
    protected function getRenderForObject($object)
    {
        foreach ($this->getRenders() as $class => $render) {
            if ($object instanceof $class) {
                return $render;
            }
        }
    }
}