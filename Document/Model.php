<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Модель товара(ов)
 * @ODM\Document(collection="Models")
 */
class Model
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Nitra\ProductBundle\Traits\ModeratedDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Имя
     * @ODM\String
     * @Gedmo\Translatable
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var \Nitra\ProductBundle\Document\Category Категория
     * @ODM\ReferenceOne(targetDocument="Category")
     * @Assert\NotBlank
     */
    protected $category;

    /**
     * @var \Nitra\ProductBundle\Document\Brand Производитель
     * @ODM\ReferenceOne(targetDocument="Brand")
     * @Assert\NotBlank
     */
    protected $brand;

    /**
     * @var string Изображение
     * @ODM\String
     */
    protected $image;

    /**
     * @var array Изображения
     * @ODM\Hash
     */
    protected $images;

    /**
     * @var string Описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var \Nitra\SeoBundle\Document\EmbedSeo SEO описание
     * @ODM\EmbedOne(targetDocument="Nitra\SeoBundle\Document\EmbedSeo")
     */
    protected $seo;

    /**
     * @var int Количество уникальных посещений страницы модели
     * @ODM\Int
     */
    protected $viewed;

    /**
     * @var boolean Популярная ли?
     * @ODM\Boolean
     */
    protected $isPopular;

    /**
     * @var \Nitra\ProductBundle\Document\Product Связь на товары
     * @ODM\ReferenceMany(targetDocument="Product", mappedBy="model")
     */
    protected $products;

    /**
     * @var \Nitra\StoreBundle\Document\Store[] Магазины на которых выводить
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * @var string Полные имена модели по магазинам
     * @ODM\Hash
     */
    protected $fullNames = array();

    /**
     * @var string Сокращенные имена модели по магазинам
     * @ODM\Hash
     */
    protected $shortNames = array();

    /**
     * @var array Массив цен на товары по магазину
     * @ODM\Hash
     */
    protected $prices = array();

    /**
     * @var string Полное имя для текущего магазина
     */
    protected $currentStoreFullName;

    /**
     * @var string Сокращенное имя для текущего магазина
     */
    protected $currentStoreShortName;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stores   = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        if ($this->currentStoreFullName) {
            return $this->currentStoreFullName;
        }
        if ($this->currentStoreShortName) {
            return $this->currentStoreShortName;
        }

        return $this->name;
    }

    /**
     * Get id
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return \Brand
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     * @param \Nitra\ProductBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return \Nitra\ProductBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set images
     * @param array $images
     * @return self
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * Get images
     * @return array $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set seo
     * @param \Nitra\SeoBundle\Document\EmbedSeo $seo
     * @return self
     */
    public function setSeo($seo)
    {
        $this->seo = $seo;
        return $this;
    }

    /**
     * Get seo
     * @return \Nitra\SeoBundle\Document\EmbedSeo $seo
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set viewed
     * @param int $viewed
     * @return self
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
        return $this;
    }

    /**
     * Get viewed
     * @return int $viewed
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set isPopular
     * @param bool $isPopular
     * @return self
     */
    public function setIsPopular($isPopular)
    {
        $this->isPopular = $isPopular;
        return $this;
    }

    /**
     * Get isPopular
     * @return boll $isPopular
     */
    public function getIsPopular()
    {
        return $this->isPopular;
    }

    /**
     * Get stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     * @param \Nitra\StoreBundle\Document\Store[] $stores
     * @return self
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
        return $this;
    }

    /**
     * Add stores
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
        return $this;
    }

    /**
     * Remove stores
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        foreach ($this->stores as $key => $existsStore) {
            if ($existsStore->getId() == $store->getId()) {
                $this->stores->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Has products
     * @return boolean
     */
    public function hasProducts()
    {
        return (bool) $this->products->count();
    }

    /**
     * Get products
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return self
     */
    public function addProduct(Product $product)
    {
        $this->products->add($product);
        $product->setModel($this);
        return $this;
    }

    /**
     * Remove product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return self
     */
    public function removeProduct(Product $product)
    {
        foreach ($this->products as $key => $existsProduct) {
            if ($existsProduct->getId() == $product->getId()) {
                $this->products->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Set fullNames
     * @param array $fullNames
     * @return self
     */
    public function setFullNames(array $fullNames)
    {
        $this->fullNames = $fullNames;
        return $this;
    }

    /**
     * Get fullNames
     * @return array $fullNames
     */
    public function getFullNames()
    {
        return $this->fullNames;
    }

    /**
     * Set shortNames
     * @param array $shortNames
     * @return self
     */
    public function setShortNames(array $shortNames)
    {
        $this->shortNames = $shortNames;
        return $this;
    }

    /**
     * Get shortNames
     * @return array $shortNames
     */
    public function getShortNames()
    {
        return $this->shortNames;
    }

    /**
     * Set prices
     * @param array $prices
     * @return self
     */
    public function setPrices(array $prices)
    {
        $this->prices = $prices;
        return $this;
    }

    /**
     * Get prices
     * @return array $prices
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set full name for current store
     * @param string $fullName
     * @return self
     */
    public function setFullName($fullName)
    {
        $this->currentStoreFullName = $fullName;
        return $this;
    }

    /**
     * Get full name for current store
     * @return string
     */
    public function getFullName()
    {
        return $this->currentStoreFullName;
    }

    /**
     * Set short name for current store
     * @param string $shortName
     * @return self
     */
    public function setShortName($shortName)
    {
        $this->currentStoreShortName = $shortName;
        return $this;
    }

    /**
     * Get short name for current store
     * @return string
     */
    public function getShortName()
    {
        return $this->currentStoreShortName;
    }
}