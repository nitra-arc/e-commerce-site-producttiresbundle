<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Nitra\ProductBundle\Sluggable\Mapping\Annotation\Slug;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document(collection="Parameters")
 */
class Parameter
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Имя параметра
     * @ODM\String
     * @Assert\NotBlank
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Внутреннее имя
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $internalName;

    /**
     * @var string Группа параметров
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $group;

    /**
     * @var string Количество значений параметра
     * @ODM\String
     * @Assert\Choice(
     *      choices={"text", "digit", "choice"},
     *      message="Тип значения параметра может быть текст, цифры, список значений."
     * )
     */
    protected $valueType;

    /**
     * @var string Тип параметра (модель или товар)
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Choice(
     *      choices={"model", "product"},
     *      message = "Параметр может быть либо для модели либо для товара ."
     * )
     */
    protected $type;

    /**
     * @var string Суффикс параметра
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $suffix;

    /**
     * @var boolean В фильтрах
     * @ODM\Boolean
     */
    protected $isFilter;

    /**
     * @var boolean В коротком описании
     * @ODM\Boolean
     */
    protected $isShortDescription;

    /**
     * @var boolean В описании
     * @ODM\Boolean
     */
    protected $isDescription;

    /**
     * @var int Порядок сортировки в фильтрах
     * @ODM\Int
     */
    protected $sortOrderInFilter;

    /**
     * @var int Порядок сортировки в коротком описании
     * @ODM\Int
     */
    protected $sortOrderInShortDescription;

    /**
     * @var int Порядок сортировки в описании
     * @ODM\Int
     */
    protected $sortOrderInDescription;

    /**
     * @var Category[] Категории, на которые распространяется параметр
     * @ODM\ReferenceMany(targetDocument="Category")
     */
    protected $categories;

    /**
     * @var \Nitra\StoreBundle\Document\Store[] Магазины к которым относится
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * @var ParameterValue[] Значения параметра
     * @ODM\ReferenceMany(
     *      targetDocument="ParameterValue",
     *      cascade={"remove", "persist"},
     *      mappedBy="parameter",
     *      sort={"sortOrder"=1, "name"=1}
     * )
     */
    protected $parameterValues;

    /**
     * @var array Диапазоны
     * @ODM\Hash
     */
    protected $parameterRanges;

    /**
     * @var boolean Отображать скрытым или раскрытым в фильтре
     * @ODM\Boolean
     */
    protected $isFilterShow;

    /**
     * @var boolean Возможен ли множественный выбор значений
     * @ODM\Boolean
     */
    protected $isMultiple;

    /**
     * @var boolean Возможно ли добавлять новые значения
     * @ODM\Boolean
     */
    protected $isDynamicValues;

    /**
     * @var boolean Множественный ли выбор значений на сайте
     * @ODM\Boolean
     */
    protected $isSiteMultiple;

    /**
     * @var boolean Выводить параметр в подборщике по параметрам
     * @ODM\Boolean
     */
    protected $isParameterSelector;

    /**
     * @var integer Порядок сортировки в подборщике по параметрам
     * @ODM\Int
     */
    protected $sortOrderInParameterSelector;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories      = new ArrayCollection();
        $this->stores          = new ArrayCollection();
        $this->parameterValues = new ArrayCollection();
    }

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->internalName ? : $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set internalName
     * @param string $internalName
     * @return self
     */
    public function setInternalName($internalName)
    {
        $this->internalName = $internalName;
        return $this;
    }

    /**
     * Get internalName
     * @return string $internalName
     */
    public function getInternalName()
    {
        return $this->internalName ? : $this->name;
    }

    /**
     * Set valueType
     * @param string $valueType
     * @return self
     */
    public function setValueType($valueType)
    {
        $this->valueType = $valueType;
        return $this;
    }

    /**
     * Get valueType
     * @return string $valueType
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * Set type
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set suffix
     * @param string $suffix
     * @return self
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * Get suffix
     * @return string $suffix
     */
    public function getSuffix()
    {
        return $this->suffix;
    }


    /**
     * Set isFilter
     * @param boolean $isFilter
     * @return self
     */
    public function setIsFilter($isFilter)
    {
        $this->isFilter = $isFilter;
        return $this;
    }

    /**
     * Get isFilter
     * @return boolean $isFilter
     */
    public function getIsFilter()
    {
        return $this->isFilter;
    }

    /**
     * Set isShortDescription
     * @param boolean $isShortDescription
     * @return self
     */
    public function setIsShortDescription($isShortDescription)
    {
        $this->isShortDescription = $isShortDescription;
        return $this;
    }

    /**
     * Get isShortDescription
     * @return boolean $isShortDescription
     */
    public function getIsShortDescription()
    {
        return $this->isShortDescription;
    }

    /**
     * Set isDescription
     * @param boolean $isDescription
     * @return self
     */
    public function setIsDescription($isDescription)
    {
        $this->isDescription = $isDescription;
        return $this;
    }

    /**
     * Get isDescription
     * @return boolean $isDescription
     */
    public function getIsDescription()
    {
        return $this->isDescription;
    }

    /**
     * Set sortOrderInFilter
     * @param int $sortOrderInFilter
     * @return self
     */
    public function setSortOrderInFilter($sortOrderInFilter)
    {
        $this->sortOrderInFilter = $sortOrderInFilter;
        return $this;
    }

    /**
     * Get sortOrderInFilter
     * @return int $sortOrderInFilter
     */
    public function getSortOrderInFilter()
    {
        return $this->sortOrderInFilter;
    }

    /**
     * Set sortOrderInShortDescription
     * @param int $sortOrderInShortDescription
     * @return self
     */
    public function setSortOrderInShortDescription($sortOrderInShortDescription)
    {
        $this->sortOrderInShortDescription = $sortOrderInShortDescription;
        return $this;
    }

    /**
     * Get sortOrderInShortDescription
     * @return int $sortOrderInShortDescription
     */
    public function getSortOrderInShortDescription()
    {
        return $this->sortOrderInShortDescription;
    }

    /**
     * Set sortOrderInDescription
     * @param int $sortOrderInDescription
     * @return self
     */
    public function setSortOrderInDescription($sortOrderInDescription)
    {
        $this->sortOrderInDescription = $sortOrderInDescription;
        return $this;
    }

    /**
     * Get sortOrderInDescription
     * @return int $sortOrderInDescription
     */
    public function getSortOrderInDescription()
    {
        return $this->sortOrderInDescription;
    }

    /**
     * Add category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * Remove category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function removeCategory(Category $category)
    {
        foreach ($this->categories as $key => $existsCategory) {
            if ($existsCategory->getId() == $category->getId()) {
                $this->categories->remove($key);
            }
        }

        return $this;
    }

    /**
     * Get categories
     * @return \Nitra\ProductBundle\Document\Category[] $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
        return $this;
    }

    /**
     * Remove store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        foreach ($this->stores as $key => $existsStore) {
            if ($existsStore->getId() == $store->getId()) {
                $this->stores->remove($key);
            }
        }

        return $this;
    }

    /**
     * Get stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Add parameterValue
     * @param \Nitra\ProductBundle\Document\ParameterValue $parameterValue
     * @return self
     */
    public function addParameterValue(ParameterValue $parameterValue)
    {
        $parameterValue->setParameter($this);
        $this->parameterValues[] = $parameterValue;
        return $this;
    }

    /**
     * Remove parameterValue
     * @param \Nitra\ProductBundle\Document\ParameterValue $parameterValue
     * @return self
     */
    public function removeParameterValue(ParameterValue $parameterValue)
    {
        foreach ($this->parameterValues as $key => $existsParameterValue) {
            if ($existsParameterValue->getId() == $parameterValue->getId()) {
                $this->parameterValues->remove($key);
            }
        }

        return $this;
    }

    /**
     * Get parameterValues
     * @return \Nitra\ProductBundle\Document\ParameterValue[] $parameterValues
     */
    public function getParameterValues()
    {
        return $this->parameterValues;
    }

    /**
     * Set parameterValues
     * @param \Nitra\ProductBundle\Document\ParameterValue[] $parameterValues
     * @return self
     */
    public function setParameterValues($parameterValues)
    {
        $this->parameterValues = $parameterValues;
        return $this;
    }

    /**
     * Set group
     * @param string $group
     * @return self
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * Get group
     * @return string $group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set isFilterShow
     * @param boolean $isFilterShow
     * @return self
     */
    public function setIsFilterShow($isFilterShow)
    {
        $this->isFilterShow = $isFilterShow;
        return $this;
    }

    /**
     * Get isFilterShow
     * @return boolean $isFilterShow
     */
    public function getIsFilterShow()
    {
        return $this->isFilterShow;
    }

    /**
     * Set isMultiple
     * @param boolean $isMultiple
     * @return self
     */
    public function setIsMultiple($isMultiple)
    {
        $this->isMultiple = $isMultiple;
        return $this;
    }

    /**
     * Get isMultiple
     * @return boolean $isMultiple
     */
    public function getIsMultiple()
    {
        return $this->isMultiple;
    }

    /**
     * Set isDynamicValues
     * @param boolean $isDynamicValues
     * @return self
     */
    public function setIsDynamicValues($isDynamicValues)
    {
        $this->isDynamicValues = $isDynamicValues;
        return $this;
    }

    /**
     * Get isDynamicValues
     * @return boolean $isDynamicValues
     */
    public function getIsDynamicValues()
    {
        return $this->isDynamicValues;
    }

    /**
     * Set parameterRanges
     * @param array $parameterRanges
     * @return self
     */
    public function setParameterRanges($parameterRanges)
    {
        $this->parameterRanges = $parameterRanges;
        return $this;
    }

    /**
     * Get parameterRanges
     * @return array $parameterRanges
     */
    public function getParameterRanges()
    {
        return $this->parameterRanges;
    }

    /**
     * Set isSiteMultiple
     * @param boolean $isSiteMultiple
     * @return self
     */
    public function setIsSiteMultiple($isSiteMultiple)
    {
        $this->isSiteMultiple = $isSiteMultiple;
        return $this;
    }

    /**
     * Get isSiteMultiple
     * @return boolean $isSiteMultiple
     */
    public function getIsSiteMultiple()
    {
        return $this->isSiteMultiple;
    }

    /**
     * Is parameterSelector
     * @param boolean $isParameterSelector
     * @return $this
     */
    public function setIsParameterSelector($isParameterSelector)
    {
        $this->isParameterSelector = $isParameterSelector;
        return $this;
    }

    /**
     * Is parameterSelector
     * @return boolean
     */
    public function getIsParameterSelector()
    {
        return $this->isParameterSelector;
    }

    /**
     * Set sortOrderInParameterSelector
     * @param integer $sortOrderInParameterSelector
     * @return $this
     */
    public function setSortOrderInParameterSelector($sortOrderInParameterSelector)
    {
        $this->sortOrderInParameterSelector = $sortOrderInParameterSelector;
        return $this;
    }

    /**
     * Get sortOrderInParameterSelector
     * @return integer
     */
    public function getSortOrderInParameterSelector()
    {
        return $this->sortOrderInParameterSelector;
    }
}