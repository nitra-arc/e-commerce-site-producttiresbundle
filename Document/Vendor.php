<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Vendors")
 */
class Vendor
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Изображение
     * @ODM\String
     */
    protected $image;

    /**
     * @var string Производитель (бренд)
     * @ODM\String
     */
    protected $name;

    /**
     * @var string Марка
     * @ODM\String
     */
    protected $vehicle;

    /**
     * @var string Год выпуска
     * @ODM\String
     */
    protected $year;

    /**
     * @var string Литраж | Модификация
     * @ODM\String
     */
    protected $modification;

    /**
     * @var string Алиас
     * @ODM\String
     * @Gedmo\Slug(fields={"name", "vehicle", "year", "modification"})
     */
    protected $alias;

    /**
     * @var array Шины
     * @ODM\Hash
     */
    protected $tires;

    /**
     * @var array Диски
     * @ODM\Hash
     */
    protected $wheels;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set vehicle
     * @param string $vehicle
     * @return self
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
        return $this;
    }

    /**
     * Get vehicle
     * @return string $vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set year
     * @param string $year
     * @return self
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * Get year
     * @return string $year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set modification
     * @param string $modification
     * @return self
     */
    public function setModification($modification)
    {
        $this->modification = $modification;
        return $this;
    }

    /**
     * Get modification
     * @return string $modification
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * Set alias
     * @param string $alias
     * @return self
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * Get alias
     * @return string $alias
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set tires
     * @param array $tires
     * @return self
     */
    public function setTires($tires)
    {
        $this->tires = $tires;
        return $this;
    }

    /**
     * Get tires
     * @return array $tires
     */
    public function getTires()
    {
        return $this->tires ?: array();
    }

    /**
     * Set wheels
     * @param array $wheels
     * @return self
     */
    public function setWheels($wheels)
    {
        $this->wheels = $wheels;
        return $this;
    }

    /**
     * Get wheels
     * @return array $wheels
     */
    public function getWheels()
    {
        return $this->wheels ?: array();
    }
}