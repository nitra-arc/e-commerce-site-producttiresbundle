<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Brands")
 */
class Brand
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Название
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var string Путь к изображению
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $image;

    /**
     * @var int Порядок сортировки
     * @ODM\Int
     */
    protected $sortOrder = 0;

    /**
     * @var \Nitra\StoreBundle\Document\Store[] Магазины
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
        return $this;
    }

    /**
     * Remove store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        foreach ($this->stores as $key => $existsStore) {
            if ($store->getId() == $existsStore->getId()) {
                $this->stores->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Get stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     * @return self
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
        return $this;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
}