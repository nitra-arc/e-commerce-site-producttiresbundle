<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints as ODMConstraints;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Товар
 * @ODM\Document(collection="Products", repositoryClass="Nitra\ProductBundle\Repository\ProductRepository")
 * @ODMConstraints\Unique(fields={"article"})
 * @Gedmo\SoftDeleteable
 */
class Product
{
    use \Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Nitra\ProductBundle\Traits\ModeratedDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Имя товара
     * @ODM\String
     * @Assert\NotBlank
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Полное имя для url для Ru локации
     * по типу parentCategory/childrenCategory/product
     * @ODM\String
     */
    protected $fullUrlAliasRu;

    /**
     * @var string Полное имя для url для En локации
     * по типу parentCategory/childrenCategory/product
     * @ODM\String
     */
    protected $fullUrlAliasEn;

    /**
     * @var string Полное имя для поиска
     * содержимое настраивается в конфиге, по умолчанию берется имя товара
     * @ODM\String
     */
    protected $fullNameForSearch;

    /**
     * @var string Полные имена товара по магазинам
     * @ODM\Hash
     */
    protected $fullNames = array();

    /**
     * @var string Сокращенные имена товара по магазинам
     * @ODM\Hash
     */
    protected $shortNames = array();

    /**
     * @var \Nitra\ProductBundle\Document\Model Модель товара
     * @ODM\ReferenceOne(targetDocument="Model", inversedBy="products")
     * @Assert\NotBlank
     */
    protected $model;

    /**
     * @var string Артикул, он же SKU
     * @ODM\String
     */
    protected $article;

    /**
     * @var int Счетчик товаров
     * Используется для автоматической генерации артикула
     * @ODM\Int
     */
    protected $counter;

    /**
     * @var string Описание с сайта производителя
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var string Дополнительное описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description2;

    /**
     * @var string Дополнительное описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description3;

    /**
     * @var string Ссылки на видео
     * @ODM\Hash
     */
    protected $videoRef;

    /**
     * @var array Ссылки на обзоры
     * @ODM\Hash
     */
    protected $reviewRef;

    /**
     * @var array Цены по магазинам
     * @ODM\Hash
     */
    protected $storePrice = array();

    /**
     * @var string Путь к основному изображению
     * @ODM\String
     */
    protected $image;

    /**
     * @var string Путь к дополнительному изображению
     * @ODM\String
     */
    protected $image2;

    /**
     * @var array Массив путей к дополнительным изображениям
     * @ODM\Hash
     */
    protected $images;

    /**
     * @var string Флэш
     * @ODM\String
     */
    protected $flash;

    /**
     * @var string Флэш превьюшка
     * @ODM\String
     */
    protected $flashPreview;

    /**
     * @var boolean Специальный ли?
     * @ODM\Boolean
     */
    protected $isSpecial;

    /**
     * @var boolean Активен?
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var boolean Популярен?
     * @ODM\Boolean
     */
    protected $isPopular;

    /**
     * @var \Nitra\ProductBundle\Document\Badge Бэйдж
     * @ODM\ReferenceOne(targetDocument="Badge")
     */
    protected $badge;

    /**
     * @var \Nitra\ProductBundle\Document\Color Цвет товара
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Color")
     */
    protected $color;

    /**
     * @var \Nitra\ProductBundle\Document\ProductParameter[] Значения параметров продукта
     * @ODM\EmbedMany(targetDocument="ProductParameter")
     */
    protected $parameters;

    /**
     * @var \Nitra\SeoBundle\Document\EmbedSeo SEO описание
     * @ODM\EmbedOne(targetDocument="Nitra\SeoBundle\Document\EmbedSeo")
     */
    protected $seoInfo;

    /**
     * @var \Nitra\ProductBundle\Document\Product[] Связь с другими товарами (акссесуары)
     * @ODM\ReferenceMany(targetDocument="Product")
     */
    protected $accessories;

    /**
     * @var string Наличие товара
     * @ODM\String
     */
    protected $stock;

    /**
     * @var string Статус
     * @ODM\String
     */
    public $status;

    /**
     * @var float Ширина см.
     * @ODM\Float
     */
    protected $width;

    /**
     * @var float Высота см.
     * @ODM\Float
     */
    protected $height;

    /**
     * @var float Длина см.
     * @ODM\Float
     */
    protected $length;

    /**
     * @var float Вес кг.
     * @ODM\Float
     */
    protected $weight;

    /**
     * @var int Количество просмотров (заходов на страницу товара разных пользователей)
     * @ODM\Int
     */
    protected $viewed;

    /**
     * @var array Массив для сортировки по бейджам
     * @ODM\Hash
     */
    protected $badgeSorts;

    /**
     * @var string Полное имя для текущего магазина
     */
    protected $currentStoreFullName;

    /**
     * @var string Сокращенное имя для текущего магазина
     */
    protected $currentStoreShortName;

    /**
     * @var float Цена для текущего магазина с учетом скидки
     */
    protected $currentStorePrice;

    /**
     * @var float Цена для текущего магазина без учета скидки
     */
    protected $currentStoreOriginalPrice;

    /**
     * @var int Скидка для текущего магазина
     */
    protected $currentStoreDiscount;

    /**
     * @var float Цена для оплаты
     */
    protected $checkoutPrice;

    /**
     * @var string Ссылка
     */
    protected $url;

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        if ($this->currentStoreFullName) {
            return $this->currentStoreFullName;
        }
        if ($this->currentStoreShortName) {
            return $this->currentStoreShortName;
        }

        return $this->name;
    }

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->accessories = new ArrayCollection();
        $this->parameters  = new ArrayCollection();
    }

    /**
     * Обнуление идентификатора при клонировании
     */
    public function __clone()
    {
        // Обнуляем id для клонирования
        $this->id = null;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set model
     * @param Model $model
     * @return self
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Get model
     * @return Model $model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description ?: $this->getModel()->getDescription();
    }

    /**
     * Set videoRef
     * @param array $videoRef
     * @return self
     */
    public function setVideoRef($videoRef)
    {
        $this->videoRef = $videoRef;
        return $this;
    }

    /**
     * Get videoRef
     * @return array $videoRef
     */
    public function getVideoRef()
    {
        return $this->videoRef;
    }

    /**
     * Set reviewRef
     * @param array $reviewRef
     * @return self
     */
    public function setReviewRef($reviewRef)
    {
        $this->reviewRef = $reviewRef;
        return $this;
    }

    /**
     * Get reviewRef
     * @return array $reviewRef
     */
    public function getReviewRef()
    {
        return $this->reviewRef;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set images
     * @param array $images
     * @return self
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * Get images
     * @return hash $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set flash
     * @param string $flash
     * @return self
     */
    public function setFlash($flash)
    {
        $this->flash = $flash;
        return $this;
    }

    /**
     * Get flash
     * @return string $flash
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * Set isSpecial
     * @param boolean $isSpecial
     * @return self
     */
    public function setIsSpecial($isSpecial)
    {
        $this->isSpecial = $isSpecial;
        return $this;
    }

    /**
     * Get isSpecial
     * @return boolean $isSpecial
     */
    public function getIsSpecial()
    {
        return $this->isSpecial;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set image2
     * @param string $image2
     * @return self
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
        return $this;
    }

    /**
     * Get image2
     * @return string $image2
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set flashPreview
     * @param string $flashPreview
     * @return self
     */
    public function setFlashPreview($flashPreview)
    {
        $this->flashPreview = $flashPreview;
        return $this;
    }

    /**
     * Get flashPreview
     * @return string $flashPreview
     */
    public function getFlashPreview()
    {
        return $this->flashPreview;
    }

    /**
     * Add storePrice
     * @param array $storePrice
     * @return self
     */
    public function addStorePrice($storePrice)
    {
        $this->storePrice[] = $storePrice;
        return $this;
    }

    /**
     * Set storePrice
     * @param array $storePrice
     * @return self
     */
    public function setStorePrice($storePrice)
    {
        $this->storePrice = $storePrice;
        return $this;
    }

    /**
     * Get storePrice
     * @return array $storePrice
     */
    public function getStorePrice()
    {
        return $this->storePrice;
    }

    /**
     * Set seoInfo
     * @param \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     * @return self
     */
    public function setSeoInfo($seoInfo)
    {
        $this->seoInfo = $seoInfo;
        return $this;
    }

    /**
     * Get seoInfo
     * @return \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     */
    public function getSeoInfo()
    {
        return $this->seoInfo;
    }

    /**
     * Set article
     * @param string $article
     * @return self
     */
    public function setArticle($article)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * Get article
     * @return string $article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Add accessory
     * @param \Nitra\ProductBundle\Document\Product $accessory
     * @return self
     */
    public function addAccessory(\Nitra\ProductBundle\Document\Product $accessory)
    {
        $this->accessories[] = $accessory;
        return $this;
    }

    /**
     * Remove accessory
     * @param \Nitra\ProductBundle\Document\Product $accessory
     * @return self
     */
    public function removeAccessory(\Nitra\ProductBundle\Document\Product $accessory)
    {
        $this->accessories->removeElement($accessory);
        return $this;
    }

    /**
     * Get accessories
     * @return Product[] $accessories
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * Set accessories
     * @param \Nitra\ProductBundle\Document\Product[] $accessories
     * @return self
     */
    public function setAccessories($accessories)
    {
        $this->accessories = $accessories;
        return $this;
    }

    /**
     * Set description2
     * @param string $description2
     * @return self
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
        return $this;
    }

    /**
     * Get description2
     * @return string $description2
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Set description3
     * @param string $description3
     * @return self
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;
        return $this;
    }

    /**
     * Get description3
     * @return string $description3
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * Set parameters
     * @param \Nitra\ProductBundle\Document\ProductParameter[] $parameters
     * @return self
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Get parameters
     * @return \Nitra\ProductBundle\Document\ProductParameter[] $parameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Add parameters
     * @param \Nitra\ProductBundle\Document\ProductParameter $parameter
     * @return self
     */
    public function addParameter(\Nitra\ProductBundle\Document\ProductParameter $parameter)
    {
        $this->parameters[] = $parameter;
        return $this;
    }

    /**
     * Remove parameters
     * @param \Nitra\ProductBundle\Document\ProductParameter $parameter
     * @return self
     */
    public function removeParameter(\Nitra\ProductBundle\Document\ProductParameter $parameter)
    {
        foreach ($this->parameters as $key => $existsParameter) {
            if ($parameter->getId() == $existsParameter->getId()) {
                $this->parameter->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Set color
     * @param \Nitra\ProductBundle\Document\Color $color
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Get color
     * @return \Nitra\ProductBundle\Document\Color $color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set badge
     * @param \Nitra\ProductBundle\Document\Badge $badge
     * @return self
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }

    /**
     * Get badge
     * @return \Nitra\ProductBundle\Document\Badge $badge
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set stock
     * @param string $stock
     * @return self
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * Get stock
     * @return string $stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set status
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        $this->isActive = ($status == 'showcase');

        return $this;
    }

    /**
     * Get status
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fullNameForSearch
     * @param string $fullNameForSearch
     * @return self
     */
    public function setFullNameForSearch($fullNameForSearch)
    {
        $this->fullNameForSearch = $fullNameForSearch;
        return $this;
    }

    /**
     * Get fullNameForSearch
     * @return string
     */
    public function getFullNameForSearch()
    {
        return $this->fullNameForSearch;
    }

    /**
     * Set fullNames
     * @param array $fullNames
     * @return self
     */
    public function setFullNames(array $fullNames)
    {
        $this->fullNames = $fullNames;
        return $this;
    }

    /**
     * Get fullNames
     * @return array $fullNames
     */
    public function getFullNames()
    {
        return $this->fullNames;
    }

    /**
     * Set shortNames
     * @param array $shortNames
     * @return self
     */
    public function setShortNames(array $shortNames)
    {
        $this->shortNames = $shortNames;
        return $this;
    }

    /**
     * Get shortNames
     * @return array $shortNames
     */
    public function getShortNames()
    {
        return $this->shortNames;
    }

    /**
     * Set fullUrlAliasRu
     * @param string $fullUrlAliasRu
     * @return self
     */
    public function setFullUrlAliasRu($fullUrlAliasRu)
    {
        $this->fullUrlAliasRu = $fullUrlAliasRu;
        return $this;
    }

    /**
     * Get fullUrlAliasRu
     * @return string
     */
    public function getFullUrlAliasRu()
    {
        return $this->fullUrlAliasRu;
    }

    /**
     * Set fullUrlAliasEn
     * @param string $fullUrlAliasEn
     * @return self
     */
    public function setFullUrlAliasEn($fullUrlAliasEn)
    {
        $this->fullUrlAliasEn = $fullUrlAliasEn;
        return $this;
    }

    /**
     * Get fullUrlAliasEn
     * @return string
     */
    public function getFullUrlAliasEn()
    {
        return $this->fullUrlAliasEn;
    }

    /**
     * Set width
     * @param float $width
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * Get width
     * @return float width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     * @param float $height
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * Get height
     * @return float height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set length
     * @param float $length
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * Get length
     * @return float length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set weight
     * @param float $weight
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * Get weight
     * @return float weight
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set isPopular
     * @param boolean $isPopular
     * @return self
     */
    public function setIsPopular($isPopular)
    {
        $this->isPopular = $isPopular;
        return $this;
    }

    /**
     * Get isPopular
     * @return boolean $isPopular
     */
    public function getIsPopular()
    {
        return $this->isPopular;
    }

    /**
     * Get viewed
     * @return int $viewed
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set viewed
     * @param int $viewed
     * @return self
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
        return $this;
    }

    /**
     * Get badgeSorts
     * @return array $badgeSorts
     */
    public function getBadgeSorts()
    {
        return $this->badgeSorts;
    }

    /**
     * Set badgeSorts
     * @param array $badgeSorts
     * @return self
     */
    public function setBadgeSorts($badgeSorts)
    {
        $this->badgeSorts = $badgeSorts;
        return $this;
    }

    /**
     * Get counter
     * @return int $counter
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set counter
     * @param int $counter
     * @return self
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;
        return $this;
    }

    /**
     * Set full name for current store
     * @param string $fullName
     * @return self
     */
    public function setFullName($fullName)
    {
        $this->currentStoreFullName = $fullName;
        return $this;
    }

    /**
     * Get full name for current store
     * @return string
     */
    public function getFullName()
    {
        return $this->currentStoreFullName;
    }

    /**
     * Set short name for current store
     * @param string $shortName
     * @return self
     */
    public function setShortName($shortName)
    {
        $this->currentStoreShortName = $shortName;
        return $this;
    }

    /**
     * Get short name for current store
     * @return string
     */
    public function getShortName()
    {
        return $this->currentStoreShortName;
    }

    /**
     * Set original price for current store
     * @param float $price
     * @return self
     */
    public function setOriginalPrice($price)
    {
        $this->currentStoreOriginalPrice = $price;
        return $this;
    }

    /**
     * Get original price for current store
     * @return float
     */
    public function getOriginalPrice()
    {
        return $this->currentStoreOriginalPrice;
    }

    /**
     * Set price for current store
     * @param float $price
     * @return self
     */
    public function setPrice($price)
    {
        $this->currentStorePrice = $price;
        return $this;
    }

    /**
     * Get price for current store
     * @return float
     */
    public function getPrice()
    {
        return $this->currentStorePrice;
    }

    /**
     * Set discount for current store
     * @param float $discount
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->currentStoreDiscount = $discount;
        return $this;
    }

    /**
     * Get discount for current store
     * @return float
     */
    public function getDiscount()
    {
        return $this->currentStoreDiscount;
    }

    /**
     * Set checkout price
     * @param float $price
     * @return self
     */
    public function setCheckoutPrice($price)
    {
        $this->checkoutPrice = $price;
        return $this;
    }

    /**
     * Get checkout price
     * @return float
     */
    public function getCheckoutPrice()
    {
        return $this->checkoutPrice;
    }
}