<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Nitra\ProductBundle\Sluggable\Mapping\Annotation\Slug;

/**
 * @ODM\Document(collection="ParameterValues")
 */
class ParameterValue
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Значение параметра
     * @ODM\String
     * @Assert\NotBlank
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var int Порядок сортировки параметра
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var boolean Популярный?
     * @ODM\Boolean
     */
    protected $isPopular;

    /**
     * @var \Nitra\ProductBundle\Document\Parameter Параметр, к которому относится значение
     * @ODM\ReferenceOne(targetDocument="Parameter", inversedBy="parameterValues")
     */
    protected $parameter;

    /**
     * To string converter
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isPopular
     * @param boolean $isPopular
     * @return self
     */
    public function setIsPopular($isPopular)
    {
        $this->isPopular = $isPopular;
        return $this;
    }

    /**
     * Get isPopular
     * @return boolean $isPopular
     */
    public function getIsPopular()
    {
        return $this->isPopular;
    }

    /**
     * Set parameter
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     * @return self
     */
    public function setParameter(Parameter $parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * Get parameter
     * @return \Nitra\ProductBundle\Document\Parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }
}