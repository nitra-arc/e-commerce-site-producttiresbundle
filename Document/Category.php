<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document(repositoryClass="Nitra\ProductBundle\Repository\CategoryRepository", collection="Categories")
 * @Gedmo\Tree(type="materializedPath", activateLocking=true)
 */
class Category implements \Gedmo\Tree\Node
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Имя
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\TreePathSource
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Имя категории в единственном числе (Холодильники - Холодильник)
     * @ODM\String
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $singularName;

    /**
     * @var string Полное имя для url для Ru локации
     * по типу parentCategory/childrenCategory/product
     * @ODM\String
     */
    protected $fullUrlAliasRu;

    /**
     * @var string Полное имя для url для En локации
     * по типу parentCategory/childrenCategory/product
     * @ODM\String
     */
    protected $fullUrlAliasEn;

    /**
     * @var string Путь к изображению
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $image;

    /**
     * @var int Порядок сортировки
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var boolean Активна ли категория
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var \Nitra\ProductBundle\Document\Category Родительская категория
     * @ODM\ReferenceOne(targetDocument="Category", nullable=true, inversedBy="children")
     * @Gedmo\TreeParent
     */
    protected $parent;

    /**
     * @var \Nitra\ProductBundle\Document\Category[] Дочерние категории
     * @ODM\ReferenceMany(targetDocument="Category", mappedBy="parent", sort={"sortOrder"=1})
     */
    protected $children;

    /**
     * @var string Путь по дереву категорий
     * @ODM\String
     * @Gedmo\TreePath(separator="|")
     */
    protected $path;

    /**
     * @var int Уровнь вложености
     * @ODM\Int
     * @Gedmo\TreeLevel
     */
    protected $level;

    /**
     * @var \DateTime Время блокировки
     * @ODM\Date
     * @Gedmo\TreeLockTime
     */
    protected $lockTime;

    /**
     * @var \Nitra\StoreBundle\Document\Store[] Магазины к которым принадлежит
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * @var \Nitra\SeoBundle\Document\EmbedSeo SEO описание
     * @ODM\EmbedOne(targetDocument="Nitra\SeoBundle\Document\EmbedSeo")
     */
    protected $seoInfo;

    /**
     * @var string Название css класса для стилизации на сайте
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $styleClass;

    /**
     * @var int Номер колонки в меню
     * @ODM\Int
     */
    protected $column;

    /**
     * @var array Настройки формирование полного имени товара
     * @ODM\Hash
     */
    protected $fullProductName = array();

    /**
     * @var array Настрйки формирование сокарщенного имени товара
     * @ODM\Hash
     */
    protected $shortProductName = array();

    /**
     * @var array Настрйки формирование полного имени модели
     * @ODM\Hash
     */
    protected $fullModelName = array();

    /**
     * @var array Настрйки формирование сокарщенного имени модели
     * @ODM\Hash
     */
    protected $shortModelName = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stores = new ArrayCollection();
    }

    /**
     * Конверт в строку
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Имя категории с приставкой по уровню вложености
     * @return string
     */
    public function getTreeName()
    {
        $prefix = ($this->level > 1) ? str_repeat('&nbsp;&nbsp;', $this->level) : '';

        return (string)html_entity_decode($prefix) . $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set singularName
     * @param string $singularName
     * @return self
     */
    public function setSingularName($singularName)
    {
        $this->singularName = $singularName;

        return $this;
    }

    /**
     * Get singularName
     * @return string $singularName
     */
    public function getSingularName()
    {
        return $this->singularName;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set parent
     * @param \Nitra\ProductBundle\Document\Category $parent
     * @return self
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     * @return \Nitra\ProductBundle\Document\Category $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get root category
     * @return Category
     */
    public function getRoot()
    {
        return $this->recursiveParent($this);
    }

    /**
     * Recursive getting parent category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return \Nitra\ProductBundle\Document\Category
     */
    protected function recursiveParent($category)
    {
        return $category->getParent()
            ? $this->recursiveParent($category->getParent())
            : $category;
    }

    /**
     * Get children
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Remove parent
     * @return self
     */
    public function removeParent()
    {
        $this->parent = null;
        return $this;
    }

    /**
     * Set path
     * @param string $path
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set level
     * @param int $level
     * @return self
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     * @return int $level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set lockTime
     * @param \DateTime $lockTime
     * @return self
     */
    public function setLockTime($lockTime)
    {
        $this->lockTime = $lockTime;
        return $this;
    }

    /**
     * Get lockTime
     * @return \DateTime $lockTime
     */
    public function getLockTime()
    {
        return $this->lockTime;
    }

    /**
     * Add store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
        return $this;
    }

    /**
     * Remove store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        foreach ($this->stores as $key => $existsStore) {
            if ($existsStore->getId() == $store->getId()) {
                $this->stores->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Get stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     * @param \Nitra\StoreBundle\Document\Store[] $stores
     * @return self
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
        return $this;
    }

    /**
     * Set seoInfo
     * @param \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     * @return self
     */
    public function setSeoInfo(\Nitra\SeoBundle\Document\EmbedSeo $seoInfo)
    {
        $this->seoInfo = $seoInfo;
        return $this;
    }

    /**
     * Get seoInfo
     * @return \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     */
    public function getSeoInfo()
    {
        return $this->seoInfo;
    }

    /**
     * Set styleClass
     * @param string $styleClass
     * @return self
     */
    public function setStyleClass($styleClass)
    {
        $this->styleClass = $styleClass;
        return $this;
    }

    /**
     * Get styleClass
     * @return string $styleClass
     */
    public function getStyleClass()
    {
        return $this->styleClass;
    }

    /**
     * Set column
     * @param int $column
     * @return self
     */
    public function setColumn($column)
    {
        $this->column = $column;
        return $this;
    }

    /**
     * Get column
     * @return int $column
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Set fullUrlAliasRu
     * @param string $fullUrlAliasRu
     * @return self
     */
    public function setFullUrlAliasRu($fullUrlAliasRu)
    {
        $this->fullUrlAliasRu = $fullUrlAliasRu;
        return $this;
    }

    /**
     * Get fullUrlAliasRu
     * @return string
     */
    public function getFullUrlAliasRu()
    {
        return $this->fullUrlAliasRu;
    }

    /**
     * Set fullUrlAliasEn
     * @param string $fullUrlAliasEn
     * @return self
     */
    public function setFullUrlAliasEn($fullUrlAliasEn)
    {
        $this->fullUrlAliasEn = $fullUrlAliasEn;
        return $this;
    }

    /**
     * Get fullUrlAliasEn
     * @return string
     */
    public function getFullUrlAliasEn()
    {
        return $this->fullUrlAliasEn;
    }

    /**
     * Set fullProductName
     * @param array $fullProductName
     * @return self
     */
    public function setFullProductName(array $fullProductName)
    {
        $this->fullProductName = array_values($fullProductName);
        return $this;
    }

    /**
     * Get fullProductName
     * @return array
     */
    public function getFullProductName()
    {
        $fullProductName = $this->fullProductName;
        $multisort = array();
        foreach ($fullProductName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $fullProductName);

        return $fullProductName;
    }

    /**
     * Set shortProductName
     * @param array $shortProductName
     * @return self
     */
    public function setShortProductName(array $shortProductName)
    {
        $this->shortProductName = array_values($shortProductName);
        return $this;
    }

    /**
     * Get shortProductName
     * @return array
     */
    public function getShortProductName()
    {
        $shortProductName = $this->shortProductName;
        $multisort = array();
        foreach ($shortProductName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $shortProductName);

        return $shortProductName;
    }

    /**
     * Set fullModelName
     * @param array $fullModelName
     * @return self
     */
    public function setFullModelName(array $fullModelName)
    {
        $this->fullModelName = array_values($fullModelName);
        return $this;
    }

    /**
     * Get fullModelName
     * @return array
     */
    public function getFullModelName()
    {
        $fullModelName = $this->fullModelName;
        $multisort = array();
        foreach ($fullModelName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $fullModelName);

        return $fullModelName;
    }

    /**
     * Set shortModelName
     * @param array $shortModelName
     * @return self
     */
    public function setShortModelName(array $shortModelName)
    {
        $this->shortModelName = array_values($shortModelName);
        return $this;
    }

    /**
     * Get shortModelName
     * @return array
     */
    public function getShortModelName()
    {
        $shortModelName = $this->shortModelName;
        $multisort = array();
        foreach ($shortModelName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $shortModelName);

        return $shortModelName;
    }
}