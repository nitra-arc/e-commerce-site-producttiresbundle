<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Параметр и значения в товарах
 * @ODM\EmbeddedDocument
 */
class ProductParameter
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Gedmo\Timestampable\Traits\TimestampableDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var \Nitra\ProductBundle\Document\Parameter Параметр
     * @ODM\ReferenceOne(targetDocument="Parameter", simple=true)
     */
    protected $parameter;

    /**
     * @var \Nitra\ProductBundle\Document\ParameterValue[] Значения параметра
     * @ODM\ReferenceMany(targetDocument="ParameterValue", simple=true)
     */
    protected $values;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * Convert to string
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parameter
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     * @return self
     */
    public function setParameter(Parameter $parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * Get parameter
     * @return \Nitra\ProductBundle\Document\Parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Add value
     * @param \Nitra\ProductBundle\Document\ParameterValue $value
     * @return self
     */
    public function addValue(ParameterValue $value)
    {
        $this->values[] = $value;
        return $this;
    }

    /**
     * Remove value
     * @param \Nitra\ProductBundle\Document\ParameterValue $value
     * @return self
     */
    public function removeValue(ParameterValue $value)
    {
        foreach ($this->values as $key => $existsValue) {
            if ($existsValue->getId() == $value->getId()) {
                $this->values->remove($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Get values
     * @return ParameterValue[]
     */
    public function getValues()
    {
        return $this->values;
    }
}