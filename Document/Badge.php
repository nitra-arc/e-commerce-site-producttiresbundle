<?php

namespace Nitra\ProductBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints as ODMConstraints;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Badges")
 * @ODMConstraints\Unique(fields={"identifier"})
 */
class Badge
{
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Имя
     * @ODM\String
     * @Assert\Length(max = 255)
     * @Assert\NotBlank
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Строковый идентификатор для ссылок
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $identifier;

    /**
     * @var string Путь к большому изображению
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $imageBig;

    /**
     * @var string Путь к среднему изображению
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $imageMedium;

    /**
     * @var string Путь к маленькому изображению
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $imageSmall;

    /**
     * @var int Порядок сортировки
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var boolean Отображать на голавной
     * @ODM\Boolean
     */
    protected $isInMainPage;

    /**
     * @var int Порядок отображения на голавной
     * @ODM\Int
     */
    protected $sortOrderOnMainPage;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set imageBig
     * @param string $imageBig
     * @return self
     */
    public function setImageBig($imageBig)
    {
        $this->imageBig = $imageBig;
        return $this;
    }

    /**
     * Get imageBig
     * @return string $imageBig
     */
    public function getImageBig()
    {
        return $this->imageBig;
    }

    /**
     * Set imageMedium
     * @param string $imageMedium
     * @return self
     */
    public function setImageMedium($imageMedium)
    {
        $this->imageMedium = $imageMedium;
        return $this;
    }

    /**
     * Get imageMedium
     * @return string $imageMedium
     */
    public function getImageMedium()
    {
        return $this->imageMedium;
    }

    /**
     * Set imageSmall
     * @param string $imageSmall
     * @return self
     */
    public function setImageSmall($imageSmall)
    {
        $this->imageSmall = $imageSmall;
        return $this;
    }

    /**
     * Get imageSmall
     * @return string $imageSmall
     */
    public function getImageSmall()
    {
        return $this->imageSmall;
    }

    /**
     * Set identifier
     * @param string $identifier
     * @return self
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * Get identifier
     * @return string $identifier
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder ? : 0;
    }

    /**
     * Set isInMainPage
     * @param boolean $isInMainPage
     * @return self
     */
    public function setIsInMainPage($isInMainPage)
    {
        $this->isInMainPage = $isInMainPage;
        return $this;
    }

    /**
     * Get isInMainPage
     * @return boolean $isInMainPage
     */
    public function getIsInMainPage()
    {
        return $this->isInMainPage;
    }

    /**
     * Set sortOrderOnMainPage
     * @param int $sortOrderOnMainPage
     * @return self
     */
    public function setSortOrderOnMainPage($sortOrderOnMainPage)
    {
        $this->sortOrderOnMainPage = $sortOrderOnMainPage;
        return $this;
    }

    /**
     * Get sortOrderOnMainPage
     * @return int $sortOrderOnMainPage
     */
    public function getSortOrderOnMainPage()
    {
        return $this->sortOrderOnMainPage;
    }
}