<?php

namespace Nitra\ProductBundle\Repository;

use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;

class CategoryRepository extends MaterializedPathRepository
{
    /**
     * {@inheritDoc}
     */
    public function getChildrenQueryBuilder($node = null, $direct = false, $sortByField = null, $direction = 'asc', $includeNode = false)
    {
        $qb = parent::getChildrenQueryBuilder($node, $direct, $sortByField, $direction, $includeNode);

        $qb ->sort('column', 'asc')
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');

        return $qb;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category|null $node
     * @return \Doctrine\MongoDB\Query\Builder
     */
    protected function getChildrenQueryBuilderWithSortOrder($node = null)
    {
        return $this->getChildrenQueryBuilder($node, null, 'level', 'desc')
            ->field('isActive')->equals(true)
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');
    }

    /**
     * @param \Symfony\Component\Routing\RouterInterface $router
     * @param integer                                    $maxLevel
     *
     * @return array
     */
    public function getCategoryTree($router, $maxLevel = 3)
    {
        $result   = array();
        $prevCats = array();
        for ($level = $maxLevel; $level > 0; $level --) {
            $cats       = $this->selectCategoriesForTree($level);
            $categories = array();
            foreach ($cats as $cat) {
                $category = array(
                    'id'       => (string) $cat['_id'],
                    'name'     => $cat['name'],
                    'alias'    => $cat['aliasEn'],
                    'column'   => isset($cat['column'])     ? $cat['column']     : 0,
                    'class'    => isset($cat['styleClass']) ? $cat['styleClass'] : null,
                    'url'      => $router->generate('category_page', array(
                        'slug' => $cat['aliasEn'],
                    )),
                    'children' => isset($prevCats[(string) $cat['_id']])
                        ? $prevCats[(string) $cat['_id']]
                        : array(),
                );
                if ($level > 1) {
                    $parentId = (string) $cat['parent']['$id'];
                    if (!array_key_exists($parentId, $categories)) {
                        $categories[$parentId] = array();
                    }
                    $categories[$parentId][] = $category;
                } else {
                    $categories[] = $category;
                }
            }
            if ($level == 1) {
                $result = $categories;
            } else {
                $prevCats = $categories;
            }
        }

        return $result;
    }

    /**
     * Group categories by columns
     *
     * @param array      $categories
     * @param array|null $parent
     *
     * @return array
     */
    public function columnCategoriesTree(&$categories, &$parent = null)
    {
        if ($parent) {
            $parent['columns'] = array();
        }
        foreach ($categories as $id => &$category) {
            if ($parent) {
                if (!array_key_exists((int) $category['column'], $parent['columns'])) {
                    $parent['columns'][$category['column']] = array();
                }
                $parent['columns'][$category['column']][$id] = &$category;
            }
            $this->columnCategoriesTree($category['children'], $category);
        }
        if ($parent) {
            ksort($parent['columns']);
        }
    }

    protected function selectCategoriesForTree($level)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)
            ->select(array(
                'name',
                'styleClass',
                'column',
                'fullUrlAliasEn',
                'parent',
                'aliasEn',
            ))
            ->field('level')->equals($level)
            ->sort('sortOrder', 1)
            ->sort('name', 1)
            ->getQuery()
            ->execute()
            ->toArray();
    }

    /**
     * @param string    $parentId
     * @param int       $level
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getCategories($parentId, $level)
    {
        //Получение всех категорий
        $qb = $this->createQueryBuilder($this->documentName)
            ->sort('level', 'asc')
            ->sort('column', 'asc')
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');

        //Получение части дерева категорий с заданного уровня
        if ($parentId) {
            $qb ->field('path')->equals(new \MongoRegex('/' . $parentId . '/'))
                ->field('id')->notEqual(new \MongoId($parentId))
                ->field('level')->gte($level);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool $active
     * @return array
     */
    protected function formatCategoryArray($Category, $active)
    {
        $result = array(
            'id'            => $Category->getId(),
            'name'          => $Category->getName(),
            'styleClass'    => $Category->getStyleClass(),
            'image'         => $Category->getImage(),
            'slug'          => $Category->getAlias(),
            'subcategories' => array(),
        );
        if ($active) {
            $result['active'] = true;
        }

        return $result;
    }

    /**
     * @param int   $level
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool  $active
     * @param array $result
     * @param bool  $menu
     */
    protected function addCategory($level, $Category, $active, &$result, $menu = false)
    {
        $parents    = array_reverse($this->getParents($level, $Category));
        $last       = &$result;
        foreach ($parents as $i => $parentCategory) {
            if (!$parentCategory->getIsActive()) {
                return;
            }

            $last = &$last[$parentCategory->getId()];
            if (array_key_exists($i + 1, $parents)) {
                $last = &$last['subcategories'];
                if ($menu) {
                    if (!array_key_exists((int)$parents[$i + 1]->getColumn(), $last)) {
                        $last[(int)$parents[$i + 1]->getColumn()] = array();
                    }
                    $last = &$last[(int)$parents[$i + 1]->getColumn()];
                }
            }
        }
        $last = $this->formatCategoryArray($Category, $active);
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool $menu
     * @return bool
     */
    protected function checkCategory($Category, $menu = false)
    {
        return true;
    }

    /**
     * @param int   $level
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param array $result
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getParents($level, $Category, $result = array())
    {
        $result[$Category->getLevel()] = $Category;

        if ($Category->getParent() && $Category->getParent()->getId() && ($Category->getParent()->getLevel() > $level)) {
            return $this->getParents($level, $Category->getParent(), $result);
        } else {
            return $result;
        }
    }

    /**
     * Запрос на получение дерева категорий
     * @param string    $parentId
     * @param int       $level
     * @param string    $categoryId
     * @param int       $maxLevel
     * @return array
     */
    public function getCategoriesTree($parentId, $level = 0, $categoryId = null, $maxLevel = 3)
    {
        $categories = $this->getCategories($parentId, $level);

        //Построение выходного массива категорий
        $results = array();
        $activeCategories = $categoryId
            ? $this->getParents($level, $categories[$categoryId])
            : array();
        foreach ($categories as $category) {
            if (($category->getLevel() <= ($level + $maxLevel)) && $this->checkCategory($category)) {
                $this->addCategory($level, $category, in_array($category, $activeCategories), $results);
            }
        }
        return $results;
    }

    /**
     * Получение дерева категорий для меню
     * @param string    $parentId
     * @param int       $level
     * @param string    $categoryId
     * @param int       $maxLevel
     * @return array
     */
    public function getCategoriesTreeMenu($parentId, $level = 0, $categoryId = null, $maxLevel = 3)
    {
        $categories = $this->getCategories($parentId, $level)->toArray();

        //Построение выходного массива категорий
        $results = array();
        $activeCategories = $categoryId
            ? $this->getParents($level, $categories[$categoryId])
            : array();
        foreach ($categories as $category) {
            if (($category->getLevel() <= ($level + $maxLevel)) && $this->checkCategory($category, true)) {
                $this->addCategory($level, $category, in_array($category, $activeCategories), $results, true);
            }
        }

        return $results;
    }

    /**
     * get all params id by category path
     *
     * @param \Nitra\ProductBundle\Document\Category|null $category
     *
     * @return array
     */
    public function getAllowedParametersIds($category)
    {
        $matches = array();
        if ($category) {
            preg_match_all('/[a-f\d]{24}/', $category->getPath(), $matches);
        }

        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Parameter')
            ->distinct('_id');
        if ($category) {
            $qb->field('categories.id')->in($matches[0]);
        }
        $allowedParameters = $qb
            ->getQuery()
            ->execute();

        $allowedParametersIds = array_map(
            function($id) { return (string) $id; },
            is_array($allowedParameters) ? $allowedParameters : $allowedParameters->toArray()
        );

        return $allowedParametersIds;
    }

    /**
     * Get all params by category path
     *
     * @param \Nitra\ProductBundle\Document\Category|null $category
     * @param boolean                                     $hydrate
     *
     * @return \Nitra\ProductBundle\Document\Parameter[]
     */
    public function getFilterParameters($category, $hydrate = true)
    {
        $matches = array();
        if ($category) {
            preg_match_all('/[a-f\d]{24}/', $category->getPath(), $matches);
        }

        $qb = $this->dm->createQueryBuilder('NitraProductBundle:Parameter')
            ->hydrate($hydrate)
            ->field('isFilter')->equals(true)
            ->sort('sortOrderInFilter', 'asc');

        if ($category) {
            $qb->field('categories.id')->in($matches[0]);
        }

        return $qb
            ->getQuery()
            ->execute()
            ->toArray();
    }
}