<?php

namespace Nitra\ProductBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\StoreBundle\Helper\nlActions;

class ProductRepository extends DocumentRepository
{
    protected $alternativeProductsMongoJs = 'function(){
        var alternative = {};
        var original = db.Products.findOne({_id: ObjectId("__productId__")});
        if (original.hasOwnProperty("parameters")) {
            db.Products.find({
                "_id": {
                    $ne: ObjectId("__productId__")
                },
                "isActive": true,
                "stock": "inStock",
                "model.$id": { $in: [ __modelsIds__ ] },
                "stores.$id": ObjectId("__storeId__"),
                "storePrice.__storeId__.price": {
                    $exists: true
                }
            }).forEach(function(p){
                var matches = 0;
                var isFilterParameters = 0;
                for (oId in original.parameters) {
                    var originalParameter = db.Parameters.findOne({"_id": original.parameters[oId].parameter});
                    if (originalParameter.hasOwnProperty("isFilter") && originalParameter.isFilter) {
                        isFilterParameters ++;
                        if (p.hasOwnProperty("parameters")) {
                            for (pId in p.parameters) {
                                if (original.parameters[oId].parameter.valueOf() == p.parameters[pId].parameter.valueOf()) {
                                    var match = false;
                                    if (original.parameters[oId].hasOwnProperty("values") && p.parameters[pId].hasOwnProperty("values")) {
                                        for (ovId in original.parameters[oId].values) {
                                            for (pvId in p.parameters[pId].values) {
                                                if (p.parameters[pId].values[pvId].valueOf() == original.parameters[oId].values[ovId].valueOf()) {
                                                    match = true;
                                                }
                                            }
                                        }
                                    }
                                    if (match) {
                                        matches ++;
                                    }
                                }
                            }
                        }
                    }
                }
                if (matches >= (isFilterParameters / 2)) {
                    alternative[p._id.valueOf()] = matches;
                }
            });
        }

        return alternative;
    }';

    /**
     * Get default query builder
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getDefaultQb()
    {
        $store = Globals::getStore();

        $qb    = $this->createQueryBuilder()
            // add price exists condition only
            // isActive condition appends by filter (BsonFilter in StoreBundle)
            ->field('storePrice.' . $store['id'] . '.price')->exists(true);

        return $qb;
    }

    /**
     * Get products as array
     *
     * @param \Doctrine\MongoDB\Query\Builder   $qb
     * @param string                            $cacheKey
     * @param integer                           $cacheTime
     *
     * @return array
     */
    public function getProductsAsArray($qb, $cacheKey = null, $cacheTime = null)
    {
        $cache = Globals::getCache();
        $key   = $cacheKey
            ? Globals::mapCacheKey($cacheKey)
            : null;

        if ($key && $cache->contains($key)) {
            $result = $cache->fetch($key);
        } else {
            $store = Globals::getStore();

            $products = $qb->find()
                ->hydrate(false)
                ->select(array(
                    'aliasEn',
                    'fullUrlAliasEn',
                    'fullNames',
                    'badge',
                    'storePrice',
                    'stock',
                    'image',
                ))
                ->getQuery()
                ->execute();

            $result = array();

            $round        = Globals::getPriceRound();
            $exchange     = Globals::getCurrency('exchange');
            $baseExchange = Globals::getBaseCurrency('exchange');
            $homeUrl      = Globals::$container->get('router')
                ->generate('nitra_store_home_index', array());

            foreach ($products as $product) {
                $id              = (string) $product['_id'];
                $priceData       = $product['storePrice'][$store['id']];
                $actionDiscount  = nlActions::getDiscountByProductId($id, $priceData['price']);
                $productDiscount = isset($priceData['discount']) ? $priceData['discount'] : 0;
                $discount        = $actionDiscount > $productDiscount ? $actionDiscount : $productDiscount;

                $converted     = $priceData['price'] * $baseExchange / $exchange;
                $discounted    = $converted * (100 - $discount) / 100;
                $originalPrice = round($converted, $round);
                $price         = round($discounted, $round);

                $result[$id] = array(
                    'id'            => $id,
                    'fullName'      => $product['fullNames'][$store['id']],
                    'alias'         => $product['aliasEn'],
                    'discount'      => $discount,
                    'price'         => $price,
                    'originalPrice' => $originalPrice,
                    'stock'         => $product['stock'],
                    'url'           => $homeUrl . $product['fullUrlAliasEn'],
                    'image'         => isset($product['image']) ? $product['image'] : null,
                    'badge'         => isset($product['badge']['$id'])
                        ? $this->dm->createQueryBuilder('NitraProductBundle:Badge')
                            ->hydrate(false)
                            ->field('id')->equals($product['badge']['$id'])
                            ->getQuery()
                            ->getSingleResult()
                        : null,
                );
            }
        }

        if ($key) {
            $cache->save($key, $result, $cacheTime);
        }

        return $result;
    }

    /**
     * Add reviews to each product (if as array)
     *
     * @param array $products
     */
    public function addReviewsToProductArray(&$products)
    {
        foreach ($products as $id => &$product) {
            $product['reviews'] = $this->getProductReviews($id);
        }
    }

    /**
     * Add whishlist data to each product (if as array)
     *
     * @param array $products
     */
    public function addWhishlistToProductArray(&$products)
    {
        $sBuyer = Globals::$container->get('session')->get('buyer');
        $buyer  = $sBuyer
            ? $this->dm->find('NitraBuyerBundle:Buyer', $sBuyer['id'])
            : null;

        foreach ($products as $id => &$product) {
            $product['whishlist'] = $this->getProductWhishlist($id, $buyer ? $buyer->getId() : null);
        }
    }

    /**
     * @param string      $id
     * @param null|string $buyerId
     *
     * @return array
     */
    public function getProductWhishlist($id, $buyerId)
    {
        $data = array('productId' => $id);
        if ($buyerId) {
            $data['inWishlist'] = (bool) $this->dm
                ->createQueryBuilder('NitraWishlistBundle:Wishlist')
                ->field('buyer.id')->equals($buyerId)
                ->field('products.id')->equals($id)
                ->getQuery()
                ->getSingleResult();
        } else {
            $data['inWishlist'] = false;
        }

        return $data;
    }

    /**
     * Get product reviews
     *
     * @param string $productId
     *
     * @return array
     */
    public function getProductReviews($productId)
    {
        $qb = $this->getDocumentManager()
            ->createQueryBuilder('NitraReviewBundle:Review')
            ->field('object_id')->equals($productId)
            ->field('status')->equals(true);

        if (!$this->getProductReviewsShowUnmoderated()) {
            $qb->field('moderated')->equals(true);
        }

        $reviews = $qb->getQuery()->execute();

        $amount  = $reviews->count();

        if ($amount) {
            $sum = 0;
            foreach ($reviews as $review) {
                if ($review->getObjRating() > 0) {
                    $sum += $review->getObjRating();
                }
            }
            $average = (int) round($sum / $amount);
        } else {
            $average = 0;
        }

        return array(
            'average' => $average,
            'amount'  => $amount,
        );
    }

    /**
     * @return boolean
     */
    protected function getProductReviewsShowUnmoderated()
    {
        $cache = Globals::getCache();
        $key   = Globals::mapCacheKey('show_unmoderated');

        if ($cache->contains($key)) {
            $showUnmoderated = $cache->fetch($key);
        } else {
            $settings = $this->dm->getRepository('NitraReviewBundle:ReviewSettings')->findOneBy(array());
            $showUnmoderated = $settings ? $settings->getShowUnmoderated() : true;
            $cache->save($key, $showUnmoderated);
        }

        return $showUnmoderated;
    }

    /**
     * Get products query builder by category
     *
     * @param \Nitra\ProductBundle\Document\Category    $category   Category instance
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getProductsByCategoryQb($category)
    {
        // if catgegory has children
        if (count($category->getChildren()) > 0) {
            // find models only in current category
            $categories = array($category->getId());
        } else {
            // find models in all categories tree from current
            $categories = $this->dm->createQueryBuilder('NitraProductBundle:Category')
                ->distinct('_id')
                ->field('path')->equals(new \MongoRegex('/' . $category->getId() . '/'))
                ->getQuery()->execute()->toArray();
        }

        // get models ids by category
        $models = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->in($categories)
            ->getQuery()->execute()->toArray();

        // return query with models condition
        return $this->getDefaultQb()
            ->field('model.id')->in($models);
    }

    /**
     * Получение рекомендуемых товаров
     *
     * @param \Nitra\ProductBundle\Document\Product     $product    Product instance
     * @param integer                                   $limit      Limit of recomended products
     *
     * @return \Doctrine\ODM\MongoDB\Cursor
     */
    public function getRecommendedProducts($product, $limit)
    {
        $store = Globals::getStore();

        $field = 'storePrice.' . $store['id'] . '.price';
        $price = $product->getPrice();

        $qb    = $this->getRecommendedProductsQb($product, $limit, $store['id'])
            ->field($field);

        $products = $this->getRecommendedProductsByPrice($qb, $price, .2);

        if (!$products->count()) {
            $products = $this->getRecommendedProductsByPrice($qb, $price, .3);
        }

        if (!$products->count()) {
            $this->unsetFieldFromQuery($qb, $field);
            $qb->field($field)->exists(true);

            $products = $qb->getQuery()->execute();
        }

        return $products;
    }

    /**
     * Get recommended products by price and percents
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb         Query builder instance
     * @param integer                               $price      Original product price
     * @param integer                               $percents   Percents for query
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    protected function getRecommendedProductsByPrice($qb, $price, $percents)
    {
        return $qb
            ->gte($price - ($price * $percents))
            ->lte($price + ($price * $percents))
            ->getQuery()->execute();
    }

    /**
     * Unset conditions by field from query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $query  Query builder instance
     * @param string                                $field  Field name
     */
    protected function unsetFieldFromQuery($query, $field)
    {
        $queryArray = $query->getQueryArray();

        unset($queryArray[$field]);

        $query->setQueryArray($queryArray);
    }

    /**
     * Get query builder for recommended products
     *
     * @param \Nitra\ProductBundle\Document\Product     $product        Product instance
     * @param integer                                   $limit          Limit for products
     * @param string                                    $storeId        Id of store
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected function getRecommendedProductsQb($product, $limit, $storeId)
    {
        // get query by category
        return $this->getProductsByCategoryQb($product->getModel()->getCategory())
            // add condition for not select current product
            ->field('id')->notEqual($product->getId())
            // set sort by price
            ->sort('storePrice.' . $storeId . '.price')
            // set limit
            ->limit($limit);
    }

    /**
     * Получение товаров конкретной категории
     *
     * @param string $categoryId
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getCategoryProductsQb($categoryId)
    {
        $modelsIds = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->equals($categoryId)
            ->getQuery()->execute()->toArray();

        return $this->getDefaultQb()
            ->field('model.id')->in($modelsIds);
    }

    /**
     * Получение товаров категории для карусели
     *
     * @param \Nitra\ProductBundle\Document\Category    $category
     * @param \Nitra\ProductBundle\Document\Product     $product
     * @param int                                       $limit
     * @param int                                       $priceDeviation
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    public function getCarouselCategoryProducts($category, $product = null, $limit = 10, $priceDeviation = 0)
    {
        $qb = $this->getCategoryProductsQb($category->getId())
            ->limit($limit)
            ->sort('name', 'ASC');

        if ($product) {
            $qb->field('id')->notEqual($product->getId());
        }

        if ($priceDeviation && $product) {
            $store = Globals::getStore();
            $priceDeviation = (int) rtrim($priceDeviation, '%');

            $qb->field('storePrice.' . $store['id'] . '.price')
                ->gte($product->getPrice() * (1 - $priceDeviation / 100))
                ->lte($product->getPrice() * (1 + $priceDeviation / 100));
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Популярные товары (по просмотрам, или галочке)
     *
     * @param string                                    $mode   Mode, on of <b>by_is_popular, by_viewed</b>
     * @param integer                                   $limit  Limit of products
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    public function getPopularProducts($mode = null, $limit = 20)
    {
        $qb = $this->getDefaultQb()->limit($limit);

        switch ($mode) {
            case 'by_is_popular':
                $qb ->field('isPopular')->equals(true);
                break;
            default:
                $qb ->field('viewed')->gt(0)
                    ->sort('viewed', -1);
                break;
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Метод для получения товаров, альтернативных переданному
     * Альтернативными считаются товары этой категории, но другого бренда,
     * и у которых количество параметров, у которых совпало как минимум одно значение
     * более или равно количиству параметров (только тех которые отображаются в фильтрах) оригинального товара деленому на два
     *
     * @param \Nitra\ProductBundle\Document\Product         $product    Product instance
     * @param \Nitra\ProductBundle\Repository\Store|null    $store      Store instance or null
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    public function getAlternativeProducts($product, $store = null)
    {
        // get javascript function
        $js = $this->getJsForSelectAlternativeProducts($product, $store);

        // execute js
        $result = $this->executeJs($js);

        // if executing is failed
        if (!$result['ok']) {
            return array();
        }

        // find products by ids
        $products = $this->getDefaultQb()
            ->field('id')->in(array_keys($result['retval']))
            ->getQuery()->execute()->toArray();

        // sort products
        array_multisort($result['retval'], SORT_DESC, $products);

        return $products;
    }

    /**
     * Get javascript function for find alternative products
     *
     * @param \Nitra\ProductBundle\Document\Product     $product    Product instance
     * @param \Nitra\StoreBundle\Document\Store|null    $store      Store instance or null
     *
     * @return string JavaScript for execute in mongodb
     */
    protected function getJsForSelectAlternativeProducts($product, $store)
    {
        // get store id
        $storeId = (!$store || !($store instanceof \Nitra\StoreBundle\Document\Store))
            ? Globals::getStore()['id']
            : $store->getId();

        // find models ids by category but without models of current model brand
        $modelsIds = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->equals($product->getModel()->getCategory()->getId())
            ->field('brand.id')->notEqual($product->getModel()->getBrand()->getId())
            ->getQuery()->execute()->toArray();

        // define replacement array for create js
        $replaces = array(
            '__storeId__'   => $storeId,
            '__productId__' => $product->getId(),
            '__modelsIds__' => 'ObjectId("' . implode('"), ObjectId("', $modelsIds) . '")',
        );

        // return js
        return str_replace(
            array_keys($replaces),
            array_values($replaces),
            $this->alternativeProductsMongoJs
        );
    }

    /**
     * Execute javascript in mongodb
     *
     * @param string $js    JavaScript function
     *
     * @return array
     */
    protected function executeJs($js)
    {
        // get database name
        $mongoDatabaseName = $this->dm->getConfiguration()->getDefaultDB();
        // get connection
        $m = $this->dm->getConnection();
        // return results, get mongodb client
        return $m->getMongo()
            // select database
            ->selectDB($mongoDatabaseName)
            // execute javasctipt function
            ->command(array(
                // js
                'eval'   => $js,
                // no lock database, while js will be executed
                'nolock' => true,
            ));
    }

    /**
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $item   Product or model instance
     * @param boolean                                                                   $short  Get short or full characteristics
     * @param string                                                                    $type   Parameters type. Only <b>product, model</b> and <b>all</b> is allowed
     *
     * @return array
     */
    public function getProductOrModelCharacteristics($item, $short = false, $type = null)
    {
        $isModel = $item instanceof \Nitra\ProductBundle\Document\Model;

        if (!$type) {
            $type = $isModel
                ? 'model'
                : 'product';
        }

        $product = $isModel
            ? $item->getProducts()->first()
            : $item;

        $items = $product
            ? $this->formatParametersToTemplate($product->getParameters(), $type, $short, $product->getModel()->getCategory())
            : array();

        return array(
            'items'   => $this->sortParameters($items, $short),
            'type'    => $type,
            'product' => $product,
            'isShort' => $short,
        );
    }

    /**
     * Format product or model parameters to template
     *
     * @param \Nitra\ProductBundle\Document\ProductParameter[]  $parameters List of ProductParameter
     * @param string                                            $type       Parameters type
     * @param boolean                                           $short
     * @param \Nitra\ProductBundle\Document\Category            $category
     * <b>true</b>  - with inShortDescription - true<br>
     * <b>false</b> - with inDescription - true
     *
     * @return array
     */
    protected function formatParametersToTemplate($parameters, $type, $short, $category)
    {
        // define result array
        $formatted = array();
        // get all params id by category path
        $allowedParametersIds = $this->getDocumentManager()->getRepository('NitraProductBundle:Category')->getAllowedParametersIds($category);

        // iterate parameters
        foreach ($parameters as $productParameter) {
            // get original parameter from product parameter
            $parameter = $productParameter->getParameter();

            if (!in_array($parameter->getId(), $allowedParametersIds)) {
                continue;
            }

            // if parameter is match by conditions
            if (!$this->checkParameterToShow($parameter, $type, $short)) {
                // skip parameter
                continue;
            }

            // define values array
            $values = array();
            // iterate parameter values
            foreach ($productParameter->getValues() as $value) {
                // add value to values array
                $values[] = $value;
            }

            // if product don`t have parameter values
            if (empty($values)) {
                // skip him
                continue;
            }

            // add item to result array
            $formatted[$parameter->getId()] = array(
                'parameter' => $parameter,
                'values'    => $values,
            );
        }

        return $formatted;
    }

    /**
     * Check parameter match by conditions
     *
     * @param \Nitra\ProductBundle\Document\Parameter   $parameter
     * @param string                                    $type
     * @param boolean                                   $short
     *
     * @return boolean
     */
    protected function checkParameterToShow($parameter, $type, $short)
    {
        // allow by type
        $byType  = in_array($type, array('product', 'model'))
            ? $parameter->getType() == $type
            : true;
        // allow by short description
        $byShort = $short && $parameter->getIsShortDescription();
        // allow by description
        $byDesc  = !$short && $parameter->getIsDescription();

        // true - if type and one of descriptions
        return $byType && ($byShort || $byDesc);
    }

    /**
     * Sort formatted parameters
     *
     * @param array     $formatted
     * @param boolean   $short
     *
     * @return array
     */
    protected function sortParameters($formatted, $short)
    {
        // define callback function for sorting
        $closure = function($a, $b) use ($short) {
            // define getter
            $getter = $short
                ? 'getSortOrderInShortDescription'
                : 'getSortOrderInDescription';

            // if equals
            if ($a['parameter']->$getter() == $b['parameter']->$getter()) {
                return 0;
            }

            return $a['parameter']->$getter() > $b['parameter']->$getter() ? 1 : -1;
        };

        // sort parameters with save keys (ids)
        uasort($formatted, $closure);

        return $formatted;
    }
}