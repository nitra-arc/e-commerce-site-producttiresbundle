<?php

namespace Nitra\ProductBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NitraProductExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->setFilterConfiguration($container, $config['filter']);
        $this->setRouterConfiguration($container, $config['semantic_links']);

        $container->getDefinition('nitra.finder')->addArgument($config['search']);

        $container->setParameter('nitra_selectors', $config['selectors']);
    }

    /**
     * Set configuration for filter
     *
     * @param ContainerInterface    $container
     * @param array                 $configuration
     */
    protected function setFilterConfiguration($container, $configuration)
    {
        $container->setParameter('nitra.filter.mode',         $configuration['mode']);
        $container->setParameter('nitra.filter.selected_box', $configuration['selected_box']);
        $container->setParameter('nitra.filter.template',     $configuration['template']);
    }

    /**
     * Set configuration for router
     *
     * @param ContainerInterface    $container
     * @param boolean               $enabled
     */
    protected function setRouterConfiguration($container, $enabled)
    {
        $container->setParameter('nitra.router.enabled', $enabled);
    }
}