<?php

namespace Nitra\ProductBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class BreadcrumbsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('nitra.breadcrumbs')) {
            return;
        }

        $definition = $container->getDefinition('nitra.breadcrumbs');
        $renders    = $container->findTaggedServiceIds('nitra.breadcrumbs.render');

        foreach (array_keys($renders) as $id) {
            $definition->addMethodCall('addRender', array(
                new Reference($id),
            ));
        }
    }
}