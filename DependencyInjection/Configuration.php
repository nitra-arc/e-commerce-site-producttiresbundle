<?php

namespace Nitra\ProductBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $rootNode = $builder
            ->root('nitra_product')
            ->addDefaultsIfNotSet()
            ->children();

        $this->addFilter($rootNode);
        $this->addSearch($rootNode);
        $this->addSelector($rootNode);

        $rootNode
            ->booleanNode('semantic_links')
                ->defaultFalse()
            ->end()
        ->end();

        return $builder;
    }

    protected function addFilter(NodeBuilder $builder)
    {
        $builder
            ->arrayNode('filter')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('template')
                        ->defaultValue('default')
                    ->end()
                    ->enumNode('mode')
                        ->values(array('product', 'model',))
                        ->defaultValue('product')
                    ->end()
                    ->booleanNode('selected_box')
                        ->defaultValue(false)
                    ->end()
                ->end()
            ->end();
    }

    protected function addSearch(NodeBuilder $builder)
    {
        $builder
            ->arrayNode('search')
                ->addDefaultsIfNotSet()
                ->children()
                    ->booleanNode('process_all_on_group_select')->defaultFalse()->end()
                    ->arrayNode('elasticsearch')
                        ->children()
                            ->integerNode('port')->defaultValue(9200)->end()
                            ->scalarNode('host')->defaultValue('localhost')->end()
                            ->scalarNode('index')->defaultNull()->end()
                        ->end()
                    ->end()
                    ->arrayNode('groups')
                        ->defaultValue(array(
                            'products' => array(
                                'route'                 => 'product_page',
                                'routeParameters'       => array(
                                    'slug' => 'alias',
                                ),
                                'limit'                 => 'page',
                                'inSuggest'             => true,
                                'template'              => 'NitraProductBundle:Search/Product:page.html.twig',
                                'repository'            => 'NitraProductBundle:Product',
                                'qbGetter'              => 'getDefaultQb',
                                'embedded'              => null,
                                'staticRouteParameters' => array(),
                                'toString'              => 'fullName',
                                'order'                 => array(
                                    'name'  => array(
                                        'model' => 1,
                                        'name'  => 1,
                                    ),
                                    'price' => array(
                                        'storePrice.{storeId}.price' => 1,
                                    ),
                                ),
                                'group'                 => array(
                                    'limit'    => 3,
                                    'template' => null,
                                ),
                                'es'                    => array(
                                    'type'   => 'products',
                                    'fields' => array(
                                        'fullNameForSearch',
                                    ),
                                ),
                                'odm'                   => array(
                                    'fields' => array(
                                        'fullNameForSearch',
                                    ),
                                ),
                            )
                        ))
                        ->prototype('array')
                            ->children()
                                ->variableNode('limit')->end()
                                ->booleanNode('inSuggest')->defaultTrue()->end()
                                ->scalarNode('template')->end()
                                ->scalarNode('repository')->end()
                                ->scalarNode('qbGetter')->end()
                                ->scalarNode('route')->isRequired()->end()
                                ->scalarNode('embedded')->defaultNull()->end()
                                ->scalarNode('toString')->defaultNull()->end()
                                ->arrayNode('routeParameters')
                                    ->isRequired()
                                    ->prototype('scalar')->end()
                                ->end()
                                ->arrayNode('staticRouteParameters')
                                    ->prototype('scalar')->end()
                                ->end()
                                ->arrayNode('order')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('group')
                                    ->children()
                                        ->variableNode('limit')->end()
                                        ->scalarNode('template')->end()
                                    ->end()
                                ->end()
                                ->arrayNode('es')
                                    ->children()
                                        ->scalarNode('type')->end()
                                        ->arrayNode('fields')
                                            ->prototype('scalar')->end()
                                        ->end()
                                    ->end()
                                ->end()
                                ->arrayNode('odm')
                                    ->isRequired()
                                    ->children()
                                        ->arrayNode('fields')
                                            ->isRequired()
                                            ->prototype('scalar')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * Добавление конфигурации подборщиков
     *
     * @param NodeBuilder $builder
     */
    protected function addSelector(NodeBuilder $builder)
    {
        $builder
            ->arrayNode('selectors')
            ->addDefaultsIfNotSet()
                ->children()
                    ->arrayNode('check_exists_products')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->booleanNode('vehicle')->defaultFalse()->end()
                            ->booleanNode('year')->defaultFalse()->end()
                            ->booleanNode('modification')->defaultFalse()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}