<?php

namespace Nitra\ProductBundle\Finder\Event;

use Symfony\Component\EventDispatcher\Event;

class PaginateEvent extends Event
{
    /** @var string */
    protected $group;
    /** @var array */
    protected $configuration;
    /** @var \Doctrine\ODM\MongoDB\Query\Builder|array */
    protected $toPaginate;
    /** @var int */
    protected $limit;
    /** @var array */
    protected $queryArray;

    public function __construct($group, $configuration, &$toPaginate, &$limit, $queryArray)
    {
        $this->group            = $group;
        $this->configuration    = $configuration;
        $this->toPaginate       = &$toPaginate;
        $this->limit            = &$limit;
        $this->queryArray       = $queryArray;
    }

    /**
     * get paginated group
     * @return string
     */
    public function getGroupName()
    {
        return $this->group;
    }

    /**
     * get configuration of group
     * @return array
     */
    public function getGroupConfiguration()
    {
        return $this->configuration;
    }

    /**
     * get query builder or array which must be paginated
     * @return \Doctrine\ODM\MongoDB\Query\Builder|array
     */
    public function getToPaginate()
    {
        return $this->toPaginate;
    }

    /**
     * get limit items to paginate
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * set limit items to paginate
     * @param int $limit
     * @return self
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }
}