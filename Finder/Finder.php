<?php

namespace Nitra\ProductBundle\Finder;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Elasticsearch\ClientBuilder;
use Nitra\ProductBundle\Finder\FinderEvents;
use Nitra\ProductBundle\Finder\Event\PaginateEvent;
use Nitra\ProductBundle\Finder\Event\PreRenderEvent;
use Nitra\StoreBundle\Lib\Globals;

class Finder
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    /** @var \Elasticsearch\Client */
    protected $client;

    /** @var \Twig_Environment */
    protected $twig;

    /** @var \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher */
    protected $dispatcher;

    // array with configuration
    protected $configuration;
    // query term
    protected $term;
    // selected group
    protected $group;

    // regex to search odm
    protected $regex;

    // amount items by group
    protected $amounts = array();

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @param array $configuration
     */
    public function __construct(Container $container, array $configuration)
    {
        $this->container        = $container;
        $this->configuration    = $configuration;
        $this->twig             = $container->get('twig');
        $this->dispatcher       = $container->get('event_dispatcher');
    }

    /**
     * Search items by term
     *
     * @param string    $term       Query for search
     * @param bool      $suggest    From suggest (if <b>true</b> - will return array, else - Response instance)
     * @param string    $group      Group for search (if null - search by all groups)
     * @param string    $sort       Key for sort
     *
     * @return Response|array
     */
    public function search($term, $suggest = false, $group = null, $sort = 'name')
    {
        if (!mb_detect_encoding($term, 'UTF-8', true)) {
            $term = iconv('Windows-1251', 'UTF-8', $term);
        }
        $this->term     = $term;
        $this->group    = $group;
        // explode by space query string
        $keyWords = preg_split('/ +/', preg_replace('/[^A-ZА-ЯЁ0-9\s]/iu', ' ', mb_strtolower($term, 'UTF-8')));

        // getting query array from elasticsearch (if enabled) or database
        $queryArrays = $this->useElasticSearch()
            ? $this->searchElastic($keyWords, $suggest)
            : $this->searchOdm($keyWords, $suggest);
        $paginatedResults = $this->paginateResults($queryArrays, $sort);

        if ($suggest) {
            return $paginatedResults;
        }

        $amounts = array_filter($this->amounts, function($item) {
            return $item > 0;
        });
        if (!$group && (count($amounts) === 1) && (current($amounts) > 0)) {
            $path = $this->container->get('router')->generate('search_page', array(
                'group' => key($amounts),
                'term'  => $term,
            ));
            return new RedirectResponse($path);
        }

        // return Response object
        return $this->renderResults($paginatedResults);
    }

    /**
     * Public getter for configuration
     *
     * @param string $group
     *
     * @return array
     */
    public function getConfiguration($group = null)
    {
        if ($group) {
            return array_key_exists($group, $this->configuration['groups'])
                ? $this->configuration['groups'][$group]
                : null;
        }

        return $this->configuration;
    }

    /**
     * Getter for Doctrine MongoDb document manager
     *
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->container->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * Getter for Knp paginator
     *
     * @return \Knp\Component\Pager\Paginator
     */
    protected function getPaginator()
    {
        return $this->container->get('knp_paginator');
    }

    /**
     * Check elasticsearch is configured and available
     *
     * @return boolean
     */
    protected function useElasticSearch()
    {
        // if configured and class exists
        if (array_key_exists('elasticsearch', $this->configuration) && class_exists('Elasticsearch\Client')) {
            // creating client for host and port from config
            $hosts = array(
                $this->configuration['elasticsearch']['host'] . ':' . $this->configuration['elasticsearch']['port'],
            );
            $client = ClientBuilder::create()->setHosts($hosts)->build();

            // if ping is success
            try {
                $ping = $client->ping();
            } catch (\Elasticsearch\Common\Exceptions\Curl\CouldNotResolveHostException $ex) {
                $ping = false;
            }
            if ($ping) {
                // save client to protected class variable
                $this->client = $client;
            }

            // return ping result
            return $ping;
        }

        return false;
    }

    /**
     * Search by elastisearch
     *
     * @param array $keyWords
     * @param boolean $suggest
     *
     * @return array
     */
    protected function searchElastic($keyWords, $suggest)
    {
        $groups = array();
        foreach ($this->configuration['groups'] as $key => $group) {
            if (($this->configuration['process_all_on_group_select'] || !$this->group || ($this->group == $key)) && (!$suggest || ($suggest && $group['inSuggest']))) {
                foreach ($group['odm']['fields'] as $field) {
                    $groups[$key] = $this->getQueryArray($group);
                    $groups[$key]['$and'][] = array(
                        '_id'   => array(
                            '$in'   => $this->getIdsFromElastic($group, $group['es']['type'], $keyWords, $group['es']['fields']),
                        ),
                    );
                }
            }
        }

        return $groups;
    }

    /**
     * Get ids from elastic search index
     *
     * @param array  $configuration
     * @param string $type
     * @param array  $keyWords
     * @param array  $fields
     *
     * @return \MongoId[]
     */
    protected function getIdsFromElastic($configuration, $type, $keyWords, $fields)
    {
        $result = $this->getElasticSearchQuery($configuration, $type, $keyWords, $fields);
        if (!$result['hits']['total']) {
            $result = $this->getElasticSearchQuery($configuration, $type, $keyWords, $fields, true);
        }

        $ids = array();
        foreach ($result['hits']['hits'] as $hit) {
            $ids[] = new \MongoId($hit['_id']);
        }

        return $ids;
    }

    /**
     * Create and send query to ElasticSearch
     *
     * @param array   $configuration
     * @param string  $type
     * @param array   $keyWords
     * @param array   $fields
     * @param boolean $fuzzy
     *
     * @return array
     */
    protected function getElasticSearchQuery($configuration, $type, $keyWords, $fields, $fuzzy = false)
    {
        $query = array(
            'fields' => array(),
            'size'   => 1000000,
            'query'  => array(
                'bool' => array(
                    'must' => array(),
                ),
            ),
        );
        $must  = $this->generateMatches($keyWords, $fields, $fuzzy);
        $query['query']['bool']['must'] = $must;

        $cm  = $this->getDocumentManager()
            ->getRepository($configuration['repository'])
            ->getClassMetadata();

        // if document contains isActive field
        if ($cm->hasField('isActive')) {
            $query['query']['bool']['must'][] = array(
                'match' => array(
                    'isActive' => true,
                ),
            );
        }

        // if document contains deletedAt field
        if ($cm->hasField('deletedAt')) {
            $query['query']['bool']['must'][] = array(
                'constant_score' => array(
                    'filter' => array(
                        'missing' => array(
                            'field'      => 'deletedAt',
                            'existence'  => true,
                            'null_value' => false,
                        ),
                    ),
                ),
            );
        }

        return $this->client->search(array(
            'body'  => $query,
            'index' => $this->configuration['elasticsearch']['index'],
            'type'  => $type,
        ));
    }

    /**
     * Generate matches array for elasticsearch query
     *
     * @param array $keyWords
     * @param array $fields
     * @param bool  $fuzzy
     *
     * @return array
     */
    protected function generateMatches($keyWords, $fields, $fuzzy = false)
    {
        $must = array();
        foreach ($keyWords as $word) {
            $queryFields = array();
            foreach ($fields as $field) {
                $queryFields[] = $this->getESField($field, $word, $fuzzy);
            }

            $must[] = array(
                'function_score'    => array(
                    'query'             =>  array(
                        'bool'              => array(
                            'should'            => $queryFields,
                        ),
                    ),
                ),
            );
        }

        return $must;
    }

    /**
     * Get fields for elasticsearch
     *
     * @param string  $field
     * @param string  $word
     * @param boolean $fuzzy
     *
     * @return array
     */
    protected function getESField($field, $word, $fuzzy)
    {
        if (preg_match('/[\d]/', $word)) {
            return array('match' => array($field => array(
                'query'             => trim($word),
                'operator'          => 'and',
            ),),);
        } elseif ($fuzzy) {
            return array('fuzzy' => array($field => array(
                'max_expansions'    => 2,
                'value'             => trim($word),
            ),),);
        } else {
            return array('wildcard' => array(
                $field              => "*" . trim($word) . "*",
            ),);
        }
    }

    /**
     * Search by database (odm)
     *
     * @param array   $keyWords
     * @param boolean $suggest
     *
     * @return array
     */
    protected function searchOdm($keyWords, $suggest)
    {
        $groups = array();
        $storeId = Globals::getStore()['id'];
        foreach ($keyWords as &$keyWord) {
            if (preg_match('/[\d]/', $keyWord)) {
                $keyWord = '(^| )' . $keyWord . '( |$)';
            }
        }
        $this->regex = '/(?=.*' . implode(')(?=.*', $keyWords) . ')/i';
        $regex = new \MongoRegex($this->regex);
        foreach ($this->configuration['groups'] as $key => $group) {
            if (($this->configuration['process_all_on_group_select'] || !$this->group || ($this->group == $key)) && (!$suggest || ($suggest && $group['inSuggest']))) {
                $groups[$key] = $this->getQueryArray($group);
                foreach ($group['odm']['fields'] as $field) {
                    $field = str_replace('{storeId}', $storeId, $field);
                    $groups[$key]['$or'][] = array(
                        $field  => $regex,
                    );
                }
            }
        }

        return $groups;
    }

    /**
     * Getting query array from repository (if qbGetter is defined in config)
     *
     * @param array $group
     *
     * @return array
     */
    protected function getQueryArray($group)
    {
        $qa = array();
        // if getter for query builder is defined
        if (array_key_exists('qbGetter', $group) && $group['qbGetter']) {
            // get query array from query builder by configuration
            $qa = $this->getDocumentManager()->getRepository($group['repository'])->{$group['qbGetter']}()->getQueryArray();
            // if key '$or' exists
            if (array_key_exists('$or', $qa)) {
                // move him to '$and'
                if (!array_key_exists('$and', $qa)) {
                    $qa['$and'] = array();
                }
                $qa['$and'][] = array('$or' => $qa['$or']);
            }
        } else {
            $qa = array();
        }
        $qa['$or'] = array();

        return $qa;
    }

    /**
     * Paginate each group
     *
     * @param array $queryArrays
     * @param string $sort
     *
     * @return array
     */
    protected function paginateResults($queryArrays, $sort)
    {
        // define results array
        $results = array();
        foreach ($queryArrays as $key => $queryArray) {
            // getting group configuration
            $groupConfiguration = $this->configuration['groups'][$key];
            // create Query Builder object with sort
            $qb = $this->getDocumentManager()->createQueryBuilder($groupConfiguration['repository'])
                ->setQueryArray($queryArray)
                ->sort($this->getSort($groupConfiguration, $sort));
            // get limit per page (or group)
            $limit = $this->getLimit($groupConfiguration, !$this->group);
            // get array (if embedded) or query builder to paginate
            $toPaginate = $this->checkEmbeddedAndProcessHim($qb, $groupConfiguration);
            // if toPaginate variable is instance of query builder
            if ($toPaginate instanceof \Doctrine\ODM\MongoDB\Query\Builder) {
                // set limit to query
                $toPaginate->limit($limit);
            }
            // create on paginate event
            $event = new PaginateEvent($key, $groupConfiguration, $toPaginate, $limit, $queryArray);
            // dispatch event
            $this->dispatcher->dispatch(FinderEvents::ON_PAGINATE, $event);
            // save paginated data to result array
            $result = $this->paginate($toPaginate, $limit, !$this->group || $this->group == $key ? false : 1);
            $this->amounts[$key] = $result->getTotalItemCount();
            // save
            $results[$key] = $result;
        }

        return $results;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb
     * @param array                                 $groupConfiguration
     *
     * @return array
     */
    protected function checkEmbeddedAndProcessHim($qb, $groupConfiguration)
    {
        // set qb as variable which was be paginated
        $toPaginate = $qb;
        // if embedded is enabled
        if ($groupConfiguration['embedded']) {
            // define results array
            $embeddedResult = array();
            // getting items
            $items = $qb->getQuery()->execute();
            // loop by items
            foreach ($items as $item) {
                // getting embedded data
                $embeddedData = $item->{'get' . ucfirst($groupConfiguration['embedded'])}();
                foreach ($embeddedData as $embedded) {
                    if ($this->isMatchEmbedded($embedded, $groupConfiguration['embedded'], $groupConfiguration['odm']['fields'])) {
                        $embeddedResult[] = array(
                            'embedded'  => $embedded,
                            'item'      => $item,
                        );
                    }
                }
            }
            // set array with embedded documents to variable which was be paginated
            $toPaginate = $embeddedResult;
        }

        return $toPaginate;
    }

    /**
     * @param object    $object
     * @param boolean   $embedded
     * @param array     $fields
     *
     * @return boolean
     */
    protected function isMatchEmbedded($object, $embedded, $fields)
    {
        foreach ($fields as $field) {
            if (preg_match("/{$embedded}/", $field) && preg_match($this->regex . 'u', $object->{'get' . ucfirst(explode('.', $field)[1])}())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Pagination
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $query
     * @param int|null                              $limit
     * @param boolean|int                           $page
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    protected function paginate($query, $limit, $page = false)
    {
        $pagination = $this->getPaginator()->paginate(
            $query,
            // page number
            $page === false
                ? $this->container->get('request')->query->get('page', 1)
                : $page,
            // limit per page
            $limit,
            array(
                // for reset automatic sorting by paginator
                'sortFieldParameterName' => null,
            )
        );

        return $pagination;
    }

    /**
     * getting limit items from configuration
     * @param array $configuration
     * @param bool $group
     * @return int
     */
    protected function getLimit($configuration, $group = true)
    {
        // if limit is defined in query string
        if ($requestLimit = $this->container->get('request')->query->get('limit')) {
            // return him
            return $requestLimit;
        }
        // if group
        $configLimit = $group
            // if is defined for group - get him, else - get 3
            ? ((key_exists('group', $configuration) && key_exists('limit', $configuration['group']) && $configuration['group']['limit']) ? $configuration['group']['limit'] : 3)
            // if is defined - get him, else - get 10
            : ((key_exists('limit', $configuration) && $configuration['limit']) ? $configuration['limit'] : 10);

        // return integer value
        return (int) is_numeric($configLimit)
            // if is numeric - return him
            ? $configLimit
            // else - getting from store settings
            : Globals::getStoreLimit($configLimit, $group ? 3 : 10);
    }

    /**
     * @param array $paginatedResults
     *
     * @return Response
     */
    protected function renderResults($paginatedResults)
    {
        $sentences = explode(" ", $this->term);
        $replace = array();
        foreach ($sentences as $keyword) {
            if ($keyword) {
                $replace["/([^<])($keyword)([^>])/ui"] = '$1<b>$2</b>$3';
            }
        }

        // if not group and results
        if (!$this->group && array_sum($this->amounts)) {
            // return Response
            return $this->renderGroupsPage($paginatedResults, $replace);
        // else if group and results
        } elseif ($this->group && $this->amounts[$this->group]) {
            // return Response
            return $this->renderGroupPage($paginatedResults, $replace);
        // if nothing founded
        } else {
            return $this->renderNoResultsPage();
        }
    }

    /**
     * Render page with groups
     *
     * @param \Knp\Component\Pager\Pagination\SlidingPagination[]   $paginatedResults
     * @param array                                                 $replace
     *
     * @return Response
     */
    protected function renderGroupsPage($paginatedResults, $replace)
    {
        $results = array();
        // render each group
        foreach ($paginatedResults as $key => $group) {
            $groupConfiguration = $this->configuration['groups'][$key];
            $template   = $this->getTemplate($groupConfiguration, true);
            $parameters = array(
                'key'       => $key,
                'results'   => $group,
                'search'    => $this->term,
                'config'    => $groupConfiguration,
            );

            // create pre render event
            $event = new PreRenderEvent('groups.item', $template, $parameters, $key, $groupConfiguration);
            // dispatch event
            $this->dispatcher->dispatch(FinderEvents::PRE_RENDER, $event);

            $results[$key] = $this->twig->render($template, $parameters);
        }

        $template   = "NitraProductBundle:Search:groupsPage.html.twig";
        $parameters = array(
            'results'   => $results,
            'search'    => $this->term,
            'config'    => $this->configuration,
            'replace'   => $replace
        );

        // create pre render event
        $event = new PreRenderEvent('groups', $template, $parameters);
        // dispatch event
        $this->dispatcher->dispatch(FinderEvents::PRE_RENDER, $event);

        // render groups page (or suggest)
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * Render page of one group
     *
     * @param \Knp\Component\Pager\Pagination\SlidingPagination[]   $paginatedResults
     * @param array                                                 $replace
     *
     * @return Response
     */
    protected function renderGroupPage($paginatedResults, $replace)
    {
        // getting configuration
        $groupConfiguration = $this->configuration['groups'][$this->group];
        $template   = $this->getTemplate($groupConfiguration, false);
        $parameters = array(
            'results'           => $paginatedResults,
            'search'            => $this->term,
            'config'            => $groupConfiguration,
            'fullConfiguration' => $this->configuration,
            'replace'           => $replace,
            'group'             => $this->group,
        );
        // create pre render event
        $event = new PreRenderEvent('group', $template, $parameters, $this->group, $groupConfiguration);
        // dispatch event
        $this->dispatcher->dispatch(FinderEvents::PRE_RENDER, $event);

        // render page with results
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * Rendering page with no result message
     *
     * @return Response
     */
    protected function renderNoResultsPage()
    {
        $template   = "NitraProductBundle:Search:groupsPage.html.twig";
        $parameters = array(
            'search'    => $this->term,
            'group'     => $this->group,
        );
        // create pre render event
        $event = new PreRenderEvent('no.results', $template, $parameters);
        // dispatch event
        $this->dispatcher->dispatch(FinderEvents::PRE_RENDER, $event);
        // render page with no results
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * Getting template from configuration
     *
     * @param array $configuration
     * @param bool $group
     * @param bool $suggest
     *
     * @return string
     */
    protected function getTemplate($configuration, $group = true, $suggest = false)
    {
        // default template for group
        $defaultGroupTemplate = 'NitraProductBundle:Search:prototypeGroup.html.twig';
        // default template
        $defaultTemplate      = 'NitraProductBundle:Search:prototypePage.html.twig';

        // if group
        if ($group) {
            // if is defined for group - get him, else - get default group template
            return array_key_exists('group', $configuration) && array_key_exists('template', $configuration['group']) && $configuration['group']['template']
                ? $configuration['group']['template']
                : $defaultGroupTemplate;
        } else {
            // if is defined - get him, else - get default page template
            return array_key_exists('template', $configuration) && $configuration['template']
                ? $configuration['template']
                : $defaultTemplate;
        }
    }

    /**
     * Getting sort from configuration
     *
     * @param array $configuration
     * @param string $sort
     *
     * @return array
     */
    protected function getSort($configuration, $sort)
    {
        $defaultSort = array(
            'name'  => array(
                'name'  => 1,
            ),
        );
        $orders = array();
        // if config for him is not configured and for group configured
        if (!$orders && array_key_exists('order', $configuration) && $configuration['order']) {
            $orders = $configuration['order'];
        }

        // if configured for selected order
        $order = array_key_exists($sort, $orders)
            // get him
            ? $orders[$sort]
            // else get default order by name
            : $defaultSort['name'];

        $resultOrder = array();
        $storeId = Globals::getStore()['id'];
        foreach ($order as $field => $sortOrder) {
            $resultOrder[str_replace('{storeId}', $storeId, $field)] = $sortOrder;
        }

        return $resultOrder;
    }
}